from flask_restplus import Resource

from app.infrastructure.db.domain_dao import SessionContext
from app.infrastructure.rest.tools import get_login_from_token
from app.infrastructure.rest.tools import api_response
from app.infrastructure.rest.schemas import PortfolioDto

from app.domain.usecases.api.portfolio_manage import PortfolioManageUseCase
from app.domain.usecases.api.portfolio_homepage import PortfolioHomePageUseCase

api = PortfolioDto.portfolio_api


@api.route('/details/', defaults={'api_version': None})
@api.route('/details/<api_version>')
class AllSavedTickers(Resource):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db = SessionContext()
        self.use_case = PortfolioManageUseCase(portfolio_dao=self.db)

    @get_login_from_token
    def get(self, **kwargs):
        """Описание портфелей"""
        api_version = kwargs.get('api_version')
        data = self.use_case.get_details(api_version)
        self.db.session_close()
        return api_response(data=data, **kwargs)


@api.route('/', defaults={'login': None, 'portfolio_id': None}, doc=False)
@api.route('/<int:portfolio_id>', doc=False)
class Portfolio(Resource):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db = SessionContext()
        self.use_case = PortfolioManageUseCase(portfolio_dao=self.db)

    def __del__(self):
        self.db.commit_and_close()

    @get_login_from_token
    def get(self, **kwargs):
        """Описание портфелей"""
        portfolio_data = self.use_case.get_portfolio(kwargs.get('portfolio_id'))
        return api_response(data=portfolio_data, **kwargs)

    @api.expect(PortfolioDto.master_portfolio)
    @get_login_from_token
    def post(self, **kwargs):
        """Описание портфелей"""
        portfolio_id = self.use_case.create_portfolio(api.payload)
        return api_response(data={"portfolio_id": portfolio_id}, **kwargs)

    @get_login_from_token
    def put(self, **kwargs):
        """Описание портфелей"""
        portfolio_data = self.use_case.update_portfolio(kwargs['portfolio_id'], api.payload)
        return api_response(data=portfolio_data, **kwargs)


@api.route('/tickers/<int:portfolio_id>', defaults={'ticker': None}, doc=False)
@api.route('/tickers/<int:portfolio_id>/<ticker>', doc=False)
class PortfolioTickers(Portfolio):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @get_login_from_token
    def get(self, **kwargs):
        """Описание портфелей"""
        portfolio_data = self.use_case.get_portfolio_tickers(kwargs['portfolio_id'], ticker=kwargs['ticker'])
        return api_response(data=portfolio_data, **kwargs)

    @get_login_from_token
    def put(self, **kwargs):
        """Описание портфелей"""
        portfolio_data = self.use_case.update_tickers(kwargs['portfolio_id'],
                                                      kwargs['ticker'],
                                                      api.payload,
                                                      )
        return api_response(data=portfolio_data, **kwargs)

    @get_login_from_token
    def post(self, **kwargs):
        """Описание портфелей"""
        portfolio_data = self.use_case.save_tickers(api.payload)
        return api_response(data=portfolio_data, **kwargs)

    @get_login_from_token
    def delete(self, **kwargs):
        """Описание портфелей"""
        portfolio_data = self.use_case.delete_tickers(kwargs['portfolio_id'],
                                                      kwargs['ticker'],
                                                      )
        return api_response(data=portfolio_data, **kwargs)


@api.route('/tickers/all/<int:portfolio_id>', doc=False)
class SavedTickersById(Portfolio):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @get_login_from_token
    def get(self, **kwargs):
        """Описание портфелей"""
        data = self.use_case.get_tickers_meta_data(kwargs['portfolio_id'])
        return api_response(data=data, **kwargs)


@api.route('/meta/', doc=False)
class PortfolioMetaData(Portfolio):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @get_login_from_token
    def get(self, **kwargs):
        """Описание портфелей"""
        data = self.use_case.get_portfolios_meta_data()
        return api_response(data=data, **kwargs)


@api.route('/client/<portfolio_id>')
@api.route('/client')
class PortfolioShowcase(Resource):

    @get_login_from_token
    def get(self, **kwargs):
        """Описание портфелей"""

        with SessionContext() as db:
            use_case = PortfolioHomePageUseCase(login=kwargs['k_login'],
                                                portfolio_id=kwargs.get('portfolio_id'),
                                                portfolio_dao=db)

            data = use_case.execute()

        return api_response(data=data, **kwargs)
