from app.domain.entities.business.portfolio import Position
from app.domain.entities.business.pnl_loader import PnlLoader
from app.domain.entities.plain import Ticker, CommonLimit
from app.domain.interfaces import IPortfolioDAO
from app.domain.constants import LANDING_PORTFOLIOS, CoreError, VIRTUAL_CASH, REAL_PORTFOLIO_ID
from app.domain.responses import SuccessResponse, ErrorResponse
from app.domain.usecases import PortfolioUseCase
from app.domain.tools import math_available


class PortfolioSubscriptionUseCase(PortfolioUseCase):
    def __init__(self, portfolio_id: int, login: str, payload: dict, portfolio_dao: IPortfolioDAO) -> None:
        super().__init__(portfolio_id, login, portfolio_dao)

        self.pnl = PnlLoader(db=self.db)
        self.master_portfolio_id = payload['portfolio_id']

        self.master_positions = None
        self.limits = []
        self.current_db_positions = self.db.get_current_positions(portfolio_id=0, client_id=self.client_id)
        self.cash_for_strategy = int(payload.get('cash', 0))
        self.replace_position = payload.get('replace', False)

        if self.master_portfolio_id in LANDING_PORTFOLIOS:
            self.replace_position = True

        #  TODO: remove ?
        self.tariff_id = payload.get('tariff_id', 1)

    def has_positions_in_other_strategy(self):
        """
        У клиента не могут быть одновременно подключеные стратегии, в которых есть повторяющиеся бумаги
        """
        result = False
        tickers_for_new_strategy = [
            t['ticker'] for t in self.db.get_portfolio_tickers(portfolio_id=self.master_portfolio_id)
        ]

        for ticker, position in self.current_db_positions.items():
            if position['ticker'] in tickers_for_new_strategy \
                    and position['portfolio_id'] not in (REAL_PORTFOLIO_ID, self.master_portfolio_id):
                result = True
                break
        return result

    def set_portfolio_start_state(self):
        self.portfolio.cash = self.cash_for_strategy
        self.portfolio.cash_deposit = self.cash_for_strategy
        self.portfolio.asset = self.cash_for_strategy
        self.portfolio.start_asset = self.cash_for_strategy

    def add_positions_to_portfolio(self):
        """
            1. Создаем позиции из эталонной структуры стратегии
            2. Создаем позиции из остатков на счете, если их нет в сохраненных позициях или в эталонной структуре
            3. Создаем позицию по виртуальному кэшу
        """

        # 1. Создаем позиции из остатков на счете, если их нет в сохраненных позициях или в эталонной структуре
        for ticker, position in self.master_positions.items():
            self.portfolio.add_position(position)

        # 2. Создаем позиции из остатков на счете, если их нет в сохраненных позициях или в эталонной структуре
        for limit in self.limits:
            ticker = limit['ticker']
            if ticker not in self.master_positions and ticker not in self.current_db_positions:
                self.portfolio.add_position(
                    Position(ticker=ticker,
                             meta=Ticker(**limit, ticker_type=limit['limit_type']),
                             limit=CommonLimit(**limit),
                             portfolio=self.portfolio))
            # Если позиция уже добавлена в портфель - обновляем фактический лимит
            elif limit['ticker'] in self.master_positions:
                balance = limit['nom_part']
                position = self.portfolio.positions[ticker]

                if math_available([balance, position.price, position.shares_in_lot]):
                    position.nom_part = balance / position.shares_in_lot
                    position.real_part_cost = balance * position.price

        # 3. Создаем позицию по виртуальному кэшу
        self.portfolio.add_position(
            Position(ticker=VIRTUAL_CASH,
                     meta=self.db.get_ticker(VIRTUAL_CASH),
                     limit=CommonLimit(nom_part=1, last_close=1),
                     portfolio=self.portfolio))

    def execute(self):
        """
        Подключение портфеля.
        В этом сценарии выполняются следующие действия:
            1. Запрашиваются данные БД (клиент, остатки, текущие стратегии)
            2. Проверяется возможность подключения:
                2.1. Соответствие риск-профиля клиента риск-профилю портфеля
                2.2. Наличие данных об остатках на счету
                2.3. Наличие ранее подключенных стратегий с аналогичными инструментами
            3. Если запрощенный портфель уже был подлючен - существующие позициии и состояние удаляются.
            4. Удаляются данные о позициях под личным управлением
            5. Запрашивается актуальный состав стратегии с последними ценами
            6. Формируется список позиций, которые требуется записать
            7. Если проставлен размер средств под управление - создаем заявки и для каждой позиции в стратегии
               проставляем "рекомендованный размер позиции в лотах"
            8. Рассчитываем итоговое состояние подключаемого портфеля (доля акций и кэша, процент соответствия,
               размер депозита)
            9. Сохраняем позиции, состояние и, опционально, заявки.
        """
        # 0. Проверка на существование пользователя
        if not self.portfolio.client:
            return ErrorResponse(error=CoreError.USER_UNKNOWN)

        # 1. Запрашиваем остатки на счетах
        self.limits = self.db.get_quik_limits(client_id=self.portfolio.client_id,
                                              client_code=self.portfolio.client.client_code)

        # 2. Проверяется возможность подключения:
        if len(self.limits) < 1:
            return ErrorResponse(error=CoreError.NO_QUIK_LIMITS)

        if self.portfolio.risk_is_not_compatible():
            return ErrorResponse(error=CoreError.RISK_PROFILE_INCOMPATIBLE)

        if self.has_positions_in_other_strategy():
            return ErrorResponse(error=CoreError.PORTFOLIO_NOT_ALLOWED_TO_SUBSCRIBE)

        # 3. Если запрощенный портфель уже был подлючен - существующие позициии и состояние удаляются.
        self.db.clear_pnl_tables(self.portfolio.client_id, self.portfolio.portfolio_id)

        # 4. Удаляются данные о позициях под личным управлением
        self.db.clear_pnl_tables(self.portfolio.client_id, REAL_PORTFOLIO_ID)

        # 5. Запрашивается актуальный состав стратегии с последними ценами
        self.master_positions = self.pnl.get_strategy_state(portfolio_id=self.master_portfolio_id)

        # 6. Формируется список позиций, которые требуется записать:
        self.add_positions_to_portfolio()

        # 7. Проставляем размер капитала
        self.set_portfolio_start_state()

        # 8. Если проставлен размер средств под управление - создаем заявки
        if self.cash_for_strategy > 0:
            self.portfolio.create_bids(client_import=True)

        # 9. Сохраняем позиции в БД
        self.db.create_positions(self.portfolio.positions)

        # 10. Рассчитываем итоговое состояние подключаемого портфеля
        self.portfolio.calc_current_pnl(days_ago=1, cash_deposit=self.cash_for_strategy, tariff_id=self.tariff_id)

        self.portfolio.set_client_allowed()

        return SuccessResponse()
