import pytest
from datetime import datetime

from app.domain.constants import Currencies, TickerTypes
from app.domain.constants import REAL_PORTFOLIO_ID, CoreError
from app.domain.responses import SuccessResponse
from app.domain.usecases.api.portfolio_subscribed import PortfolioSubscriptionUseCase


@pytest.fixture
def replace_positions(replace_positions_value):
    """ Return tab name from passed params """
    return replace_positions_value


@pytest.fixture
def master_portfolio_id(master_portfolio_value):
    """ Return master_portfolio_value  from passed params """
    return master_portfolio_value


class TestPortfolioAddCashUseCaseMain:

    @pytest.fixture(autouse=True)
    def _get_context(self, db_mock, portfolio_id, test_login, new_asset, replace_positions,
                     client_risk_level, test_ticker, master_portfolio_id, app_client):
        ticker_price = 200
        self._db = db_mock
        db_mock.get_client_risk_profile.return_value = client_risk_level

        db_mock.get_or_create_client.return_value = (1, app_client)
        db_mock.get_quik_limits.return_value = [
            {'ticker': Currencies.SUR.value, 'nom_part': 4000, 'limit_type': TickerTypes.MONEY, 'shares_in_lot': 1,
             'last_close': 1},
            {'ticker': test_ticker, 'nom_part': 5, 'limit_type': TickerTypes.SHARES, 'shares_in_lot': 1,
             'last_close': ticker_price},
        ]
        db_mock.check_current_portfolio.return_value = [], []
        db_mock.get_tickers.return_value = [{'ticker': test_ticker, 'isin': test_ticker, 'shares_in_lot': 1,
                                             'currency': Currencies.RUR.value, 'type': TickerTypes.SHARES,
                                             'portfolio_id': portfolio_id, 'current_vol': 0.2, 'hardcode_part': 0.5,
                                             'vol_update_time': datetime.today()}]

        db_mock.get_quotes.return_value = [{
            'classcode': test_ticker,
            'securcode': 'TQBR',
            'tradedate': datetime.today(),
            'high': ticker_price,
            'low': ticker_price,
            'open': ticker_price,
            'close': ticker_price,
            'ticker': test_ticker,
            'usd_close': 70.2
        }]
        db_mock.get_tariffs.return_value = [{'currency': 'RUR', 'commission': 0.001},
                                            {'currency': 'USD', 'commission': 0.001}]

        self._use_case = PortfolioSubscriptionUseCase(login=test_login,
                                                      portfolio_id=REAL_PORTFOLIO_ID,
                                                      portfolio_dao=db_mock,
                                                      payload={
                                                          'portfolio_id': master_portfolio_id,
                                                          'cash': new_asset,
                                                          'replace': replace_positions
                                                      }
                                                      )

    @pytest.mark.parametrize('new_asset, replace_positions, client_risk_level, master_portfolio_id',
                             [(0, False, -1, 25)])
    def test_risk_is_incompatible(self, new_asset, replace_positions, client_risk_level, master_portfolio_id):
        data = self._use_case.execute()
        assert data.error == CoreError.RISK_PROFILE_INCOMPATIBLE

    @pytest.mark.parametrize('new_asset, replace_positions, client_risk_level, master_portfolio_id',
                             [(0, False, 1, 26)])
    def test_not_allowed_to_subscribe_portfolio_with_same_positions(self, new_asset, replace_positions,
                                                                    client_risk_level, master_portfolio_id,
                                                                    test_ticker):
        self._db.get_portfolio_tickers.return_value = [{'ticker': test_ticker}]
        data = self._use_case.execute()
        assert data.error == CoreError.PORTFOLIO_NOT_ALLOWED_TO_SUBSCRIBE

    @pytest.mark.parametrize('new_asset, replace_positions, client_risk_level, master_portfolio_id', [
        (0, False, 1, 25),
        (0, False, 1, 164),
        (0, True, 1, 25)
    ])
    def test_execute_is_ok(self, new_asset, replace_positions, client_risk_level, master_portfolio_id):
        data = self._use_case.execute()
        assert isinstance(data, SuccessResponse)

    @pytest.mark.parametrize('new_asset, replace_positions, client_risk_level, master_portfolio_id',
                             [(0, True, 1, 25)])
    def test_no_limits(self, new_asset, replace_positions, client_risk_level, master_portfolio_id):
        self._db.get_quik_limits.return_value = []
        data = self._use_case.execute()
        assert data.error == CoreError.NO_QUIK_LIMITS

    @pytest.mark.parametrize('new_asset, replace_positions, client_risk_level, master_portfolio_id', [
        (10000, True, 1, 25)
    ])
    def test_execute_is_ok_with_cash(self, new_asset, replace_positions, client_risk_level, master_portfolio_id):
        data = self._use_case.execute()
        assert isinstance(data, SuccessResponse)
