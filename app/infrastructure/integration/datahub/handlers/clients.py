from app.config import logger
from app.infrastructure.db.infra_dao import SessionContext
from app.infrastructure.helpers import store_clients_from_catalog

CLIENT_BUTCH_SIZE = 1000


def process():
    with SessionContext() as db_session:
        client_count = db_session.get_client_count()
        total_processed = 0
        total_created = 0

        while total_processed < client_count:
            clients = db_session.get_clients_slice(total_processed, CLIENT_BUTCH_SIZE)
            login_list = [client.login for client in clients]
            total_created += store_clients_from_catalog(login_list, db_session)
            total_processed += len(login_list)

    logger.info(f'catalog_job_results: total: {total_processed}, created: {total_created}')
