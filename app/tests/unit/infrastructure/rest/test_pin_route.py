from pytest import fixture, mark
from unittest.mock import patch
from app.settings import APP_URL_PREFIX

PIN_ENDPOINTS = ['addPIN', 'confirmSMS', 'resendSMS', 'checkPin', 'deletePin', 'havePin']


class TestPinRoute:

    @fixture(autouse=True)
    def _get_context(self, client):
        self.client = client

    @mark.parametrize('pin_service_url', PIN_ENDPOINTS)
    @patch('app.infrastructure.rest.routes.pin.r')
    @patch('app.infrastructure.rest.routes.pin.get_test_access_token')
    @patch('app.infrastructure.rest.tools.SessionContext')
    def test_confirm_sms_is_ok(self, db_mock, token_mock, requests_mock, pin_service_url):
        token_mock.return_value = (1, 'test_access_token', 1)
        requests_mock.post.return_value.json.return_value = {'data': 'ok'}
        resp = self.client.post(f'{APP_URL_PREFIX}/pin/{pin_service_url}', data={'pin': 123})
        assert resp.status_code == 200
