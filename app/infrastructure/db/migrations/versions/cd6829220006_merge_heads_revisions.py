"""merge heads revisions

Revision ID: cd6829220006
Revises: e4831b3dc5d2, df13f4c00466
Create Date: 2020-06-09 16:11:33.046731

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cd6829220006'
down_revision = ('e4831b3dc5d2', 'df13f4c00466')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
