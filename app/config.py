import sys
import logging.handlers

from app.settings import LOG_LEVEL

# ---------------- Logging ------------------------------------------------------------
level = logging.getLevelName(LOG_LEVEL.upper())
if not isinstance(level, int):
    print(f'Unknown log level: {level}')
    print('Exiting...')
    exit(1)

logger = logging.getLogger()
logger.setLevel(level)
default_handler = logging.StreamHandler(sys.stdout)
default_handler.setFormatter(
    logging.Formatter('%(asctime)s %(levelname)s %(process)d --- [%(thread)d] %(name)s : %(message)s'))
logger.addHandler(default_handler)

rabbit_logger = logger
proxy_logger = logger
