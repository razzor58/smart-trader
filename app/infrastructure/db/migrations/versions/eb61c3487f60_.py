"""empty message

Revision ID: eb61c3487f60
Revises: fd85b81b0c16
Create Date: 2020-03-29 00:31:12.124852

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'eb61c3487f60'
down_revision = 'fd85b81b0c16'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('clients_profiles', sa.Column('filling_datetime', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('clients_profiles', 'filling_datetime')
    # ### end Alembic commands ###
