import os
import logging
from datetime import datetime
from app.infrastructure.db.domain_dao import SessionContext
from app.domain.entities.business.portfolio import PortfolioExt

cron_logger = logging.getLogger('clavis_cron')
cron_logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

fh1 = logging.FileHandler(os.path.join('logs', 'cron.log'))
fh1.setLevel(logging.DEBUG)
fh1.setFormatter(formatter)
cron_logger.addHandler(fh1)

db = SessionContext()


if __name__ == '__main__':
    cron_logger.info("==== Start process === ")
    days_ago = 1
    start_time = datetime.now()

    # Все портфели портфели
    portfolio_list = db.get_portfolio_list()
    ok_cnt = 0
    err_cnt = 0
    for portfolio in portfolio_list:
        try:
            cron_logger.info("calc for portfolio_id: {0} client_id: {1}".format(
                portfolio['portfolio_id'],
                portfolio['client_id']
            ))
            client_id = portfolio['client_id']
            portfolio_id = portfolio['portfolio_id']
            p = PortfolioExt(portfolio_id=portfolio_id, client_id=client_id)
            trade_data = db.get_pnl_track(portfolio_id=portfolio_id, days_ago=days_ago, client_id=client_id)
            p.calc_pnl(trade_data=trade_data, days_ago=days_ago)
            ok_cnt += 1
        except Exception as e:
            cron_logger.error(e)
            err_cnt += 1

    exec_time = datetime.now() - start_time
    cron_logger.info("====  End process. Updated: {0} Failed: {1} exec_time: {2} ====".format(
        ok_cnt, err_cnt, exec_time)
    )
