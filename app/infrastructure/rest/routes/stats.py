from flask_restplus import Resource
from flask_restplus import marshal
from flask_restplus import reqparse

from app.config import logger
from app.infrastructure.rest.schemas import parse_form_params, StatDto
from app.infrastructure.rest.tools import get_login_from_token
from app.infrastructure.db.domain_dao import SessionContext
from app.domain.usecases.api.stats_show import StatsShowUseCase

api = StatDto.stat_api
_token = StatDto.token

parser = reqparse.RequestParser()
parser.add_argument('after', type=int, location='args',
                    help='Последняя запись, после которой возвращается список записей'
                    )
parser.add_argument('div_mode', type=str, location='args', default='expected', help='Тип запрошенных дивидендов')


@api.route('/<string:tab_name>', defaults={'portfolio_id': None})
@api.route('/<int:portfolio_id>/<string:tab_name>')
class GetStat(Resource):

    @api.doc(parser=parser)
    @get_login_from_token
    def post(self, portfolio_id, tab_name, **kwargs):
        """Статистика по портфелю для вкладок"""
        # Если portfolio_id нет в location - берем из query_string
        if portfolio_id is None:
            portfolio_id = parse_form_params('portfolioId')

        login = kwargs.get('k_login')
        params = parser.parse_args()

        with SessionContext() as db:
            use_case = StatsShowUseCase(portfolio_id=portfolio_id,
                                        login=login,
                                        portfolio_dao=db,
                                        )

            data = use_case.execute(tab_name=tab_name,
                                    after=params.get('after'),
                                    div_mode=params.get('div_mode'),
                                    portfolio_id=portfolio_id,
                                    )
            resp_formats = {
                'positions': StatDto.positions_output,
                'bids': StatDto.stats_bids_output,
                'bids_old': StatDto.positions_output,
                'trade_history': StatDto.positions_output,
                'dividends': StatDto.positions_output,
                'stats': StatDto.positions_output
            }
            resp = marshal(data, resp_formats[tab_name])
            logger.info(f"ans {login}: {resp}")

            return resp, 200
