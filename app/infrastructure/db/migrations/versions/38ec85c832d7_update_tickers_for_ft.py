"""update tickers for FT

Revision ID: 38ec85c832d7
Revises: 7a3de80a63cc
Create Date: 2020-06-26 14:11:37.252801

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '38ec85c832d7'
down_revision = '7a3de80a63cc'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("delete from tickers where type='futures';")
    op.execute("delete from tickers where ticker = 'RU000A0JXPG2' and (isin is null or title is null);")


def downgrade():
    pass
