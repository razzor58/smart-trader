from datetime import date

from app.domain.entities.business.trader import Trade


class TestTrader:
    def test_creating(self):
        trade = Trade(
            "direction",
            80,
            date(2020, 1, 13),
            **{
                "ticker": "XXX",
                "price": "123",
            }
        )
        assert str(trade) == "XXX d:2020-01-13v:0.8p: 123i: 0q: 80"
