"""dip_643

Revision ID: 4e55fb25246b
Revises: 6e561aab7e54
Create Date: 2020-06-08 12:52:07.492331

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4e55fb25246b'
down_revision = '6e561aab7e54'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("update signals_rais set hardcode_part=1 where portfolio_id in (162,163,164);")


def downgrade():
    pass
