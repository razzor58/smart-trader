from app.domain.interfaces import IPortfolioDAO


class PortfolioPageManageUseCase:
    def __init__(self, portfolio_dao: IPortfolioDAO):
        self.db = portfolio_dao

    def get_portfolio_pages(self, page_id):
        return self.db.get_portfolio_pages(page_id)

    def create_portfolio_page(self, values):
        return self.db.create_portfolio_page(values)

    def delete_portfolio_page(self, page_id):
        return self.db.delete_portfolio_page(page_id)
