import unittest
import requests as r
from . import NEW_ASSET_SIZE
from . import PORTFOLIO_ID, SERVICE_URL


class TestStatPostions(unittest.TestCase):
    """    Метод отправки пакетки в ProxyFix

   """
    resp = None

    def setUp(self):
        method = '/portfolios/add_cash/{portfolio_id}'.format(portfolio_id=PORTFOLIO_ID)
        url = SERVICE_URL + method
        message = {
            "new_asset_size": NEW_ASSET_SIZE
        }

        self.resp = r.post(url, json=message).json()

    def test_add_cash_is_valid(self):
        test_result = isinstance(self.resp, dict)
        self.assertTrue(test_result)

    def test_add_cash_success(self):
        """
            {
              "data": null,
              "success": true,
              "error": null
            }
        """
        try:
            self.assertTrue(self.resp['success'])
        except Exception:
            print(self.resp)
            raise


if __name__ == '__main__':
    unittest.main()
