from flask_restplus import Resource
from app.infrastructure.rest.tools import get_login_from_token
from app.infrastructure.rest.tools import api_response
from app.infrastructure.rest.schemas import RequestDto
from app.infrastructure.db.domain_dao import SessionContext
from app.domain.usecases.api.bid_request import BidRequestUseCase

api = RequestDto.api


@api.route('/')
class ClientBidRequest(Resource):
    @api.expect(RequestDto.model)
    @get_login_from_token
    def post(self, **kwargs):
        """ Торговля произвольынми бумагами без портфеля """
        with SessionContext() as db:
            use_case = BidRequestUseCase(login=kwargs.get('k_login'), portfolio_dao=db)
            result = use_case.execute(api.payload)
        return api_response(data=result.data, error=result.error)
