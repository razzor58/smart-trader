from datetime import datetime
from app.config import logger
from app.domain.constants import Currencies, CurrencyCodes
from app.infrastructure.db.infra_dao import SessionContext
from app.infrastructure.tools import http_request
from app.settings import CALENDAR_API_TOKEN

CALENDAR_USA_API_URL = 'https://api.bcs.ru/calendar/workdaysforeign'
CALENDAR_RUS_API_URL = 'https://api.bcs.ru/calendar/workdays'


def process():
    with SessionContext() as db_session:
        for curr in [Currencies.RUR, Currencies.USD]:
            data = http_request(**{
                'base_url': CALENDAR_USA_API_URL if curr is Currencies.USD else CALENDAR_RUS_API_URL,
                'method': 'GET',
                'params': {
                    'token': CALENDAR_API_TOKEN,
                    'countrycode': CurrencyCodes.USD if curr is Currencies.USD else CurrencyCodes.RUR
                }
            })

            dates_full_list = [datetime.strptime(d['date'], '%d.%m.%Y') for d in data]
            current_year = datetime.now().year
            data_to_add = []
            dates_list = []
            for index, date_item in enumerate(dates_full_list):
                if index == len(dates_full_list) - 1 or date_item.year != current_year:
                    continue

                dates_list.append(date_item)
                data_to_add.append({
                    'dates': date_item,
                    'next_date': dates_full_list[index + 1],
                    'prev_date': dates_full_list[index - 1],
                    'currency': curr.value
                })

            db_session.update_calendar(dates_list, curr.value, data_to_add)
            logger.info(f'Results: dates_updated={len(data_to_add)}, currency={curr.value}, year={current_year}')
