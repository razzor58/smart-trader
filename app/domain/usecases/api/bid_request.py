from typing import Dict
from datetime import datetime
from app.domain.interfaces import IPortfolioDAO
from app.domain.constants import Operation, PositionTypes, BidRecord, REAL_PORTFOLIO_ID
from app.domain.tools import get_bucket_num
from app.domain.responses import SuccessResponse


class BidRequestUseCase:

    def __init__(self, login: str, portfolio_dao: IPortfolioDAO):
        self.db = portfolio_dao
        self.client = self.db.get_client(login)

    def execute(self, request: Dict):
        """ Создание произвольной заявки по запросу """
        bucket_num = get_bucket_num()
        self.db.update_bids({'portfolio_id': REAL_PORTFOLIO_ID,
                             'client_id': self.client.id,
                             'send_status': BidRecord.NEW},
                            {'send_status': BidRecord.ARCHIVE})
        self.db.save_bids([
            {
                'operation': Operation(PositionTypes.LONG).OPEN,
                'create_date': datetime.now(),
                'bid_time': datetime.now().strftime('%Y-%m-%d'),
                'ticker':  item['secur_code'],
                'nominal': item['qty'],
                'send_status': BidRecord.NEW,
                'client_id': self.client.id,
                'portfolio_id': REAL_PORTFOLIO_ID,
                'bucket_num': bucket_num
            }
            for item in request['data']
        ])
        return SuccessResponse()
