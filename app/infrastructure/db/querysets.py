import csv
from datetime import datetime
from sqlalchemy import case, and_, cast, select, union_all
from sqlalchemy import String, Integer, Date, Float, DateTime
from sqlalchemy.sql.expression import desc, literal_column
from sqlalchemy.sql.expression import func
from app.config import logger
from app.domain.constants import SyncStatus, Limits, BidRecord, BidOperation, ProxyFixTradeTypes, ExecStatus
from app.infrastructure.db.meta.tools import get_query_rows
from app.infrastructure.db.meta.models import (
    DepoLimits, MoneyLimits, Tickers, Trades, SignalsRais, Quotes, Dividends, DateOptions, ClientsBids, WorkDays,
    Portfolio, ClientsPortfolios, RiskProfiles, Clients, PortfolioPages, PortfolioTickers, TariffRules, LastQuotes,
    Signals
)


class SessionQueries:
    def __init__(self, session):

        self.session = session

    def get_quik_limits(self, **kwargs):
        client_code = kwargs.get('client_code')

        d = DepoLimits
        m = MoneyLimits
        t = Tickers

        row_num_col_depo = func.row_number().over(partition_by=(d.client_code, d.sec_code),
                                                  order_by=desc(d.create_date)).label('row_num')

        row_num_col_money = func.row_number().over(partition_by=(m.client_code, m.curr_code),
                                                   order_by=desc(m.create_date)).label('row_num')

        query_d = ((self.session
                    .query(d.sec_code, d.current_bal)
                    .add_column(row_num_col_depo)
                    .add_columns(literal_column("'depo'", type_=String).label('limit_type'))
                    .add_columns(t.shares_in_lot)
                    .add_columns(d.awg_position_price.label('last_close'))
                    .filter(d.client_code == client_code)
                    .filter(d.sec_code == t.secur_code)
                    .filter(d.limit_kind == Limits.T2))
                   .from_self()
                   .filter(row_num_col_depo == 1))

        query_m = ((self.session
                    .query(m.curr_code, m.current_bal)
                    .add_column(row_num_col_money)
                    .add_columns(literal_column("'money'", type_=String).label('limit_type'))
                    .add_columns(literal_column("1", type_=Integer).label('shares_in_lot'))
                    .add_columns(literal_column("0", type_=Integer).label('last_close'))
                    .filter(m.client_code == client_code)
                    .filter(m.limit_kind == Limits.T2))
                   .from_self()
                   .filter(row_num_col_money == 1))

        _result = query_d.union_all(query_m).all()
        _cols = ['ticker', 'nom_part', 'rn', 'limit_type', 'shares_in_lot', 'last_close']

        rows = [dict(zip(_cols, u)) for u in _result]

        return rows

    def get_client_home_page(self, login, portfolio_id=None):
        p = Portfolio
        cp = ClientsPortfolios

        rp = RiskProfiles
        cl = Clients
        pp = PortfolioPages
        b = ClientsBids

        # TODO: Replace <func.isnull> to <func.coalesce> during migration on Postgres
        stmt = (self.session.query(
            rp.group_name.label('client_risk_profile'),
            rp.risk_level.label('client_risk_level'),
            cl.id.label('client_id'),
            cl.login.label('client_login'),
            cl.landing_page)
                .select_from(cl)
                .outerjoin(rp, cl.risk_profile == rp.profile_id)
                .filter(cl.login == login)
                ).distinct().subquery()

        active_bids = (self.session.query(
            func.count(b.id).label('bids_cnt'),
            b.client_id,
            b.portfolio_id)
                       .select_from(b)
                       .join(cl, cl.id == b.client_id)
                       .filter(cl.login == login)
                       .filter(b.send_status.in_([BidRecord.NEW, BidRecord.ACCEPT]))
                       ).group_by(b.client_id.name, b.portfolio_id).subquery()

        qs = (self.session.query(
            p.id.label('portfolio_id'),
            p.name,
            p.currency_cash.label('recommended_cash'),
            p.income_avg,
            p.currency,
            p.risk_profile,
            p.risk_level,
            p.iir_enabled,
            case(
                {None: 0},
                value=cp.id,
                else_=1
            ).label('subscribed'),
            func.coalesce(cp.accordance, 0).label('accordance'),
            func.coalesce(cp.start_asset, 0).label('asset'),
            case(
                {None: None},
                value=cp.id,
                else_=func.coalesce(cp.synchronized, True)
            ).label('synchronized'),
            stmt.c.client_risk_profile,
            stmt.c.client_risk_level,
            case([
                (func.coalesce(cp.id, 0) < 1, 0)
            ], else_=1
            ).label('subscribed'),
            case([
                (active_bids.c.bids_cnt > 0, 1),
                (cp.start_asset > 0, 1)
            ], else_=0
            ).label('active')
        ).select_from(p)
              .join(pp, pp.portfolio_id == p.id)
              .join(stmt, stmt.c.landing_page == pp.landing_page)
              .outerjoin(cp, and_(cp.client_id == stmt.c.client_id, cp.portfolio_id == p.id))
              .outerjoin(active_bids, and_(active_bids.c.client_id == cp.client_id,
                         active_bids.c.portfolio_id == cp.portfolio_id))
              .filter(~p.id.in_([28, 29]))

              )
        if portfolio_id:
            qs = qs.filter(p.id == portfolio_id)

        res = get_query_rows(qs)
        return res

    def get_tickers(self, **kwargs):
        # Tables
        pt = PortfolioTickers
        t = Tickers

        # Filters
        portfolio_id = kwargs.get('portfolio_id')
        trade_date = kwargs.get('trade_date')

        # Query
        qs = (self.session.query(
            pt.id,
            pt.portfolio_id,
            pt.ticker,
            pt.hardcode_part,
            pt.valid_from,
            pt.valid_to,
            pt.max_part,
            pt.current_vol,
            pt.vol_update_time,
            pt.show_on_site,
            t.shares_in_lot,
            t.currency,
            t.secur_code,
            t.classcode,
            t.isin,
            t.type,
            t.title,
            t.shortname,
        )
              .filter(pt.portfolio_id == portfolio_id)
              .filter(pt.valid_from <= trade_date)
              .filter(pt.valid_to >= trade_date)
              .filter(pt.ticker == t.ticker)
              .filter(t.classcode.isnot(None))

              )

        return get_query_rows(qs)

    def get_calendar(self, **kwargs):

        # Tables
        t = WorkDays

        # Filters
        trade_date = kwargs.get('trade_date')

        # Query
        qs = (self.session.query(
            t.dates,
            t.prev_date,
            t.next_date,
            t.cur,
        )
              .filter(t.dates >= trade_date)
              .order_by(t.dates)
              )

        return get_query_rows(qs)

    def get_strategy_details(self, **kwargs):
        p = Portfolio
        pt = PortfolioTickers
        t = Tickers
        s = Signals
        lq = LastQuotes
        return get_query_rows(self.session.query(p.id.label('portfolio_id'),
                                                 p.name,
                                                 p.tariff,
                                                 p.owner,
                                                 p.status,
                                                 pt.ticker,
                                                 pt.current_vol,
                                                 pt.hardcode_part,
                                                 t.type,
                                                 t.title,
                                                 s.vol,
                                                 s.acc_vol,
                                                 s.upload_flag,
                                                 s.signal,
                                                 s.operation,
                                                 s.position_type,
                                                 func.to_char(s.time, 'YYYY-MM-DD').label('time'),
                                                 func.to_char(lq.tradedate, 'YYYY-MM-DD').label('trade_date'),
                                                 lq.close)
                              .select_from(p)
                              .join(pt, pt.portfolio_id == p.id)
                              .join(t, t.ticker == pt.ticker)
                              .outerjoin(s, s.short_code == t.ticker)
                              .outerjoin(lq, lq.short_code == t.ticker)
                              .filter(p.status == 1)
                              .order_by(p.id))

    def get_quotes(self, **kwargs):

        # Tables
        t = LastQuotes

        # Filters
        trade_date = kwargs.get('trade_date')
        ticker_list = kwargs.get('ticker_list')

        # Query
        qs = (self.session.query(
            t.classcode,
            t.securcode,
            t.tradedate,
            t.high,
            t.low,
            t.open,
            t.close,
            t.short_code.label('ticker'),
            t.usd_close,
            func.dense_rank().over(partition_by=[t.securcode],
                                   order_by=[desc(t.tradedate)]).label('row_number')
        ).filter(t.short_code.in_(ticker_list))
              .filter(t.tradedate <= trade_date)).cte()

        main_query = (self.session.query(qs)
                          .filter(qs.c.row_number == 1)
                          .all())

        res = get_query_rows(main_query)
        return res

    def get_tariffs(self, **kwargs):

        # Tables
        t = TariffRules
        p = Portfolio

        # Filters
        trade_date = kwargs.get('trade_date')
        portfolio_id = kwargs.get('portfolio_id')

        # Query
        qs = (self.session.query(
            t.currency,
            t.commission,
            p.id,
        )
              .filter(t.tariff_id == p.tariff_id)
              .filter(t.date_from <= trade_date)
              .filter(t.date_to >= trade_date)
              .filter(p.id == portfolio_id)
              )
        return get_query_rows(qs)

    def get_volumes(self, **kwargs):
        # Tables
        t = SignalsRais

        # Filters
        trade_date = kwargs.get('trade_date')
        portfolio_id = kwargs.get('portfolio_id')

        # Query
        qs = (self.session.query(
            t.ticker,
            t.operation,
            t.vol,
            t.acc_vol,
            t.acc_position,
            t.change_position,
            t.signal_time,
            t.hardcode_part,
            t.position_type,
            t.portfolio_id,
            func.dense_rank().over(partition_by=t.ticker,
                                   order_by=[desc(t.signal_time), desc(t.id)]
                                   ).label('row_number')
        )
              .filter(t.portfolio_id == portfolio_id)
              .filter(t.signal_time <= trade_date)
              ).cte()

        main_query = (self.session.query(qs)
                      .filter(qs.c.row_number == 1)
                      .all())

        res = get_query_rows(main_query)

        return res

    @staticmethod
    def define_add_operation(new_nominal, row, side):

        if (new_nominal < 0 and int(side) == ProxyFixTradeTypes.BUY) or \
                (new_nominal > 0 and int(side) == ProxyFixTradeTypes.SELL):
            operation = BidOperation.get_opposite(row.operation)
        else:
            operation = row.operation
        return operation

    def get_date_options(self, **kwargs):
        # Tables
        t = DateOptions
        # Filters
        trade_date = kwargs.get('trade_date')
        isin_list = kwargs.get('isin_list')

        # Base query
        base_query = self.session.query(
            t.isin,
            t.tradedate,
            t.currentFacevalue,
            t.accruedInterest,
            func.dense_rank().over(partition_by=[t.isin],
                                   order_by=[desc(t.tradedate)]
                                   ).label('row_number')
        ).filter(t.isin.in_(isin_list))

        qs = base_query.filter(cast(t.tradedate, Date) <= trade_date).cte()
        main_query = (self.session.query(qs)
                      .filter(qs.c.row_number == 1)
                      .all())

        return get_query_rows(main_query)

    def duplicate_bid(self, row, side, cum_qty):

        side_coef = 1 if int(side) == 1 else -1
        new_nominal = int((row.nominal*row.shares_in_lot - side_coef * cum_qty)/row.shares_in_lot)

        if new_nominal != 0:
            return {
                'bucket_num':  row.bucket_num,
                'create_date': datetime.now(),
                'portfolio_id': row.portfolio_id,
                'client_id': row.client_id,
                'ticker': row.ticker,
                'operation': self.define_add_operation(new_nominal, row, side),
                'nominal': new_nominal,
                'shares_in_lot': row.shares_in_lot,
                'bid_time': row.bid_time,
                'send_status': BidRecord.NEW,
            }
        else:
            return None

    def repeat_bid(self, row, values):

        # Если исполненный объем не равен рекомендованному - создаем добавляем новую запись в пакетку
        recommended_size = abs(int(row.nominal * row.shares_in_lot))
        cum_qty = abs(int(values['cumQty'] or 0))

        if recommended_size != cum_qty:
            new_bid = self.duplicate_bid(row, values['side'], cum_qty)
            if new_bid:
                self.session.add(ClientsBids(**new_bid))

        return True

    def update_bids_by_report(self, bid_id, **values):
        try:
            bid_id = int(bid_id)
        except (ValueError, TypeError):
            logger.error(f'Invalid bid_id={bid_id} values={values}')
            return

        leavesQty = values.get('leavesQty') or 0
        exec_status = int(values['exec_status'])

        # Если заявка просто принята Биржей
        if exec_status == ExecStatus.NEW:
            self.session.query(ClientsBids)\
                                 .filter(ClientsBids.id == bid_id)\
                                 .update(dict(send_status=BidRecord.ACCEPT, exec_status=exec_status))

        # Когда заявка отклонена
        elif exec_status == ExecStatus.REJECTED:
            bid_record = self.session.query(ClientsBids).filter(ClientsBids.id == bid_id).first()
            if bid_record:
                self.repeat_bid(bid_record, values)

            self.session.query(ClientsBids)\
                .filter(ClientsBids.id == bid_id)\
                .update(dict(
                    send_status=BidRecord.SEND_ERR,
                    exec_status=exec_status,
                    exec_message=values['exec_message'])
                )

        elif exec_status == ExecStatus.CANCELED:
            bid_record = self.session.query(ClientsBids).filter(ClientsBids.id == bid_id).first()
            if bid_record:
                self.repeat_bid(bid_record, values)

            self.session.query(ClientsBids)\
                .filter(ClientsBids.id == bid_id)\
                .update(dict(
                    send_status=BidRecord.CANCELED,
                    exec_status=exec_status,
                    exec_message=values['exec_message'])
                )

        # Когда заявка частично исполнена
        elif exec_status == ExecStatus.PARTIALLY_FILL:
            self.session.query(ClientsBids) \
                .filter(ClientsBids.id == bid_id) \
                .update(dict(send_status=BidRecord.PART,
                             exec_status=exec_status,
                             exec_message=values['exec_message'],
                             order_num=values['order_num'],
                             trade_num=values['trade_num'],
                             ax_price=values['ax_price'],
                             ex_value=values['ex_value'],
                             side=values['side'],
                             quik_trade_vol=abs(int(values['cumQty'])),
                             )
                        )
        # Когда заявка полностью исполнена
        elif exec_status == ExecStatus.FILL and int(leavesQty) == 0:
            # Находим заявку
            row = self.session.query(ClientsBids).filter(ClientsBids.id == bid_id).first()

            # Если исполненный объем не равен рекомендованному - создаем добавляем новую запись в пакетку
            if row:
                self.repeat_bid(row, values)
                current_ex_value = row.ex_value or 0
            else:
                current_ex_value = 0

            # Накопленный исполненный объем в лотах и деньгах
            cum_val = abs(int(values['cumQty']))
            qum_ex_value = values['ex_value'] + current_ex_value

            self.session.query(ClientsBids) \
                .filter(ClientsBids.id == bid_id) \
                .update(dict(send_status=BidRecord.COMPLETE,
                             exec_status=exec_status,
                             exec_message=values['exec_message'],
                             order_num=values['order_num'],
                             trade_num=values['trade_num'],
                             ax_price=values['ax_price'],
                             side=values['side'],
                             ex_value=qum_ex_value,
                             quik_trade_vol=cum_val,
                             )
                        )

            # Если все заявки, отправленные в рамках пакетки, исполнены - считаем портфель "синхронизированным"
            min_exec_status = self.session.query(func.min(ClientsBids.exec_status)) \
                .filter(ClientsBids.bucket_num == row.bucket_num) \
                .scalar()

            if min_exec_status == ExecStatus.FILL:
                self.session.query(ClientsPortfolios)\
                                     .filter(ClientsPortfolios.client_id == row.client_id)\
                                     .filter(ClientsPortfolios.portfolio_id == row.portfolio_id)\
                                     .update(dict(synchronized=SyncStatus.SYNCHRONIZED))

    # TODO: в отдельный проект Backtest
    def get_track(self, **kwargs):

        # Читаем параметры
        trade_date = kwargs.get('trade_date')
        ticker_list = kwargs.get('ticker_list')
        portfolio_id = kwargs.get('portfolio_id')

        # Таблицы откуда запрашиваем
        s = SignalsRais
        q = Quotes
        d = Dividends
        o = DateOptions
        t = Tickers

        # Объединение таблиц
        j = d.outerjoin(q, and_(d.short_code == q.short_code, d.settlement_date == q.tradedate))
        j1 = q.join(t, q.short_code == t.ticker)
        j2 = j1.outerjoin(o, and_(t.isin == o.isin, q.tradedate == o.tradedate))

        # Signals join options
        j3 = s.outerjoin(o, and_(s.code == o.isin, cast(s.signal_time, Date) == o.tradedate))

        # Сделки
        trade_data = select([
            cast(cast(s.signal_time, Date), DateTime).label('tradedate'),
            s.ticker,
            cast(s.price, Float).label('price'),
            cast(s.price, Float).label('rur_price'),  # TODO: RUR price should be correct for ALL master_portfolio_table
            case(
                [(and_(s.vol > 0, s.operation.in_(['close long', 'open short'])), (-1) * s.vol)],
                else_=s.vol
            ).label("vol"),
            s.acc_vol,
            s.operation,
            s.position_type,
            s.hardcode_part.label('max_part'),
            literal_column("NULL", type_=String).label('usd_close'),
            literal_column("NULL", type_=String).label('close'),
            literal_column("NULL", type_=String).label('divs'),
            literal_column("'trade'", type_=String).label('type'),
            literal_column("'pnl'", type_=String).label('vol_type'),
            literal_column("1", type_=Integer).label('order_rank'),
            o.currentFacevalue,
            o.accruedInterest,
        ]).select_from(j3)\
            .where(s.signal_time >= trade_date)\
            .where(s.ticker.in_(ticker_list))\
            .where(s.portfolio_id == portfolio_id)\
            .where(s.position_type != 'short') \
            .where(s.vol.isnot(None))

        # Котировки
        eod = select([
            q.tradedate,
            q.short_code.label('ticker'),
            q.close.label('price'),
            case(
                {"NASDAQ_BEST": cast(q.close * q.usd_close, Float),
                 "NYSE_BEST": cast(q.open * q.usd_close, Float)},
                value=q.classcode,
                else_=q.close
            ).label("rur_price"),
            literal_column("NULL", type_=String).label('vol'),
            literal_column("NULL", type_=String).label('acc_vol'),
            literal_column("NULL", type_=String).label('operation'),
            literal_column("NULL", type_=String).label('position_type'),
            literal_column("NULL", type_=String).label('hardcode_part'),
            q.usd_close,
            q.close,
            literal_column("NULL", type_=String).label('divs'),
            literal_column("'eod'", type_=String).label('type'),
            literal_column("'pnl'", type_=String).label('vol_type'),
            literal_column("3", type_=Integer).label('order_rank'),
            o.currentFacevalue,
            o.accruedInterest,
        ]).select_from(j2).where(q.tradedate >= trade_date) \
            .where(q.short_code.in_(ticker_list))

        # Дивиденды
        divs = select([
            d.settlement_date.label('tradedate'),
            d.short_code.label('ticker'),
            literal_column("NULL", type_=String).label('price'),
            literal_column("NULL", type_=String).label('rur_price'),
            literal_column("NULL", type_=String).label('vol'),
            literal_column("NULL", type_=String).label('acc_vol'),
            literal_column("NULL", type_=String).label('operation'),
            literal_column("NULL", type_=String).label('position_type'),
            literal_column("NULL", type_=String).label('hardcode_part'),
            q.usd_close,
            literal_column("NULL", type_=String).label('close'),
            d.divs,
            literal_column("'dvd'", type_=String).label('type'),
            literal_column("'pnl'", type_=String).label('vol_type'),
            literal_column("2", type_=Integer).label('order_rank'),
            literal_column("NULL", type_=String).label('currentFacevalue'),
            literal_column("NULL", type_=String).label('accruedInterest'),
        ]).select_from(j).where(d.settlement_date >= trade_date)\
            .where(d.short_code.in_(ticker_list))\

        # Объединение всех таблиц
        sub_q = union_all(trade_data, eod, divs).alias('a2')
        query = select([sub_q]).order_by("tradedate").order_by("order_rank")

        # Результат
        rows = self.session.query(query).all()
        res = get_query_rows(rows)

        return res

    # TODO: в отдельный проект Backtest
    def export_to_csv(self):
        portfolios = {
            3: {'cash': 10000, 'name': 'US1'},
            4: {'cash': 10000, 'name': 'US2'},
            6: {'cash': 16000, 'name': 'US3'},
            25: {'cash': 100000, 'name': 'RU1'},
            26: {'cash': 200000, 'name': 'RU2'},
            27: {'cash': 500000, 'name': 'RU3'},
        }

        t = Trades
        i = Tickers
        # Скачиваем данные
        for portfolio_id, ref in portfolios.items():
            cash = ref['cash']
            name = ref['name']
            qs = (self.session.query(
                t.open_date,
                t.ticker,
                case(
                    {"open long": "B",
                     "close long": "S"},
                    value=t.direction,
                    else_=None
                ).label("operation"),
                t.market_price.label('price'),
                (t.quantity*i.shares_in_lot).label('quantity')
            )
                  .filter(t.portfolio_id == portfolio_id)
                  .filter(t.ticker == i.ticker)
                  .filter(t.client_id == 1)

                  ).order_by(t.open_date)
            res = get_query_rows(qs)

            with open(f'trades_{name}.tsv', 'w', newline='') as csvfile:
                writer = csv.writer(csvfile, delimiter='\t')
                writer.writerow(['Баланс на открытии', cash])
                writer.writerow([])
                writer.writerow(['Дата/время сделки', 'Инструмент', 'Направление', 'Цена', 'Количество'])
                for row in res:
                    writer.writerow([
                        row['open_date'].strftime('%Y-%m-%d %H:%M:%S'),
                        row['ticker'],
                        row['operation'],
                        round(row['price'], 2),
                        abs(row['quantity'])
                    ])
