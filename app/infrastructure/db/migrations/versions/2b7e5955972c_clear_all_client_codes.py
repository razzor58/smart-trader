"""clear_all_client_codes

Revision ID: 2b7e5955972c
Revises: a33b611ed03f
Create Date: 2020-07-02 17:51:21.000709

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2b7e5955972c'
down_revision = 'a33b611ed03f'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("update clients set client_code=NULL;")


def downgrade():
    pass
