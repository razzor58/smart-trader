"""update_tickers_current_vol

Revision ID: 7384dbe9c2e4
Revises: a1dfbd5673f3
Create Date: 2020-06-17 13:49:47.951439

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '7384dbe9c2e4'
down_revision = 'a1dfbd5673f3'
branch_labels = None
depends_on = None


def upgrade():
    op.execute('''update portfolio_tickers set hardcode_part = 1, current_vol = signals_rais.acc_vol
        from signals_rais
        where portfolio_tickers.portfolio_id = signals_rais.portfolio_id
        and portfolio_tickers.ticker = signals_rais.ticker
        and signals_rais.portfolio_id in (162,163,164);
        ''')


def downgrade():
    pass
