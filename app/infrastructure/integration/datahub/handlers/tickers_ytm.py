from datetime import datetime
from app.config import logger as log
from app.infrastructure.tools import KeyCloak, Realms, http_request
from app.infrastructure.db.infra_dao import SessionContext

RU_DATA_DATE_YTM_URLS = 'https://dh2.efir-net.ru/v2/Bond/CalculateBondMulti'


def process():
    with SessionContext() as db_session:
        tickers_updated = 0
        ticker_list = db_session.get_ticker_list_for_ytm()
        yield_type = {'both': 0, 'worst': 1, 'mat': 2}
        items_list = [{'id': v['isin'], 'value': v['close'], 'valueType': 0} for v in ticker_list]

        body = http_request(**{
            'base_url': RU_DATA_DATE_YTM_URLS,
            'method': 'POST',
            'params': None,
            'json': {
                'date': datetime.today().strftime('%Y-%m-%d'),
                'items': items_list,
                'periods': yield_type['worst'],
                'fields': ['is_offer', 'date', 'yield', 'dur']
            },
            'headers': KeyCloak(Realms.RU_DATA).get_auth_header()
        })

        for item in body:
            if item['dataList']:
                data = item['dataList'][0]
                db_session.update_tickers_data(filter_data={'isin': item['id']},
                                               values={'ytm': data['yield'],
                                                       'offer_date': data['date'],
                                                       'has_offer': data['is_offer'],
                                                       'last_update_time': datetime.now()
                                                       })
                tickers_updated += 1
        log.info(f'Results: tickers_updated={tickers_updated}')
