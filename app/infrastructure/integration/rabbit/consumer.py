# Отдельная служба
from utils import rabbit_logger as log
import threading
import pika
import rabbit_client.processor as processor

# Импорт конфигурации
from utils import config


def process_export(unused_channel, basic_deliver, properties, body):
    p = processor.Processor()
    p.do_cycle(unused_channel, basic_deliver, properties, body)


def data_handler(channel, method, properties, body):

    thread = threading.Thread(target=process_export, args=(channel, method, properties, body))
    thread.start()
    while thread.is_alive():  # Loop while the thread is processing
        channel._connection.sleep(1.0)

    channel.basic_ack(delivery_tag=method.delivery_tag)


def main():

    # # Find config
    # config = ConfigParser()
    # CONF_PATH = Path(Path(__file__).parent, 'config.ini')
    # config.read(CONF_PATH)

    # MQ Connection config to MQ Server
    user = config['rabbitmq']['rabbit_user']
    password = config['rabbitmq']['rabbit_password']
    host = config['rabbitmq']['server_ip']
    port = config['rabbitmq']['server_port']

    # Consumer config
    queue_name = config['rabbitmq']['queue_name']
    exchange = config['rabbitmq']['exchange']
    routing_key = config['rabbitmq']['routing_key']

    # Create connect parameters
    credentials = pika.PlainCredentials(username=user, password=password)
    params = pika.ConnectionParameters(host=host, port=port, virtual_host='/', credentials=credentials, heartbeat=30)

    # Create connection
    connection = pika.BlockingConnection(params)
    channel = connection.channel()

    # Declare queue
    channel.queue_bind(queue=queue_name, exchange=exchange, routing_key=routing_key)
    # channel.queue_declare(queue=queue_name)
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(data_handler, queue_name)

    try:
        log.info("Start")
        channel.start_consuming()
    except Exception as e:
        log.error(e, exc_info=True)
        channel.stop_consuming()
    channel.close()


if __name__ == '__main__':
    main()
