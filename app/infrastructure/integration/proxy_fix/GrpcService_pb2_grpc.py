# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc

from app.infrastructure.integration.proxy_fix import GrpcService_pb2 as GrpcService__pb2, GrpcExec_pb2 as GrpcExec__pb2


class ProxyFixServiceStub(object):
  # missing associated documentation comment in .proto file
  pass

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.CreateMarketOrder = channel.unary_unary(
        '/ProxyFixService/CreateMarketOrder',
        request_serializer=GrpcService__pb2.NewMarketOrderDataRequest.SerializeToString,
        response_deserializer=GrpcService__pb2.NewOrderResponse.FromString,
        )
    self.CreateNewLimitOrder = channel.unary_unary(
        '/ProxyFixService/CreateNewLimitOrder',
        request_serializer=GrpcService__pb2.NewLimitOrderDataRequest.SerializeToString,
        response_deserializer=GrpcService__pb2.NewOrderResponse.FromString,
        )
    self.CreateNewTakeProfitOrder = channel.unary_unary(
        '/ProxyFixService/CreateNewTakeProfitOrder',
        request_serializer=GrpcService__pb2.NewTakeProfitOrderDataRequest.SerializeToString,
        response_deserializer=GrpcService__pb2.NewOrderResponse.FromString,
        )
    self.CreateNewStopLimitMarketOrder = channel.unary_unary(
        '/ProxyFixService/CreateNewStopLimitMarketOrder',
        request_serializer=GrpcService__pb2.NewStopLimitMarketOrderDataRequest.SerializeToString,
        response_deserializer=GrpcService__pb2.NewOrderResponse.FromString,
        )
    self.CreateNewStopLimitOrder = channel.unary_unary(
        '/ProxyFixService/CreateNewStopLimitOrder',
        request_serializer=GrpcService__pb2.NewStopLimitOrderDataRequest.SerializeToString,
        response_deserializer=GrpcService__pb2.NewOrderResponse.FromString,
        )
    self.CreateNewTakeProfitStopLimitOrder = channel.unary_unary(
        '/ProxyFixService/CreateNewTakeProfitStopLimitOrder',
        request_serializer=GrpcService__pb2.NewTakeProfitStopLimitOrderDataRequest.SerializeToString,
        response_deserializer=GrpcService__pb2.NewOrderResponse.FromString,
        )
    self.SubscrReports = channel.unary_stream(
        '/ProxyFixService/SubscrReports',
        request_serializer=GrpcExec__pb2.ReportSubscriptionRequest.SerializeToString,
        response_deserializer=GrpcExec__pb2.Report.FromString,
        )
    self.CancelOrder = channel.unary_unary(
        '/ProxyFixService/CancelOrder',
        request_serializer=GrpcService__pb2.CancelOrderDataRequest.SerializeToString,
        response_deserializer=GrpcService__pb2.NewOrderResponse.FromString,
        )
    self.ReplaceBasicOrder = channel.unary_unary(
        '/ProxyFixService/ReplaceBasicOrder',
        request_serializer=GrpcService__pb2.ReplaceBasicOrderDataRequest.SerializeToString,
        response_deserializer=GrpcService__pb2.NewOrderResponse.FromString,
        )
    self.ReplaceStopOrder = channel.unary_unary(
        '/ProxyFixService/ReplaceStopOrder',
        request_serializer=GrpcService__pb2.ReplaceStopOrderDataRequest.SerializeToString,
        response_deserializer=GrpcService__pb2.NewOrderResponse.FromString,
        )


class ProxyFixServiceServicer(object):
  # missing associated documentation comment in .proto file
  pass

  def CreateMarketOrder(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def CreateNewLimitOrder(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def CreateNewTakeProfitOrder(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def CreateNewStopLimitMarketOrder(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def CreateNewStopLimitOrder(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def CreateNewTakeProfitStopLimitOrder(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def SubscrReports(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def CancelOrder(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def ReplaceBasicOrder(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def ReplaceStopOrder(self, request, context):
    # missing associated documentation comment in .proto file
    pass
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_ProxyFixServiceServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'CreateMarketOrder': grpc.unary_unary_rpc_method_handler(
          servicer.CreateMarketOrder,
          request_deserializer=GrpcService__pb2.NewMarketOrderDataRequest.FromString,
          response_serializer=GrpcService__pb2.NewOrderResponse.SerializeToString,
      ),
      'CreateNewLimitOrder': grpc.unary_unary_rpc_method_handler(
          servicer.CreateNewLimitOrder,
          request_deserializer=GrpcService__pb2.NewLimitOrderDataRequest.FromString,
          response_serializer=GrpcService__pb2.NewOrderResponse.SerializeToString,
      ),
      'CreateNewTakeProfitOrder': grpc.unary_unary_rpc_method_handler(
          servicer.CreateNewTakeProfitOrder,
          request_deserializer=GrpcService__pb2.NewTakeProfitOrderDataRequest.FromString,
          response_serializer=GrpcService__pb2.NewOrderResponse.SerializeToString,
      ),
      'CreateNewStopLimitMarketOrder': grpc.unary_unary_rpc_method_handler(
          servicer.CreateNewStopLimitMarketOrder,
          request_deserializer=GrpcService__pb2.NewStopLimitMarketOrderDataRequest.FromString,
          response_serializer=GrpcService__pb2.NewOrderResponse.SerializeToString,
      ),
      'CreateNewStopLimitOrder': grpc.unary_unary_rpc_method_handler(
          servicer.CreateNewStopLimitOrder,
          request_deserializer=GrpcService__pb2.NewStopLimitOrderDataRequest.FromString,
          response_serializer=GrpcService__pb2.NewOrderResponse.SerializeToString,
      ),
      'CreateNewTakeProfitStopLimitOrder': grpc.unary_unary_rpc_method_handler(
          servicer.CreateNewTakeProfitStopLimitOrder,
          request_deserializer=GrpcService__pb2.NewTakeProfitStopLimitOrderDataRequest.FromString,
          response_serializer=GrpcService__pb2.NewOrderResponse.SerializeToString,
      ),
      'SubscrReports': grpc.unary_stream_rpc_method_handler(
          servicer.SubscrReports,
          request_deserializer=GrpcExec__pb2.ReportSubscriptionRequest.FromString,
          response_serializer=GrpcExec__pb2.Report.SerializeToString,
      ),
      'CancelOrder': grpc.unary_unary_rpc_method_handler(
          servicer.CancelOrder,
          request_deserializer=GrpcService__pb2.CancelOrderDataRequest.FromString,
          response_serializer=GrpcService__pb2.NewOrderResponse.SerializeToString,
      ),
      'ReplaceBasicOrder': grpc.unary_unary_rpc_method_handler(
          servicer.ReplaceBasicOrder,
          request_deserializer=GrpcService__pb2.ReplaceBasicOrderDataRequest.FromString,
          response_serializer=GrpcService__pb2.NewOrderResponse.SerializeToString,
      ),
      'ReplaceStopOrder': grpc.unary_unary_rpc_method_handler(
          servicer.ReplaceStopOrder,
          request_deserializer=GrpcService__pb2.ReplaceStopOrderDataRequest.FromString,
          response_serializer=GrpcService__pb2.NewOrderResponse.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'ProxyFixService', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
