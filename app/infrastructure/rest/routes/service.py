from flask_restplus import Resource

from app.config import logger
from app.infrastructure.rest.schemas import ServiceDto
from app.infrastructure.db.domain_dao import SessionContext
from app.infrastructure.rest.tools import SUCCESS_RESP, api_response
from app.infrastructure.rest.tools import get_login_from_token
from app.infrastructure.rest.schemas import PortfolioDto

from app.domain.usecases.api.portfolio_delete import PortfolioDeleteUseCase
from app.domain.usecases.api.portfolio_subscribed import PortfolioSubscriptionUseCase
from app.domain.usecases.api.portfolio_add_cash import PortfolioAddCashUseCase
from app.domain.constants import CoreError

p_api = PortfolioDto.portfolio_api
api = ServiceDto.service_api


def service_add(login, api):
    with SessionContext() as db:
        use_case = PortfolioSubscriptionUseCase(portfolio_id=api.payload['portfolio_id'],
                                                login=login,
                                                payload=api.payload,
                                                portfolio_dao=db,
                                                )
        result = use_case.execute()

    return api_response(data=result.data, error=result.error)


def service_delete(login, portfolio_id):
    with SessionContext() as db:
        use_case = PortfolioDeleteUseCase(portfolio_id=portfolio_id, login=login, portfolio_dao=db)
        use_case.execute()

    return api_response(data=SUCCESS_RESP)


@api.route('/add')
class Service(Resource):
    @api.expect(ServiceDto.service)
    @get_login_from_token
    def post(self, **kwargs):
        """ Первоначальное создание клиента в структуре RAIS """
        # Берем логин из заголовков
        login = kwargs.get('k_login')
        response = service_add(login, api)
        return response


@api.route('/delete/<int:portfolio_id>/<string:login>')
class ServiceDelete(Resource):
    @get_login_from_token
    def post(self, portfolio_id, login, **kwargs):
        """ Первоначальное создание клиента в структуре RAIS """
        # Берем логин из заголовков
        user_login = login or kwargs.get('k_login')
        logger.info(f'user_login={user_login}')
        response = service_delete(user_login, portfolio_id)
        return response


@api.route('/add/<login>')
class ServiceWithLoginRoute(Resource):
    @api.expect(ServiceDto.service)
    def post(self, login):
        """ Первоначальное создание клиента в структуре RAIS """
        response = service_add(login, api)
        return response


@p_api.route('/add_cash', defaults={'login': None, 'portfolio_id': None})
@p_api.route('/add_cash/<portfolio_id>', defaults={'login': None})
class ClientPortfolioAddCash(Resource):

    @p_api.expect(PortfolioDto.new_asset_size)
    @get_login_from_token
    def post(self, portfolio_id, login, **kwargs):
        """Обновить pnl по клиенту"""
        k_login = login or kwargs['k_login']
        portfolio_id_final = portfolio_id or kwargs['portfolio_id_from_qs']
        if portfolio_id_final:
            with SessionContext() as db:
                use_case = PortfolioAddCashUseCase(login=k_login,
                                                   portfolio_id=int(portfolio_id_final),
                                                   portfolio_dao=db,
                                                   payload=api.payload,
                                                   )
                use_case.execute()
                res = api_response()
        else:
            res = api_response(error=CoreError.PORTFOLIO_UNKNOWN)
        return res
