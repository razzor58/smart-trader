from datetime import datetime
import re
import app.infrastructure.integration.rabbit.handlers as handlers
from app.config import rabbit_logger


class Processor(object):

    def __init__(self):
        self.logger = rabbit_logger

    def do_cycle(self, unused_channel, basic_deliver, properties, body):
        try:
            # Логируем факт начала обработки сообщения
            self.logger.info('received: {0} ,tag: {1} '.format(basic_deliver.routing_key, basic_deliver.delivery_tag))
            start_time = datetime.now()

            # Универсальный обработчик на каждый
            for key in handlers.workers:
                if re.match(key, basic_deliver.routing_key) is not None:
                    obj = handlers.workers[key]
                    obj.process(body=body)
                    exec_time = str(datetime.now() - start_time)
                    self.logger.info("processed: {0}.  exec_time: {1}".format(basic_deliver.delivery_tag, exec_time))
                    break

        except Exception as e:
            self.logger.error(e, exc_info=True)
