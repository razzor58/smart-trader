FROM tiangolo/uwsgi-nginx-flask:python3.6-2020-05-07

RUN apt-get clean && \
    apt-get -y update && \
    apt-get -y install g++ libsnappy-dev

ARG SSH_PRV_KEY

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh && \
    ssh-keyscan gitlab.gitlab.bcs.ru > /root/.ssh/known_hosts
RUN echo ${SSH_PRV_KEY} | base64 -d > /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa

COPY ./requirements.txt .
RUN pip install --proxy=s-proxy-04-g.global.bcs:8080 --no-cache-dir -r ./requirements.txt

RUN rm -rf /root/.ssh/
RUN mkdir -p /app/static && \
    mkdir -p /var/tmp/uwsgi_flask_metrics

COPY clavis.py /app/main.py

COPY ./app /app/app
