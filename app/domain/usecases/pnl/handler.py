import os
import json
import time
from typing import Optional
from datetime import datetime, timedelta
from collections import defaultdict
from app.domain.interfaces import IPortfolioDAO
from app.infrastructure.rest.schemas import json_serial
from app.domain.tools import get_chart_data
from app.domain.entities.business.portfolio import PortfolioExt


class GetPnlUseCase:

    def __init__(self, login: Optional[str], portfolio_dao: IPortfolioDAO, portfolio_id: Optional[int] = None):
        self.db = portfolio_dao
        if login:
            self.client_id, self.client = self.db.get_or_create_client(login)
        self.round_digit = 7
        self.portfolio_id = portfolio_id

        if portfolio_id:
            self.portfolio = PortfolioExt(portfolio_id=portfolio_id, db=self.db, login=login)

    def get_portfolio_stat(self, stat):
        # stat = stat[-1]
        stat_asset = stat[0]['asset']
        end_asset = stat[-1]['asset']
        cash_deposit = stat[-1]['cash_deposit']

        if len(stat) > 1:
            try:
                calc_income = round(((end_asset - cash_deposit) / stat_asset - 1) * 100, self.round_digit)
            except ZeroDivisionError:
                calc_income = 0
        else:
            calc_income = 0

        total_divs = stat[-1]['total_divs'] or 0
        commission_size = stat[-1]['commission_size'] or 0

        # костыль для ДУ
        # TODO: костыльный депозит для ДУ: выпилить когда будет время
        if int(self.client_id) == 2063:
            cash_deposit = round(cash_deposit + stat_asset, 2)
        else:
            cash_deposit = round(cash_deposit, 2)

        res = {
            "asset": round(stat[-1]['asset'], 2),
            "cash": stat[-1]['cash'],
            "shares": stat[-1]['shares'],
            "income": round(calc_income, 2),
            "name": self.portfolio.name,
            "last_eod": stat[-1]['tradedate'],
            "currency": self.portfolio.currency_format,
            "commission_size": round(commission_size, 2),
            "total_divs": round(total_divs, 2),
            "cash_deposit": cash_deposit,
            "portfolio_id": self.portfolio_id
        }
        return res

    def get_du_chart_data(self, stat):
        res = []
        for row in stat:
            dt1 = round(time.mktime((row['tradedate'] + timedelta(hours=4)).timetuple())) * 1000
            value = round(row['income'] * 100, self.round_digit)
            res.append([dt1, value])
        return res

    def get_portfolio_stat_group(self, stat, master_portfolios):
        groups = defaultdict(list)
        group_res = defaultdict(dict)
        for row in stat:
            groups[row['portfolio_id']].append(row)
        for group, data in groups.items():
            group_res[group] = self.get_portfolio_stat(data)

        for group_id, data in group_res.items():
            data.update({
                "name": master_portfolios[group_id].replace('Умный портфель ', '').replace(': ', '_')
            })
        return group_res

    # TODO: move to rest module
    def save_results_to_json(self, results, save_file=False):
        res = json.dumps(results, default=json_serial)
        if save_file:
            fp = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..', 'manager',
                              'chart_data_{}.json'.format(self.portfolio_id))
            with open(fp, 'w') as f:
                f.write(res)

        return res

    @staticmethod
    def group_position_by_portfolio(position, master_portfolios):
        res = defaultdict(dict)

        # на вход приходит либо dict либо list
        if isinstance(position, list):
            position_iterator = [(pos['ticker'], pos) for pos in position]
        else:
            position_iterator = position.items()

        for ticker, position_data in position_iterator:
            ticker_portfolio_id = position_data['portfolio_id']
            if ticker_portfolio_id in master_portfolios:
                group_name = master_portfolios[ticker_portfolio_id]
                res[group_name][ticker] = position_data
        return res

    def get_trades_results(self):
        trades_results = self.portfolio.get_trades()
        res = defaultdict(list)

        for k, v in trades_results.items():
            for trade in v:
                res[k].append(dict((key, val or '') for key, val in trade.items()))

        return res

    @staticmethod
    def format_portfolio_report(stat):
        res = []
        for row in stat:
            margin_short_size = row.get('margin_short_size', 0.000)
            item = (row['tradedate'],
                    round(row['cash'], 2),
                    round(row['cash_deposit'], 2),
                    round(row['shares'], 2),
                    round(row['asset'], 2),
                    round(margin_short_size, 2),
                    )
            res.append(item)
        return res

    def get_report(self):
        return self.db.get_summary_stat()

    def get_client(self, login):
        client = self.db.get_client(login)
        if client:
            return {
                'login': client.login,
                'fullname': client.fullname,
            }
        return {}

    def get_pnl_all(self):
        return self.db.get_client_stat(self.client_id, self.client.client_code, self.client.login)

    def get_pnl(self):
        positions = self.portfolio.get_saved_positions()
        master_structure = self.db.get_master_structure()
        positions_groups = self.group_position_by_portfolio(positions, master_structure)

        trades = self.get_trades_results()

        stat = self.portfolio.get_portfolio_reports()
        bids = self.portfolio.get_portfolio_bids()
        if len(stat) == 0:
            cash = positions.get("CASH", {'cost': 0})['cost']
            stat = [{
                "portfolio_id": int(self.portfolio_id),
                "asset": cash,
                "cash": cash,
                "commission_size": stat[-1]['commission_size'] if len(stat) > 0 else 0,
                "total_divs": stat[-1]['total_divs'] if len(stat) > 0 else 0,
                "cash_deposit": 0,
                "shares": 0,
                "income": 0,
                "tradedate": datetime.now()
            }]

        portfolio_groups = self.get_portfolio_stat_group(stat, master_structure)
        chart_data = get_chart_data(stat)
        portfolio = self.get_portfolio_stat(stat)
        report = self.format_portfolio_report(stat)

        results = {
            'chart_data': chart_data,
            'positions': positions,
            'positions_groups': positions_groups,
            'portfolio_groups': portfolio_groups,
            'trades': trades,
            'clients_bids': bids,
            'portfolio': portfolio,
            'report': report
        }

        return self.save_results_to_json(results)

    def recalc_pnl(self, **kwargs):
        # Читаем сделки из БД
        days_ago = kwargs.get('days_ago', 1)
        trade_data = self.portfolio.pnl.get_pnl_track(portfolio_id=self.portfolio_id,
                                                      client_id=self.client_id,
                                                      days_ago=days_ago)

        # Пересчитываем результаты
        self.portfolio.calc_pnl(trade_data=trade_data, **kwargs)
        self.portfolio.db.session_commit()
        self.portfolio.db.session_close()
