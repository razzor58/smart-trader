from datetime import datetime
from typing import Optional, Any
from app.domain.entities.plain import Ticker, Quotes, PortfolioTicker
from app.domain.entities.plain import CommonLimit, ClientsPositions
from app.domain.constants import Currencies, Operation, TickerTypes, VIRTUAL_CASH, PositionTypes, REAL_PORTFOLIO_ID
from app.domain.tools import create_from_dict, define_bid_operation, get_position_by_balance, get_obj_to_save


class Position:
    def __init__(self,
                 ticker: Optional[str] = None,
                 meta:  Optional[Ticker] = None,
                 position: Optional[dict] = None,
                 limit: Optional[CommonLimit] = None,
                 strategy: Optional[PortfolioTicker] = None,
                 prices: Optional[Quotes] = None,
                 tariffs: Optional[float] = None,
                 portfolio: Optional[Any] = None
                 ):
        # Создаем эксземпляр из сохраненной записи в БД
        if position:
            create_from_dict(self, position)

        # Собираем эксземпляр из эталонной структуры или остатков на счете
        else:
            self.ticker = ticker
            self.meta = meta
            self.ticker_type = meta.type
            self.ticker_currency = meta.currency

            if limit:
                self.nom_part = limit.nom_part
                self.portfolio_id = portfolio.portfolio_id if self.ticker == VIRTUAL_CASH else REAL_PORTFOLIO_ID
                self.real_part_cost = limit.nom_part*limit.last_close
                self.cost = 0
                self.operation = define_bid_operation(PositionTypes.WAIT, limit.nom_part)
                self.price = limit.last_close
                self.shares_in_lot = self.meta.shares_in_lot
                self.position_type = get_position_by_balance(limit.nom_part)
                self.recommend_part = limit.nom_part
                self.client_id = portfolio.client_id
                self.is_signal_active = False

            elif strategy:
                # meta
                self.shares_in_lot = self.meta.shares_in_lot

                # Portfolio fields
                self.strategy = strategy
                self.portfolio_id = strategy.portfolio_id
                self.max_part = strategy.hardcode_part or 1
                self.position_type = get_position_by_balance(strategy.current_vol)
                self.last_signal_time = strategy.vol_update_time or datetime.today()
                self.operation = Operation(PositionTypes(self.position_type)).OPEN
                self.vol = strategy.current_vol
                self.acc_vol = 0
                self.cost = 0
                self.recommend_part = 0
                self.is_signal_active = False
                self.signal_size = 0

                # Prices
                self.tradedate = prices.tradedate
                self.prices = prices
                self.price = self.prices.price
                self.usd_close = self.prices.usd_close
                self.rur_price = self.prices.rur_price

                # Commission
                self.tariffs = tariffs

                # State
                self.status = 0
                self.div_size = 0
                self.fixed_profit = 0
                self.calculate_profit = 0
                self.real_part_cost = 0
                self.nom_part = self.get_nom_part(limit)
                self.avg_open_price = 0
                self.open_date = None
                self.first_trade = True
                self.client_id = None

    @property
    def save_fields(self):
        return get_obj_to_save(self, ClientsPositions)

    def is_not_currency(self):
        return self.ticker not in Currencies.__members__

    def get_rur_price(self):
        if self.ticker.currency == Currencies.USD:
            rur_price = self.prices.close * self.prices.usd_close
        else:
            rur_price = self.prices.close
        return rur_price

    def get_nom_part(self, limit):

        if self.ticker == VIRTUAL_CASH:
            nom_part = 1
        elif limit is None:
            nom_part = 0
        elif self.ticker not in Currencies.__members__:
            nom_part = int(limit.nom_part / self.ticker.shares_in_lot)
        else:
            nom_part = limit.nom_part

        return nom_part

    @staticmethod
    def get_size(meta, strategy):

        # Если клиенту рекомендуется набрать сразу весь объем интрумента (без долевого набора)
        # Всем облигациям ставим hardcode = 1
        # if strategy['acc_position'] == strategy['hardcode_part']:
        #     size = 1

        if meta.type == TickerTypes.BONDS:
            size = strategy.acc_position or 0
        else:
            size = strategy.acc_vol or 0

        # Не пустой объем
        final_size = 0 if size is None else size

        return final_size

    @staticmethod
    def default():
        return {
            'acc_vol': 0,
            'status': 0,
            'open_date': None,
            'nom_part': 0,
            'avg_open_price': 0,
            'div_size': 0,
            'first_trade': True,
            'fixed_profit': 0,
            'calculate_profit': 0,
            'cost': 0,
            'recommend_part': 0,
            'real_part_cost': 0,
        }
