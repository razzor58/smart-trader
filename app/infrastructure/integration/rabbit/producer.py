#!/usr/local/fox/env/bin/python
import pika
import logging

# Импорт конфигурации
from app import settings


class Producer(object):

    def __init__(self, routing_key=None):
        self.log = logging.getLogger('rabbit')

        url = settings.RABBIT_MQ_URL
        self.exchange_name = settings.RABBIT_MQ_EXCHANGE
        self.routing_key = routing_key if routing_key else settings.RABBIT_MQ_ROUTING_KEY

        self.properties = pika.BasicProperties()

        # Create rabbitmq connection
        try:
            self.connection = pika.BlockingConnection(pika.URLParameters(url))
            self.channel = self.connection.channel()

            self.channel.exchange_declare(exchange=self.exchange_name, exchange_type='topic', durable=True)
        except Exception as e:
            self.log.error("Cant connect to Rabbit")
            self.log.error(e, exc_info=True)

    def publish(self, message):
        try:
            self.channel.basic_publish(exchange=self.exchange_name,
                                       routing_key=self.routing_key,
                                       body=message,
                                       properties=self.properties)
            self.log.info(self.routing_key + ': ' + message + ':' + str(self.properties))
        except Exception as e:
            self.log.error(self.routing_key)
            self.log.error(e, exc_info=True)
