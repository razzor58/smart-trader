from unittest.mock import patch, MagicMock
from app.infrastructure.integration.datahub.handlers.clients import process


class TestClientJob:
    @patch('app.infrastructure.helpers.KeyCloak')
    @patch('app.infrastructure.integration.datahub.handlers.clients.SessionContext')
    @patch('app.infrastructure.helpers.http_request')
    def test_job_is_ok(self, http_request, test_db, test_token):
        mo = MagicMock()
        mo.get_client_count.return_value = 1
        mo.get_clients_slice.return_value = [MagicMock()]
        test_db.return_value.__enter__.return_value = mo

        http_request.return_value = {
            'data': {'clients': [
                {'clientId': 'f258b0cd-53e2-4c42-82bb-55bd0f0f47d3', 'klogin': 'k-123123', 'firstName': 'Иван',
                 'middleName': 'Иванович', 'lastName': 'Иванов', 'agreements': [
                    {'agreementId': '3d7e5be6-9ce5-474b-ae47-dbe57914a966', 'agreementNum': '555222/19-м-иис',
                     'leverageType': 'Стандартное', 'isOvernightEnabled': True, 'iiaType': None,
                     'tariffId': 'edd1b06a-771d-40fa-9721-3bbc6134e81d', 'tariffName': 'БКС Директ',
                     'updateDate': '2020-05-13T23:00:06.037+0000', 'tradeCodes': ['TEST12314'],
                     'changes': {'portfolioList': []}}]}
            ]}
        }
        process()
