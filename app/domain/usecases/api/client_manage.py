from app.domain.interfaces import IPortfolioDAO
from app.infrastructure.helpers import store_single_client
from collections import defaultdict


class ClientManageUseCase:

    def __init__(self, db_session: IPortfolioDAO, login: str):
        self.db = db_session
        self.client = self.db.get_client(login)

    def set_default_account(self, login):

        saved_agreements = self.db.get_agreements_data(login)
        unique_agreements_ids = set(row['id'] for row in saved_agreements)
        client_code = set(row['client_code'] for row in saved_agreements).pop()

        if len(unique_agreements_ids) == 1 and client_code is None:
            self.set_active_account(unique_agreements_ids.pop(), flash=True)

    def get_agreements_data(self, login):

        if self.client or store_single_client(login) > 0:
            agreements = defaultdict(dict)
            limits = defaultdict(dict)
            agg_trade_codes = defaultdict(list)

            self.set_default_account(login)

            # Ответ для фронта
            for row in self.db.get_agreements_data(login):
                agg_id = row['id']
                agg_trade_codes[agg_id].append(row['value'])
                agreements[agg_id]['agreement_id'] = agg_id
                agreements[agg_id]['agreement_num'] = row['agreement_num']
                agreements[agg_id]['is_active'] = row['client_code'] in agg_trade_codes[agg_id]
                limits[row['currency']] = row['balance']
                agreements[agg_id]['limits'] = limits

            return [value for key, value in agreements.items()]
        else:
            return {}

    def set_active_account(self, agreement_id, flash=False):

        res = self.db.get_agreements(agreement_id)
        agreement = None
        for agg in res:
            if len(agg['value']) > 3 and agg['value'][0:2] != '82':
                agreement = agg
                break

        if agreement:
            self.db.update_client(
                {'id': self.client.id},
                {'client_code': agreement['value'],
                 'agreement_number': agreement['agreement_num'],
                 'agreement_id': agreement['id']
                 })
            if flash:
                self.db.flush()

    def get_current_portfolios(self):
        return self.db.get_client_portfolios(self.client.id)
