from app.config import logger as log
from app.infrastructure.tools import http_request
from app.infrastructure.db.infra_dao import SessionContext
from app.settings import QUOTES_SERVICE_WITH_DELAY_URL


def process():
    with SessionContext() as db_session:
        values_to_insert = []
        all_portfolio_ticker = db_session.get_ticker_list()
        tickers = dict([(f"{v['classcode'] }|{v['secur_code']}", v['ticker']) for v in all_portfolio_ticker])

        quotes = http_request(**{
            'base_url': QUOTES_SERVICE_WITH_DELAY_URL,
            'method': 'POST',
            'params': None,
            'json': [k for k, v in tickers.items()]
        })

        for r in quotes:
            try:
                price_data = r['info'][0]
                class_code = r['classCode']
                secur_code = r['securCode']
                ticker_close_price = price_data['close']
            except (KeyError, IndexError):
                log.error(f'no price for: class_code={class_code} and secur_code={secur_code}')
                ticker_close_price = None

            if ticker_close_price:
                db_session.delete_last_quotes({
                    'classcode': class_code,
                    'securcode': secur_code
                })
                values_to_insert.append({
                    'classcode': r['classCode'],
                    'securcode': r['securCode'],
                    'tradedate': price_data['tradeDate'],
                    'high': price_data['high'],
                    'low': price_data['low'],
                    'open': price_data['open'],
                    'close': price_data['close'],
                    'lasttrade_date': price_data['lastTradeDate'],
                    'date': price_data['lastTradeDate'],
                    'short_code': tickers[f'{class_code}|{secur_code}'],
                })

        db_session.add_last_quotes(values_to_insert)
        log.info(f'Results: records_updated={len(values_to_insert)}')
