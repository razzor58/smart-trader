from datetime import datetime

from app.domain.interfaces import IPortfolioDAO
from app.domain.usecases import PortfolioUseCase
from app.domain.constants import DATETIME_FORMAT, DATE_FORMAT, Currencies, TickerTypes, REAL_PORTFOLIO_ID
from app.domain.constants import BidOperation
from app.domain.tools import get_stock_name


class TradeShowUseCase(PortfolioUseCase):
    def __init__(self, login: str, portfolio_id: int, portfolio_dao: IPortfolioDAO):
        super().__init__(login, portfolio_id, portfolio_dao)
        self.positions = None
        self.bids = self.get_bids()
        self.tickers = [x['ticker'] for x in self.bids]

        self.tickers.append(Currencies.USD000UTSTOM.value)
        if Currencies.USDRUB.value in self.tickers:
            self.tickers.remove(Currencies.USDRUB.value)

    def get_bids(self):
        """ Возвращаем заявки для подтверждения клиентом"""
        filters = dict(portfolio_id=self.portfolio_id, client_id=self.client_id)

        if self.portfolio_id == REAL_PORTFOLIO_ID:
            filters['without_balance'] = True

        return self.db.get_last_bids(**filters)

    def get_currency_limit(self):
        return dict([(limit['curr_code'], limit['current_bal'])
                     for limit in self.portfolio.db.get_money_limits(
                         self.portfolio.client.client_code)])

    def execute(self, prices):
        operation_data = {
            'buy': {
                'sum': 0,
                'items': []
            },
            'sell': {
                'sum': 0,
                'items': []
            },
        }

        # Получаем шаблоны пакетных заявок
        bids = self.bids

        bonds = dict([(x['ticker'], x['isin']) for x in bids if x.get('type') == 'bonds'])

        total_buy = 0
        total_sell = 0

        # Считаем параметры пакетки
        if len(bids) == 0:
            bucket_num = '0'
            msg = 'Нет активных заявок для отправки'
        else:
            # Берем цену облигаций
            bonds_costs = self.portfolio.pnl.get_bonds_cost(prices, bonds)

            bucket_num = str(bids[0]['bucket_num'])
            msg = "Данные для пакетной заявки"

            for order in bids:

                # Размер пакетки формируем как разницу между рекомендованным и исполненным объемом
                order_size = abs(order['nominal'])  # - quik_trade_vol/order['shares_in_lot']

                ticker = order['ticker']
                shares_count = order['shares_count']

                # Если в портеле есть валюта USDRUB - в котировках у нее другой тикер
                prices_key = Currencies.USD000UTSTOM.value if ticker == Currencies.USDRUB.value else ticker

                # Текущая время и цена инструмента, которые прищли из API
                price = prices[prices_key]['price']
                price_time = datetime.fromtimestamp(prices[prices_key]['time']).strftime(DATETIME_FORMAT)

                order_cur = order['currency']
                rur_price = price if order['currency'] == Currencies.RUR.value else \
                    price * prices.get(Currencies.USD000UTSTOM.value, {'price': 0})['price']

                # TODO: handle exception?
                if order['type'] == TickerTypes.BONDS:
                    bond_price = bonds_costs.get(ticker, 0)
                    rur_cost = shares_count * bond_price
                    cost = rur_cost

                else:
                    rur_cost = round(shares_count * rur_price, 3)
                    cost = round(shares_count * price, 3)
                    bond_price = 0

                side = BidOperation.side_is(order['operation'])
                dev_prc = order['price_deviation'] / 100
                price_deviation = 1 - dev_prc if side == 'sell' else 1 + dev_prc

                # Собираем данные для каждой позиции ИИР
                item = {
                    "ticker": ticker,
                    "code": order['code'],
                    "isin": order['isin'],
                    "currency": order_cur,
                    "currentBalance": order['current_balance'],
                    "short_description": order['short_description'],
                    "title": order['title'],
                    "sharesCount": abs(shares_count),
                    "nominal": order_size,
                    "type": order['type'],
                    "side": side,
                    "operation": order['operation'] or "open long",
                    "cost": abs(cost),
                    "rurCost": rur_cost,
                    "price": round(price, 2),
                    "rurPrice": rur_price,
                    "price_bonds": bond_price,
                    "rurPrice_bonds": bond_price,
                    "priceTime": price_time,
                    "priceDeviation": price_deviation,
                    "changeEnabled": True,
                    "dev_prc": dev_prc,
                    "tradeOrganization": get_stock_name(order['classcode']),
                }

                if order['operation'] in ('open long', 'close short'):
                    operation_data['buy']['items'].append(item)
                    total_buy += rur_cost
                else:
                    operation_data['sell']['items'].append(item)
                    total_sell += rur_cost

        # Итоги
        operation_data['sell']['sum'] = round(abs(total_sell), 2)
        operation_data['buy']['sum'] = round(total_buy, 2)

        # currency_limit - свободные ДС в валюте портфеля
        currency_limit = self.get_currency_limit()
        agg_date = self.portfolio.client.agreement_date or datetime.now()
        profile = self.db.get_client_profile_details(self.client_id)

        try:
            filling_datetime = profile.filling_datetime.strftime(DATETIME_FORMAT) if profile else ''
            profile_name = profile.profile_name if profile else ''
        except Exception:
            filling_datetime = ''
            profile_name = ''

        # Собираем данные для полноценные данные для дисклеймера
        meta_data = {
            "date": datetime.now().strftime(DATETIME_FORMAT),
            "client": self.portfolio.client.fullname or '',
            "client_profile": profile_name,
            "profile_fill_date": filling_datetime,
            "contract_date": filling_datetime,
            "agreementNumber": self.portfolio.client.agreement_number,
            "agreementDate": agg_date.strftime(DATE_FORMAT),
            "tradingCode": self.portfolio.client.client_code,
            "currencyLimitRUR": currency_limit[Currencies.SUR.value],
            "currencyLimitUSD": currency_limit[Currencies.USD.value],
            "num": bucket_num,
            "bid_time": datetime.now().strftime(DATETIME_FORMAT),
            "msg": msg
        }
        operation_data.update(meta_data)
        self.portfolio.db.session_close()
        return operation_data
