import pytest
from app.domain.constants import BidStatus
from app.domain.entities.business.portfolio import PortfolioExt

testdata = [
    ([2, 2, 2], BidStatus.EXECUTED),
    ([2, 2, None], BidStatus.PART_EXECUTED),
    ([1, 2, 2], BidStatus.PART_EXECUTED),
    ([2, None, None], BidStatus.PART_EXECUTED),
    ([1, 2, 2], BidStatus.PART_EXECUTED),
    ([0, 8, 8], BidStatus.ACCEPTED),
    ([5, 8, 8], BidStatus.ACCEPTED),
    ([5, 0, 8], BidStatus.ACCEPTED),
    ([8, None, None], BidStatus.ACCEPTED),
    ([None, None, None], BidStatus.ACCEPTED),
    ([8, 8, 8], BidStatus.NOT_EXECUTED),
    ([8, 8, 'trash'], BidStatus.NOT_EXECUTED),
]


class TestPortfolioBidStatus:
    @pytest.mark.parametrize('statuses,expected', testdata)
    def test_bid_status(self, statuses, expected):
        assert PortfolioExt.get_bid_status(statuses) == expected
