from app.domain.entities.business.portfolio import PortfolioExt
from app.config import logger


class ExporterService(object):
    """Обрабатывает сделки и лимиты из экспортера"""

    @staticmethod
    def apply_quik_trade_from_kafka(quik_trade, db_session):
        """ Меняем состав портфеля когда пришла сделка с Экспортера"""

        # Берем данные клиента
        logger.info("apply: {}".format(quik_trade))
        is_trade_new = db_session.check_trade(trade_num=quik_trade['trade_num'])

        # Если сделка новая
        if is_trade_new:

            client = db_session.get_client_by_code(client_code=quik_trade['client_code'])
            # Если клиент подключен к системе
            if client:

                client_id = client.id

                # Находим соотвествующую позицию
                position = db_session.get_positions_by_client_id(client_id, quik_trade['sec_code'])

                if position is None:
                    logger.error("QUIK trade can not be applied. client_id: {0}, sec_code: {1}".format(
                        client_id, quik_trade['sec_code'])
                    )
                else:
                    portfolio_id = position.portfolio_id

                    # Находим портфель в БД
                    portfolio = PortfolioExt(
                        portfolio_id,
                        db_session,
                        '',
                        client_id=client_id,
                        get_db_position=True
                    )
                    portfolio.init_trader(with_trades_history=True)

                    # Конвертируем поля
                    ticker = quik_trade['sec_code']
                    tradedate = quik_trade['trade_date']
                    portfolio_id = portfolio.positions[ticker].portfolio_id

                    replace_params = {
                        'ticker': ticker,
                        'vol': quik_trade['qty'],
                        'tradedate': quik_trade['trade_date'],
                        'price': float(quik_trade['price']),
                        'shares_in_lot': portfolio.positions[ticker].shares_in_lot,
                        'portfolio_id': portfolio_id,
                        'commission': 0.001
                    }
                    quik_trade.update(replace_params)

                    # выполняем сделку QUIK
                    return_trade = portfolio.trader.execute_quik_trade(**quik_trade, return_trade=True)

                    # обновляем позиции
                    db_session.update_positions(portfolio.positions)

                    # обновляем статистику
                    portfolio.update_asset_values(tradedate=tradedate)
                    portfolio.accordance = portfolio.calculate_accordance()
                    portfolio.save_db_state()

                    # сохраняем историю сделок
                    db_session.save_trade(return_trade.to_dict())
                    db_session.session_commit()

    @staticmethod
    def apply_quik_limit_from_kafka(quik_limit, db_session):
        """ Обновление данных о сводобных ДС портфеля, когда пришли лимиты с Экспортера"""
        # Если клиент подключен к системе
        client = db_session.get_client_by_code(client_code=quik_limit['client_code'])

        if client:
            db_session.update_limit_from_exporter(
                client_id=client.id,
                curr_code=quik_limit['curr_code'],
                current_bal=quik_limit['current_bal'],
                event_time=quik_limit['event_time'],
            )
