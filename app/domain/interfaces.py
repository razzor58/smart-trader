import abc
from typing import List, Tuple, Dict, Any, Optional
from app.domain.constants import ResponsesTypes, Landings
from app.domain.entities.plain import ClientsPortfolio, Portfolio, ClientsPositions, Client, ClientsBids
from app.domain.entities.plain import PortfolioReport, Trade, Ticker


class IPortfolioDAO(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def flush(self):
        pass

    @abc.abstractmethod
    def create_portfolio(self, portfolio: ClientsPortfolio) -> None:
        pass

    @abc.abstractmethod
    def create_master_portfolio(self, portfolio: dict) -> None:
        pass

    @abc.abstractmethod
    def check_current_portfolio(self, client_id: int) -> Tuple[List[int], List[str]]:
        pass

    @abc.abstractmethod
    def create_positions(self, positions: List[ClientsPositions]) -> None:
        pass

    @abc.abstractmethod
    def create_portfolio_tickers(self, data: dict) -> None:
        pass

    @abc.abstractmethod
    def clear_pnl_tables(self, client_id: int, portfolio_id: int) -> None:
        pass

    @abc.abstractmethod
    def get_all_from_table(self, table_type: str) -> List[Dict]:
        pass

    @abc.abstractmethod
    def get_client_home_page(self, login: str, portfolio_id: Optional[int]) -> List[Dict]:
        pass

    @abc.abstractmethod
    def get_portfolio_origin(self, portfolio_id: Optional[int], as_dict: Optional[bool]) -> Portfolio:
        pass

    @abc.abstractmethod
    def get_portfolio_tickers(self, portfolio_id: int, ticker: Optional[str]) -> List[Dict]:
        pass

    @abc.abstractmethod
    def get_portfolios_tickers_with_prices(self, landing_page: Landings) -> List[Any]:
        pass

    @abc.abstractmethod
    def get_quik_limits(self, client_id: int, client_code: str) -> List[Dict]:
        pass

    @abc.abstractmethod
    def get_or_create_client(self, login: str) -> Client:
        pass

    @abc.abstractmethod
    def get_client_risk_profile(self, client_id: int) -> int:
        pass

    @abc.abstractmethod
    def get_ticker(self, ticker: str) -> Ticker:
        pass

    @abc.abstractmethod
    def get_current_state(self, client_id: int, portfolio_id: int) -> ClientsPortfolio:
        pass

    @abc.abstractmethod
    def get_depo_limit(self, client_id: int) -> Dict[str, int]:
        pass

    @abc.abstractmethod
    def get_bid(self, client_id: int, portfolio_id: int, ticker: str) -> ClientsBids:
        pass

    @abc.abstractmethod
    def get_trades(self, portfolio_id: Optional[int], client_id: int, status: Optional[int]) -> Dict[str, Dict]:
        pass

    @abc.abstractmethod
    def get_portfolio_bids(self, client_id: int, portfolio_id: int) -> List[Dict]:
        pass

    @abc.abstractmethod
    def get_bid_by_id(self, bid_id: int) -> ClientsBids:
        pass

    @abc.abstractmethod
    def get_current_positions(self, portfolio_id: Optional[int], client_id: int, **kwargs) -> Dict[str, Dict]:
        pass

    @abc.abstractmethod
    def get_portfolio_stat(self, portfolio_id: Optional[int], client_id: int, **kwargs) -> List[PortfolioReport]:
        pass

    @abc.abstractmethod
    def save_portfolio_results(self,
                               report: Optional[dict],
                               report_list: Optional[List[dict]]
                               ) -> None:
        pass

    @abc.abstractmethod
    def save_trade(self, trade: Trade) -> None:
        pass

    @abc.abstractmethod
    def save_trade_bulk(self, trades: List) -> None:
        pass

    @abc.abstractmethod
    def save_rows(self, table: str, rows: List[Dict]) -> None:
        pass

    @abc.abstractmethod
    def set_current_state(self, state: dict) -> None:
        pass

    @abc.abstractmethod
    def update_position_fields(self, entity_filter: Dict, values: Dict) -> None:
        pass

    # TODO: typing error if use PortfolioExt.positions instead Any ?
    @abc.abstractmethod
    def update_positions(self, positions: Any):
        pass

    @abc.abstractmethod
    def update_bid_template(self, template_id: int, **kwargs) -> None:
        pass

    @abc.abstractmethod
    def update_master_portfolio(self, portfolio_id: int, portfolio: dict) -> None:
        pass

    @abc.abstractmethod
    def update_portfolio_tickers(self, portfolio_id: int, ticker: str, data: dict) -> None:
        pass

    @abc.abstractmethod
    def get_portfolio_list(self, portfolio_id: int):
        pass

    @abc.abstractmethod
    def get_master_structure(self) -> Dict:
        pass

    @abc.abstractmethod
    def get_client_stat(self, client_id: int, client_code: str, login: str) -> Dict:
        pass

    @abc.abstractmethod
    def get_summary_stat(self) -> Dict:
        pass

    @abc.abstractmethod
    def get_date_options(self, trade_date, isin_list) -> Dict:
        pass

    @abc.abstractmethod
    def get_money_limits(self, client_code: str) -> Dict:
        pass

    @abc.abstractmethod
    def get_client(self, login: str) -> Any:
        pass

    @abc.abstractmethod
    def update_bids(self, filters: Dict, values: Dict):
        pass

    @abc.abstractmethod
    def save_bids(self, param: List[Dict]):
        pass

    @abc.abstractmethod
    def get_agreements_data(self, login: str) -> List[Dict]:
        pass

    @abc.abstractmethod
    def update_client(self, filters: Dict, values: Dict):
        pass

    @abc.abstractmethod
    def get_agreements(self, agreement_id: int) -> List[Dict]:
        pass

    @abc.abstractmethod
    def get_client_portfolios(self, client_id: int) -> List[Dict]:
        pass

    @abc.abstractmethod
    def get_strategy_details(self) -> List[Dict]:
        pass


class IResponse(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def get_type(self) -> ResponsesTypes:
        pass


class IConfig(metaclass=abc.ABCMeta):

    @property
    @abc.abstractmethod
    def open_hour(self):
        return NotImplementedError

    @property
    @abc.abstractmethod
    def close_hour(self):
        return NotImplementedError

    @property
    @abc.abstractmethod
    def close_minutes(self):
        return NotImplementedError

    @property
    @abc.abstractmethod
    def close_seconds(self):
        return NotImplementedError

    @property
    @abc.abstractmethod
    def record_to_page(self):
        return NotImplementedError
