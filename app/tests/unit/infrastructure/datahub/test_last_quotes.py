from unittest.mock import patch, MagicMock
from app.infrastructure.integration.datahub.handlers.last_quotes import process


class TestLastQuotesJob:
    @patch('app.infrastructure.integration.datahub.handlers.last_quotes.SessionContext')
    @patch('app.infrastructure.integration.datahub.handlers.last_quotes.http_request')
    def test_job_is_ok(self, http_request, test_db):
        mo = MagicMock()
        mo.get_ticker_list.return_value = [{'secur_code': 'YNDX', 'classcode': 'TQBR', 'ticker': 'YNDX'}]
        test_db.return_value.__enter__.return_value = mo
        http_request.return_value = [{'classCode': 'TQBR', 'securCode': 'YNDX', 'profit': 0.3, 'monthClose': 2630.4,
                                      'yearClose': 2425.0,
                                      'info': [{'tradeCount': 159438, 'tradeDate': '2020-05-19T00:00:00Z',
                                                'open': 2975.2,
                                                'close': 2953.8, 'high': 2980.0, 'low': 2923.6,
                                                'lastTradeDate': '2020-05-19T14:41:00Z',
                                                'firstTradeDate': '2020-05-19T09:59:00Z',
                                                'value': 470223011.8}
                                               ]
                                      }]
        process()
