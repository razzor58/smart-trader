from unittest.mock import patch
from app.infrastructure.integration.datahub.handlers.signals import process


class TestSignalsJob:

    @patch('app.infrastructure.integration.datahub.handlers.signals.SessionContext')
    @patch('app.infrastructure.integration.datahub.handlers.calendar.http_request')
    def test_job_is_ok(self, http_request, test_db):
        http_request.return_value = [{'signal': 'sell', 'vol': 0.1, 'acc_vol': 0.1, 'operation': 'close long',
                                      'bar': {'open': 76.49, 'close': 76.42, 'hi': 76.96, 'lo': 76.31,
                                              'time': 1594083600, 'volume': 0,
                                              'Signal': 'wait', 'analyzed_id': 12}, 'price': 76.42}]
        process()
