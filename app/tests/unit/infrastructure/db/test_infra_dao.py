from app.infrastructure.db.infra_dao import SessionContext
from unittest.mock import patch
from pytest import fixture


class TestDomainDao:

    @fixture(autouse=True)
    @patch('app.infrastructure.db.meta.engine.Session')
    def _get_context(self, mock_session):
        self.session = SessionContext()

    def test_create_client_data(self):
        data = {'reqId': '70df36ee-a536-49a4-a715-047607e40051',
                'data': {'clients': [
                    {'clientId': '5b979aa3-5fd6-4ee4-ae19-1c96d2b2c8a1', 'klogin': 'k-242424', 'firstName': 'Михаил',
                     'middleName': 'Михайлович', 'lastName': 'Михайлов',
                     'agreements': [
                         {'agreementId': 'c16c2593-fe44-4719-abab-5c00d9ebf1ac', 'agreementNum': '626262/19-мтс-иис',
                          'isOvernightEnabled': True, 'iiaType': None,
                          'tariffId': '395c7e14-a128-11e9-80cd-086266806d7e',
                          'tariffName': 'Трейдер', 'updateDate': '2020-06-19T22:05:59.170+0000',
                          'tradeCodes': ['556677']},
                         {'agreementId': 'af0f023b-7a94-4fc9-bf33-66cfec5fa6ba', 'agreementNum': '175175/15-мтл',
                          'isOvernightEnabled': True, 'iiaType': None,
                          'tariffId': '395c7e14-a128-11e9-80cd-086266806d7e',
                          'tariffName': 'Трейдер', 'updateDate': '2020-06-19T22:05:58.940+0000',
                          'tradeCodes': ['188888', '8200y8x']}]}]}}

        self.session.create_or_update_clients_data_if_not_exists(data['data']['clients'][0])
