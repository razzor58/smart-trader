import unittest
import requests as r
from . import SERVICE_URL


class TestPostTrades(unittest.TestCase):
    """    Метод отправки пакетки в ProxyFix """

    resp = None

    def setUp(self):
        method = '/trades/26'
        url = SERVICE_URL + method
        self.trade_template = r.get(url).json()

    def test_send_to_fix(self):
        method = '/trades/26'
        url = SERVICE_URL + method
        message = {
            "num": self.trade_template['data']['num'],
            "data": self.trade_template['data']
        }
        self.resp = r.post(url, json=message).json()
        all_resp_is_ok = True
        try:
            for order_id, fix_resp in self.resp['data']['details'].items():

                if fix_resp.get('send_status', 'FAILED') != 'OK':
                    all_resp_is_ok = False
        except Exception:
            print(self.resp)

        self.assertTrue(all_resp_is_ok)


if __name__ == '__main__':
    unittest.main()
