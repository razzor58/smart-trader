import pytest
from app.domain.usecases.api.client_manage import ClientManageUseCase

AGREEMENTS_LIST = [
    {'login': 'k-213213', 'client_code': 'TEST003', 'id': 4, 'agreement_num': 'test/agg', 'value': 'TEST003',
     'balance': 766404.71, 'currency': 'SUR'},
    {'login': 'k-213213', 'client_code': None, 'id': 4, 'agreement_num': 'test/agg', 'value': 'TEST003',
     'balance': 766404.71, 'currency': 'SUR'},
    {'login': 'k-213213', 'client_code': None, 'id': 4, 'agreement_num': 'test/agg',
     'value': 'TEST003', 'balance': 0.0, 'currency': 'USD'},
    {'login': 'k-213213', 'client_code': None, 'id': 6, 'agreement_num': 'asdad/asda',
     'value': 'TEST002', 'balance': 1000000.0, 'currency': 'SUR'}
]

test_data = [
    (AGREEMENTS_LIST[0:1], True),
    (AGREEMENTS_LIST[1:2], False),
    (AGREEMENTS_LIST[1:], False)
]


class TestClientManageUseCase:

    @pytest.mark.parametrize('agreements_list, is_active_result', test_data)
    def test_get_agreements_data_is_ok(self, db_mock, test_login, agreements_list, is_active_result):
        db_mock.get_agreements_data.return_value = agreements_list

        use_case = ClientManageUseCase(login=test_login, db_session=db_mock)
        data = use_case.get_agreements_data(test_login)
        is_active = set([item['is_active'] for item in data]).pop()
        ok = is_active is is_active_result
        assert ok
