from app.domain.usecases.pnl.handler import GetPnlUseCase
from app.domain.entities.plain import Client


class TestGetPnlUseCase:

    def test_uc_get_pnl_all(self, db_mock, test_login):
        db_mock.get_or_create_client.return_value = (1, Client({'client_code': 'test', 'login': test_login}))
        db_mock.get_client_stat.return_value = {}
        use_case = GetPnlUseCase(login=test_login, portfolio_dao=db_mock)
        data = use_case.get_pnl_all()
        assert isinstance(data, dict)

    def test_uc_get_report(self, db_mock):
        db_mock.get_summary_stat.return_value = {}
        use_case = GetPnlUseCase(login=None, portfolio_dao=db_mock)
        data = use_case.get_report()
        assert isinstance(data, dict)
