from datetime import datetime
from app.domain.interfaces import IPortfolioDAO
from app.domain.entities.business.portfolio import PortfolioExt
from app.domain.responses import SchedulerResponse
from app.domain.entities.plain import ScheduleResult


class CreateBidsForAllUseCase:

    def __init__(self, portfolio_dao: IPortfolioDAO):
        self.db = portfolio_dao

    def update_template_stat(self, template_id, **values):
        self.db.update_bid_template(template_id=template_id, **values)

    def execute(self, portfolio_id, template_id):
        date_start = datetime.now()
        # Все подключенные портфели портфели
        portfolio_list_raw = self.db.get_portfolio_list(portfolio_id=portfolio_id)

        # Исключаем мастер - портфели
        portfolio_list = [p for p in portfolio_list_raw if p['client_id'] not in (0, 1)]
        total = len(portfolio_list)

        # Фиксируем общее число заявок для отправки
        self.update_template_stat(template_id, total=total)

        # Создаем пакетные заявки по каждому клиенту, подключенному к указанному портфелю
        success_counter = 0
        error_counter = 0

        for portfolio in portfolio_list:
            try:
                portfolio_id = portfolio['portfolio_id']
                client_id = portfolio['client_id']
                ps_new = PortfolioExt(portfolio_id=portfolio_id,
                                      client_id=client_id,
                                      db=self.db,
                                      get_db_position=True)
                ps_new.create_bids()
                success_counter += 1
                self.update_template_stat(template_id, complete=success_counter)
            except Exception:
                error_counter += 1

        exec_time = datetime.now() - date_start

        return SchedulerResponse(data=ScheduleResult(exec_time, success_counter, error_counter, total))
