from app.domain.interfaces import IPortfolioDAO
from app.domain.entities.business.portfolio import PortfolioExt


class ApplyReportUseCase:

    def __init__(self, portfolio_dao: IPortfolioDAO):
        self.db = portfolio_dao

    def execute(self, execution_report):
        """ Меняем состав портфеля как только прилетел отчет от биржи
         {
              'internalNum': '58',
              'clOrdId': '1sd541w9d',
              'execRepForBasicOrder': {
                'execRep': {
                  'ordType': '1',
                  'ordStatus': '2'
                },
                'execRepDetailNormalOrder': {
                  'msgType': '8',
                  'handlInst': '1',
                  'ordStatus': '2',
                  'execType': 'F',
                  'orderQty': 10.0,
                  'cumQty': 10.0,
                  'lastQty': 10.0,
                  'clOrdId': '1sd541w9d',
                  'symbol': 'Аэрофлот',
                  'idSource': '8',
                  'securityId': 'AFLT',
                  'side': '1',
                  'ordType': '1',
                  'avgPx': 136.02,
                  'execTransType': '0',
                  'orderId': '190807-TQBR-17930873327',
                  'execId': 'TQBR-5giKR-B-1-1-N',
                  'trdMatchId': '190807-TQBR-84061859',
                  'lastPx': 136.02,
                  'account': 'L01-00000F00',
                  'currency': 'RUB',
                  'clientId': 'TEST003',
                  'tradeNum': '84061859',
                  'transactTime': '20190807-08:38:10.497',
                  'tradeDate': '20190807',
                  'orderNum': '17930873327',
                  'exValue': 13602.0,
                  'exUserId': 'NU0058900010',
                  'dealSettlementCode': 'Y2',
                  'tradeType': 'T',
                  'commType': '3',
                  'timeInForce': '0',
                  'futSettDate': '20190809',
                  'securityExchange': 'TQBR',
                  'traderId': 'NU0058900010',
                  'currCode': 'RUB',
                  'userId': '99469',
                  'tradeTimeGMT': '-3:00:00',
                  'firmId': 'NC0058900000',
                  'tradingSessionSubId': '3',
                  'requestId': '17',
                  'lastLiquidityInd': 2
                }
              }
            }
        """
        bid_id = execution_report['clOrdId']

        # Берем данные из нашего ордера
        bid_details = self.db.get_bid_by_id(bid_id=bid_id)

        # Если нашли
        if bid_details:
            # Читаем поля
            portfolio_id = bid_details.portfolio_id
            client_id = bid_details.client_id

            # Находим портфель в БД
            portfolio = PortfolioExt(client_id=client_id,
                                     portfolio_id=portfolio_id,
                                     db=self.db,
                                     get_db_position=True)

            portfolio.init_trader(with_trades_history=True)

            # Конвертируем поля
            td = execution_report['tradeDate']
            tradedate = td[0:4] + '-' + td[4:6] + '-' + td[6:]

            operations = {
                '1': 'Купля',
                '2': 'Продажа'
            }
            format_trade = {
                'operation': operations[execution_report['side']],
                'ticker': execution_report['securityId'],
                'price': execution_report['lastPx'],
                'vol': execution_report['cumQty'] / bid_details.shares_in_lot,
                'tradedate': tradedate,
                'value': execution_report['exValue'],
                'trade_num': execution_report['tradeNum'],
                'shares_in_lot': bid_details.shares_in_lot,
                'commission': 0.001
            }
            # выполняем сделку QUIK
            return_trade = portfolio.trader.execute_quik_trade(**format_trade, return_trade=True)

            # обновляем позиции
            self.db.update_positions(portfolio.positions)

            # обновляем статистику
            item = portfolio.update_asset_values(tradedate=tradedate)
            item['tradedate'] = tradedate
            item['portfolio_id'] = portfolio.portfolio_id
            item['client_id'] = portfolio.client_id
            portfolio.save_portfolio_report(**item)

            # item['accordance'] = master_portfolio_table.calculate_accordance()

            # Сохраняем текущее состояние
            portfolio.save_db_state()

            # сохраняем историю сделок
            self.db.save_trade(return_trade)
