from flask import Blueprint
from flask import Flask
from flask import url_for
from flask_cors import CORS
from flask_migrate import Migrate
from flask_restplus import Api
from flask_sqlalchemy import SQLAlchemy

from app.settings import DB_CONNECT_STRING, APP_URL_PREFIX, MULTIPROCESS_METRICS_MODE, APP_DOC_ENABLED
from app.infrastructure.metrics.helper import get_flask_metrics_class

database = SQLAlchemy()


class CustomApi(Api):
    @property
    def specs_url(self):
        """Monkey patch for HTTPS"""
        _scheme = 'http' if '5000' in self.base_url else 'https'
        return url_for(self.endpoint('specs'), _external=True, _scheme=_scheme)


def create_app():
    blueprint = Blueprint('api', __name__, url_prefix=APP_URL_PREFIX)

    api = CustomApi(
        blueprint,
        title='Rais API MS',
        version='1.0',
        doc='/doc' if APP_DOC_ENABLED else False,
        description='Rais API MS',
    )
    # Создаем экзэмпляр приложения
    app = Flask(__name__)
    app.add_url_rule('/health', 'health', view_func=lambda: 'ok')

    app.config["SQLALCHEMY_DATABASE_URI"] = DB_CONNECT_STRING
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.register_blueprint(blueprint)

    database.init_app(app)
    migrate = Migrate(app, database, compare_type=True)  # noqa F841

    from app.infrastructure.rest.routes.portfolio import api as portfolio_ns
    from app.infrastructure.rest.routes.pin import api as pin_ns
    from app.infrastructure.rest.routes.service import api as service_ns
    from app.infrastructure.rest.routes.trade import api as trade_ns
    from app.infrastructure.rest.routes.stats import api as stat_ns
    from app.infrastructure.rest.routes.pnl import pnl as pnl_ns
    from app.infrastructure.rest.routes.portfolio_page import api as portfolio_page_ns
    from app.infrastructure.rest.routes.bid_request import api as order_ns
    from app.infrastructure.rest.routes.client import api as client_ns
    from app.infrastructure.rest.routes.admin import api as admin_ns

    api.add_namespace(portfolio_ns, path='/portfolios')
    api.add_namespace(service_ns, path='/service')
    api.add_namespace(trade_ns, path='/trades')
    api.add_namespace(stat_ns, path='/stat')
    api.add_namespace(pin_ns, path='/pin')
    api.add_namespace(pnl_ns, path='/pnl')
    api.add_namespace(portfolio_page_ns, path='/portfolio_pages')
    api.add_namespace(order_ns)
    api.add_namespace(client_ns)
    api.add_namespace(admin_ns)

    # Включаем Cross Origin
    CORS(app)

    get_flask_metrics_class(MULTIPROCESS_METRICS_MODE)(app)

    from app.infrastructure.rest.tools import exception_handler
    portfolio_ns.error_handlers[Exception] = exception_handler

    return app
