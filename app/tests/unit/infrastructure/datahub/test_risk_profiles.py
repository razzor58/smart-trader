from unittest.mock import patch, MagicMock
from app.infrastructure.integration.datahub.handlers.risk_profiles import process


class TestRiskProfilesJob:
    @patch('app.infrastructure.integration.datahub.handlers.risk_profiles.KeyCloak')
    @patch('app.infrastructure.integration.datahub.handlers.risk_profiles.SessionContext')
    @patch('app.infrastructure.helpers.http_request')
    def test_job_is_ok(self, http_request, test_db, test_token):
        mo = MagicMock()
        mo.get_clients.return_value = [{'id': 4, 'master_id': 'f5cefe5c-ae75-4b58-9f50-4a9e4265cc42'}]
        test_db.return_value.__enter__.return_value = mo
        http_request.return_value = {
            'profiles': [
                {
                    'questionary_id': '04a09617-1e78-40d6-8c05-cfdee3a6bfe8',
                    'template_id': '9042f71d-3d51-4602-9406-1101346610eb',
                    'filling_datetime': '2012-02-29T06:43:32Z',
                    'confirmed': True,
                    'investmentAdviceAgreement': False,
                    'isActual': False,
                    'profile': [
                        {
                            'id': '22c0d151-46c6-4a23-bd46-7a3e4d71e51a',
                            'type': 'risk',
                            'name': '[1]Умеренно-агрессивный'
                        }
                    ]
                }
            ]
        }
        process()
