from datetime import datetime

from app.domain.entities.business.position import Position
from app.domain.entities.plain import PortfolioTicker, Quotes, Ticker
from app.domain.tools import calc_bond_real_cost
from app.domain.constants import Currencies, TickerTypes

TODAY = datetime.today()


class PnlLoader(object):
    """ Подкачка данных для набора позиций """
    def __init__(self, db, **kwargs):
        self.db = db

        self.tickers_meta_data = None
        self.volumes = None
        self.quotes = None
        self.date_options = None
        self.tariffs = None

    @staticmethod
    def get_filters(kwargs, tickers_meta_data):
        filters = {}

        filters.update(kwargs)

        if 'trade_date' not in filters:
            filters['trade_date'] = datetime.today()

        if 'ticker_list' not in filters:
            filters['ticker_list'] = [t['ticker'] for t in tickers_meta_data]

        if 'isin_list' not in filters:
            filters['isin_list'] = [t['isin'] for t in tickers_meta_data]

        return filters

    @staticmethod
    def filter_rows(rows_list, ticker, field='ticker'):
        rows = list(filter(lambda x: x.get(field, '') == ticker, rows_list))
        result = {} if len(rows) == 0 else rows[0]
        return result

    def get_price_data(self, ticker_meta, quotes, date_options):

        # Read params
        ticker = ticker_meta['ticker']
        isin = ticker_meta['isin']
        currency = ticker_meta['currency']
        ticker_type = ticker_meta['type']

        # Find ticker prices
        filtered_quotes = self.filter_rows(quotes, ticker)

        # Если нашли цену
        if len(filtered_quotes) > 0:
            raw_price = filtered_quotes['close']

            # Корректируем цену иностранных инструментов
            if currency == Currencies.USD.value:
                ticker_price = raw_price
                ticker_rur_price = raw_price * filtered_quotes['usd_close']
            # Для российский акций - просто цена
            else:
                ticker_price = raw_price
                ticker_rur_price = raw_price

            # Для купонов считаем цену с НКД
            if ticker_type == TickerTypes.BONDS:
                option = self.filter_rows(date_options, isin, field='isin')

                bond_price = calc_bond_real_cost(raw_price,
                                                 option['currentFacevalue'],
                                                 option['accruedInterest'])

                ticker_rur_price = bond_price
                ticker_price = bond_price

        # Если нет цен
        else:
            ticker_price = 0
            ticker_rur_price = 0
            filtered_quotes = {'tradedate': datetime.today(), 'usd_close': 0}

        return {
            'price': float(ticker_price),
            'rur_price': float(ticker_rur_price),
            'tradedate': filtered_quotes['tradedate'],
            'usd_close': filtered_quotes['usd_close']
        }

    def get_commission(self, meta, tariffs):
        currency = Currencies.RUR.value if meta['currency'] in (Currencies.SUR.value, Currencies.RUB.value) \
            else meta['currency']
        tariffs = self.filter_rows(tariffs, currency, field='currency')

        return float(tariffs['commission'])

    def get_bonds_cost(self, tickers, isin_dict, trade_date=TODAY):
        res = {}
        date_options = self.db.get_date_options(isin_list=list(
            isin_dict.values()),
                                                trade_date=trade_date)

        for ticker, val in tickers.items():
            if ticker in isin_dict:
                option = self.filter_rows(date_options,
                                          isin_dict[ticker],
                                          field='isin')

                res[ticker] = calc_bond_real_cost(val['price'],
                                                  option['currentFacevalue'],
                                                  option['accruedInterest'])

        return res

    def load_data(self, **kwargs):
        # По умолчанию все берем на сегодня
        if 'trade_date' not in kwargs:
            kwargs['trade_date'] = datetime.today()

        # Запрашиваем инструменты стратегии и их текущие объемы
        self.tickers_meta_data = self.db.get_tickers(**kwargs)

        # Параметры запроса
        request_params = self.get_filters(kwargs, self.tickers_meta_data)

        # Запрашиваем цены инструментов
        self.quotes = self.db.get_quotes(**request_params)

        # Запрашиваем НКД
        self.date_options = self.db.get_date_options(**request_params)

        # Запрашиваем тарифы
        self.tariffs = self.db.get_tariffs(**request_params)

    def get_strategy_state(self, **kwargs):
        """ Текущий объем и цены """
        # Результирующий словарь
        positions = {}
        # Скачиваем данные
        self.load_data(**kwargs)

        # Формируем массив позиций
        for ticker_meta in self.tickers_meta_data:
            # код инструмента
            ticker = ticker_meta['ticker']

            # Создаем позицию если есть объемы
            positions[ticker] = Position(
                ticker=ticker,
                meta=Ticker(**ticker_meta),
                prices=Quotes(
                    self.get_price_data(ticker_meta, self.quotes,
                                        self.date_options)),
                strategy=PortfolioTicker(ticker_meta),
                tariffs=self.get_commission(ticker_meta, self.tariffs))
        return positions

    def get_history_track(self, **kwargs):
        try:
            self.tickers_meta_data = self.db.get_tickers(**kwargs)

            # Параметры запроса
            request_params = self.get_filters(kwargs, self.tickers_meta_data)

            # Скачиваем данные
            trade_data = self.db.get_track(**request_params)

        except Exception as e:
            raise e
        finally:
            self.db.close_sessions()

        return trade_data
