"""empty message

Revision ID: 7a3de80a63cc
Revises: 7384dbe9c2e4, 2e5f8013ebb0
Create Date: 2020-06-17 16:54:04.228723

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7a3de80a63cc'
down_revision = ('7384dbe9c2e4', '2e5f8013ebb0')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
