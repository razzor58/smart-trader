import unittest
import requests as r
from . import SERVICE_URL


class TestHavePin(unittest.TestCase):
    """    Метод отправки пакетки в ProxyFix

   """
    resp = None

    def setUp(self):
        method = '/pin/havePin'
        url = SERVICE_URL + method
        self.resp = r.post(url).json()

    def test_have_pin_resp_is_valid(self):
        test_result = isinstance(self.resp, dict)
        self.assertTrue(test_result)

    def test_have_pin_success(self):
        """
            {
              "data": {
                "havePin": true
              },
              "success": true,
              "error": null
            }
        """
        try:
            self.assertTrue(self.resp['success'])
        except KeyError:
            print(self.resp)


if __name__ == '__main__':
    unittest.main()
