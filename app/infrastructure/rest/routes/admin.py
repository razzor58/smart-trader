from flask_restplus import Resource
# from app.infrastructure.rest.tools import auth_app
from app.infrastructure.rest.schemas import AdminDto
from app.infrastructure.db.domain_dao import SessionContext
from app.domain.usecases.admin.handler import AdminUseCase

api = AdminDto.api


@api.route('/strategies')
class StrategiesDetails(Resource):
    # @auth_app
    def get(self, **kwargs):
        with SessionContext() as db:
            use_case = AdminUseCase(db)
            return use_case.get_main_page()
