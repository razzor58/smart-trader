from os.path import dirname, basename, isfile
import glob
import importlib

modules = glob.glob(dirname(__file__)+"/*.py")
__all__ = [basename(f)[:-3] for f in modules if isfile(f) and not f.endswith('__init__.py')]

workers = dict()


for file in __all__:
    cls = getattr(importlib.import_module('rabbit_client.handlers.' + file), 'Worker')
    obj = cls()
    try:
        workers[obj.routing_key] = obj
    except KeyError:
        print(file + "don't have routing_key attribute")
