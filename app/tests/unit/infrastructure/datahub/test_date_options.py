from unittest.mock import patch, MagicMock
from app.infrastructure.integration.datahub.handlers.date_options import process


class TestDateOptionsJob:
    @patch('app.infrastructure.integration.datahub.handlers.date_options.KeyCloak')
    @patch('app.infrastructure.integration.datahub.handlers.date_options.SessionContext')
    @patch('app.infrastructure.integration.datahub.handlers.date_options.http_request')
    def test_job_is_ok(self, http_request, test_db, test_token):
        mo = MagicMock()
        mo.get_ticker_list.return_value = [{'isin': 'RU000A0JR1Y7', 'trade_mode': None}]
        test_db.return_value.__enter__.return_value = mo
        http_request.return_value = {'error': None, 'couponDate': '2020-09-15T00:00:00Z', 'couponPeriod': 182,
                                     'couponRate': 8.25, 'payPerBond': 41.14, 'offerDate': None, 'buyBackPrice': None,
                                     'currentFacevalue': 1000.0, 'accruedInterest': 14.01, 'listingLevel': 2,
                                     'sumCouponsMty': 41.14, 'sumCouponsOffer': None, 'lastFacevalueChangeDate': None}
        process()
