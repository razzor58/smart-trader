from collections import defaultdict
from datetime import datetime
from typing import List, Union

from app.domain.constants import (TariffsTypes, Currencies, TickerTypes, BidStatus, SyncStatus, ExecStatus,
                                  MARGIN_SHORT_SIZE)
from app.domain.interfaces import IPortfolioDAO
from app.domain.entities.business.trader import TradeManager
from app.domain.entities.business.pnl_loader import PnlLoader
from app.domain.entities.business.position import Position
from app.domain.entities.business.bids import Bid
from app.domain.entities.plain import ClientsPortfolio
from app.domain.tools import calc_bond_real_cost, get_obj_to_save

from app.infrastructure.helpers import store_single_client, update_single_profile


class PortfolioExt(object):
    """ Основной класс реализующий управление портфелем """
    def __init__(self, portfolio_id: int, db: IPortfolioDAO, login: str, cash=None, **kwargs):

        self.portfolio_id = portfolio_id
        self.client = None
        self.pnl = PnlLoader(db)
        self.db = db
        self.ref = None

        # Справочные данные из БД
        if portfolio_id:
            self.ref = self.db.get_portfolio_origin(portfolio_id, as_dict=False)
            self.name = self.ref.name
            self.currency = self.ref.currency
            self.currency_format = '$' if self.ref.currency == Currencies.USD.value else 'руб.'
        else:
            self.currency = None
            self.currency_format = 'руб.'
            self.name = 'Сводные данные'

        if kwargs.get('client_id'):
            self.client_id = kwargs.get('client_id')
        # У мастер портфелей владелец client_id = 1
        elif login is None:
            self.client_id = 1
        else:
            self.client_id = self.setup_client(login)

        self.state = None
        # Но Берем поля состояния из БД (или сразу init state ?)
        if self.client_id and self.portfolio_id != 1:
            self.init_state()

        # TODO: переопределить поля из self.state
        # Поля

        self.start_asset = 0
        self.cash_deposit = 0
        self.income = 0
        self.asset = cash
        self.cash = cash
        self.shares = 0
        self.last_eod = datetime.utcfromtimestamp(0)
        self.date_from = datetime.now()

        self.synchronized = SyncStatus.SYNCHRONIZED
        self.tariff_id = TariffsTypes.DEFAULT

        # Данные для pnl по умолчанию не запрашиваем
        self.stat = None
        self.tickers = None
        self.trader = None
        self.depo_limit = None

        # Если нужен репльный портфель из БД - сразу берем текущие позиции
        if kwargs.get('get_db_position', False):
            self.positions = {}
            self.apply_db_state()
        else:
            self.positions = {}

        # Комиссии
        self.commission_size = 0
        self.total_divs = 0
        self.margin_short_size = 0
        self.margin_long_size = 0

    @property
    def shares_positions(self):
        return [
            position for ticker, position in self.positions.items()
            if position.portfolio_id == self.portfolio_id and position.is_not_currency()
        ]

    @property
    def save_field(self):
        return get_obj_to_save(self, ClientsPortfolio)

    def setup_client(self, login: str) -> Union[int, None]:
        self.client = self.db.get_client(login)
        if self.client:
            return self.client.id

        if store_single_client(login) > 0:
            self.client = self.db.get_client(login)
            if self.client:
                if self.ref and self.ref.iir_enabled == 1:
                    update_single_profile(self.client.id, self.client.master_id)
                return self.client.id

        return

    def to_dict(self):
        return {
            'client_id': self.client_id,
            'portfolio_id': self.portfolio_id,
            'shares': self.shares,
            'cash': self.cash,
            'start_asset': self.start_asset,
            'asset': self.asset,
            'date_from': self.date_from,
            'cash_deposit': self.cash_deposit,
            'tariff_id': self.tariff_id,
            'synchronized': self.synchronized,
        }

    def init_state(self):
        row = self.db.get_current_state(portfolio_id=self.portfolio_id, client_id=self.client_id)
        self.state = ClientsPortfolio(row) if row else None

    def add_position(self, position: Position):
        position.client_id = self.client_id
        if position.portfolio_id is None:
            position.portfolio_id = self.portfolio_id

        self.positions[position.ticker] = position

    def update_portfolio_meta(self, portfolio_id, check_ref=True):
        if check_ref:
            self.ref = self.db.get_portfolio_origin(portfolio_id, as_dict=False)
        self.portfolio_id = portfolio_id
        self.currency = self.ref.currency

    def current_cash_position(self):
        cash_position = self.positions.get(Currencies.CASH.value)
        return cash_position.cost if cash_position else 0

    def apply_db_state(self):
        """пишем в поля все данные которые есть в БД"""
        for t, p in self.get_saved_positions().items():
            self.add_position(Position(ticker=t, position=p))

        if self.state is not None:

            self.asset = self.state.cash
            self.cash = self.state.cash

            self.start_asset = self.state.start_asset
            self.cash_deposit = self.state.cash_deposit

            self.total_divs = self.state.total_divs
            self.commission_size = self.state.commission_size

            # TODO: refactor
            self.depo_limit = self.db.get_depo_limit(self.client_id)

        else:
            self.cash = self.current_cash_position()
            self.asset = self.current_cash_position()
            self.cash_deposit = 0

    def add_divs(self, **kwargs):
        """ Добавляем дивиденды """
        position = self.positions[kwargs['ticker']]
        div_size = float(kwargs['divs'] * position.nom_part * position.ticker.shares_in_lot)

        # для мультивалютного портфеля дивиденды считаем в рублях по курсу на день зачисления
        if position.ticker.currency == Currencies.USD and self.ref.currency == Currencies.RUR:
            # Если праздничеый день в РФ и курса доллара нет - берем предыдуще значение из позиции
            usd_close = kwargs['usd_close'] or position.prices.usd_close
            div_size *= float(usd_close)

        position.div_size += div_size

        # Обновляем данные портфеля
        self.cash += div_size
        self.total_divs += div_size

    def init_trader(self, **kwargs):

        if kwargs.get('with_trades_history', False):
            params = dict(trades=[v.__dict__ for v in self.shares_positions if v.nom_part != 0])
        else:
            params = {}

        self.trader = TradeManager(portfolio=self, mode='real', **params)

    def create_bids(self, **kwargs):
        bid = Bid(portfolio=self)
        bid.create(**kwargs)
        return self.positions

    def risk_is_not_compatible(self):
        if self.ref.iir_enabled == 0:
            return False

        return self.ref.risk_level > self.db.get_client_risk_profile(self.client_id)

    def calc_current_pnl(self, **kwargs):
        """ Оценка портфеля в текущий момент подключения
            Сокращенный и измененный PNL для подключения реальных клиентов
        """
        cash_deposit = kwargs.get('cash_deposit', 0)

        item = self.get_portfolio_results()
        item['tradedate'] = self.date_from
        item['portfolio_id'] = self.portfolio_id
        item['client_id'] = self.client_id
        item['cash_deposit'] = cash_deposit
        item['asset'] = cash_deposit
        item['cash'] = cash_deposit

        self.save_portfolio_report(result_rows=[item])

        # Проставляем значения по умолчанию
        item['tariff_id'] = kwargs.get('tariff_id', 1)
        item['synchronized'] = kwargs.get('synchronized', 1)

        # Проставляем валюту по умолчанию
        item['currency'] = self.ref.currency

        # в момент подключения - считаем start_asset = asset = deposit
        if item.get('start_asset') is None:
            item['start_asset'] = cash_deposit

        # Сохраняем актуальные позиции
        self.save_db_state()

    def change_position(self,
                        ticker,
                        trade_size=None,
                        cash_change=None,
                        avg_open_price=None,
                        vol_origin=None,
                        fixed_profit=0,
                        **kwargs):
        """ Вызывается при исполнении сделок трейдером -  меняется состояние инстурментов в порфеле """
        position = self.positions[ticker]

        position.nom_part += trade_size

        # Банковское округление в Python3 - это песня. поэтому вот так
        acc_vol = float(format(self.positions[ticker].acc_vol + float(vol_origin), '.3f'))
        position.acc_vol = acc_vol
        position.fixed_profit += fixed_profit
        position.vol = 0

        # Статус позиции
        if kwargs.get('quik_position_type'):
            position.position_type = kwargs.get('quik_position_type')

        elif position.acc_vol == 0:
            position.position_type = 'wait'

        else:
            position_type = kwargs.get('position_type')
            if position_type:
                self.positions[ticker].position_type = position_type

        # Стоимость сохраняем при наборе позиции или в конце дня
        if abs(cash_change) > 0:
            self.positions[ticker].real_part_cost -= cash_change

        if int(self.positions[ticker].nom_part) == 0:
            self.positions[ticker].fixed_profit = 0
            self.positions[ticker].cost = 0

        # Меняем среднюю цену входа
        self.positions[ticker].avg_open_price = avg_open_price

        # Меняем состояние портфеля
        # Если бумага принадлежит инвест.части  - меняем кэш
        if str(self.positions[ticker].portfolio_id) == str(self.portfolio_id):
            self.cash += cash_change
            self.positions[Currencies.CASH.value].cost = self.cash

        res = self.get_portfolio_results(**kwargs)
        self.asset = res['asset']
        self.shares = res['shares']

        # при разгрузке портфеля
        if int(self.start_asset) == 0:
            self.positions[Currencies.CASH.value].cost = 0

        # Накопленный итог по комиссии за сделки
        if kwargs['commission'] > 0:
            self.commission_size += float(kwargs['commission'])

    def get_saved_positions(self, **kwargs):
        """ Позиции в БД на сейчас (при подключении портфеля - все по нулям) """
        return self.db.get_current_positions(self.portfolio_id, client_id=self.client_id, **kwargs)

    def get_positions_sum(self):
        """ Возвращает сумму акций и денежных средств """
        shares_sum = sum([position.real_part_cost for position in self.shares_positions])

        if Currencies.SUR.value in self.positions:
            cash = self.positions[Currencies.SUR.value].real_part_cost
        elif Currencies.CASH.value in self.positions:
            cash = self.positions[Currencies.CASH.value].real_part_cost
        else:
            cash = 0

        return shares_sum, cash

    def get_portfolio_reports(self):
        """ Возвращает таблицу с результатам работы """
        return self.db.get_portfolio_stat(self.portfolio_id, self.client_id)

    def get_portfolio_bids(self):
        """ Возвращает клиенские заявки для вкладка Заявки """
        # получаем данные
        db_rows = self.db.get_portfolio_bids(self.portfolio_id, self.client_id)

        # Группируем ответ
        res = defaultdict(dict)
        for row in db_rows:
            row['sign'] = 'buy' if row['operation'] in ('open long', 'close short') else 'sell'
            res[str(row['bucket_num'])]['bid_create_time'] = row['bid_create_time']
            row['quik_trade_lots'] = int((row['quik_trade_vol'] or 0) / (row['shares_in_lot'] or 1))
            if 'items' not in res[str(row['bucket_num'])]:
                res[str(row['bucket_num'])]['items'] = []

            res[str(row['bucket_num'])]['items'].append(row)

        for k, v in res.items():
            bid_status = self.get_bid_status([x['exec_status'] for x in v['items']])
            res[k]['execute_message'] = bid_status
            res[k]['is_executed'] = bid_status == BidStatus.EXECUTED

        return [v for k, v in res.items()]

    @staticmethod
    def get_bid_status(statuses: List[int]) -> BidStatus:
        if all([status == ExecStatus.FILL for status in statuses]):
            return BidStatus.EXECUTED
        elif set(statuses).intersection([ExecStatus.PARTIALLY_FILL, ExecStatus.FILL]):
            return BidStatus.PART_EXECUTED
        elif set(statuses).intersection([ExecStatus.NEW, ExecStatus.REPLACED, None]):
            return BidStatus.ACCEPTED

        return BidStatus.NOT_EXECUTED

    def get_portfolio_results(self, **kwargs):
        """ Возвращает сводные данные по портфелю """
        shares, cash = self.get_positions_sum()
        if self.start_asset == 0 or self.start_asset == self.asset:
            income = 0
        else:
            income = (shares + self.cash) / self.asset - 1

        asset = shares + cash
        res = {
            "cash": cash,
            "shares": shares,
            "asset": asset,
            "income": income,
            "margin_short_size": self.margin_short_size,
            "commission_size": self.commission_size,
            "total_divs": self.total_divs
        }

        # Переоценка капитала в рублях в конце дня для общего PNL
        if kwargs.get('usd_close'):
            usd_close = float(kwargs.get('usd_close'))
            is_usd = True if self.ref.currency == 'USD' else False

            asset_rur = asset * usd_close if is_usd else asset
        else:
            asset_rur = self.asset

        res.update({
            "asset_rur": asset_rur,
        })

        return res

    def get_margin_size(self, **kwargs):
        """ Вычисляем размер маржи """

        # Текущее состояние
        position = self.positions[kwargs['ticker']]
        position_type = position.position_type

        # Пока расчет только для шортов
        if position_type == 'short' and position.cost < 0:
            days_delta = (kwargs['tradedate'] - position.tradedate).days
            margin_size = abs(position.cost) * (kwargs.get('margin_short', MARGIN_SHORT_SIZE) / 100 * days_delta)

        elif position_type == 'long':
            margin_size = 0
        else:
            margin_size = 0

        return margin_size

    def get_trades(self, **kwargs):
        """ Список сделок,  когда получим сделку из экспортера"""
        return self.db.get_trades(self.portfolio_id, self.client_id, **kwargs)

    # TODO: move to BackTest service
    def calc_pnl(self, trade_data, **kwargs):
        """ Обработка входящий событий - пересчет PNL """
        days_ago = kwargs.get('days_ago', 1)
        # Очищаем предыдущие результаты работы портфеля в БД
        if days_ago is None:
            self.clear_db_data(portfolio_id=self.portfolio_id, client_id=self.client_id)

            # Если нужно пересчитать pnl полностью - берем дату подключения портфеля
            trade_date_from = self.get()['date_from']  # - timedelta(days=1)

            # Инициализируем позиции и сохраняем их в экземпляре
            if self.client_id == 1:
                self.cash = self.ref.currency_cash
                self.asset = self.ref.currency_cash
            else:
                self.cash = self.state['cash']
                self.asset = self.state['asset']

            # Запрашиваем последние доли от интеллекта
            self.positions = self.get_master_positions(trade_date=trade_date_from)

            # Инициализируем трейдера для торговли лотами
            self.init_trader()

            # Набираем лоты по эталонным долям
            self.trader.init_positions()

            # Сохраняем в БД начальные позиции
            self.positions = self.db.create_empty_positions(master_position=self.positions, client_id=self.client_id)

        else:
            self.apply_db_state()

            # Создаем трейдера для пересчета позиций
            self.init_trader(with_trades_history=True)
            # position_to_change = [v for k, v in self.positions.items() if float(v['nom_part']) != 0 and k != 'CASH']
            # self.trader = TradeManager(portfolio=self, mode='real', trades=position_to_change)

        # Просчитываем историю работы портфеля на историчесом массиве сделок
        portfolio_report_dict = defaultdict(dict)
        for trade in trade_data:
            # Если новый тикер - создаем позицию
            if trade['ticker'] not in self.positions:
                self.positions = self.create_position(**trade, save_to_db=True)

            if trade['type'] == 'quik_trade':
                # Если запускаем досчет - то не пересчитываем сделки которые обработали считали
                is_new_trade = trade['new_trade']
                if days_ago is None or int(is_new_trade) == 1:
                    result_trade = self.trader.execute_quik_trade(**trade, return_trade=True)
                    self.update_positions_in_db()

                    # move to apply_trade_from_kafka method
                    self.merge_bids(trade=trade)
                    self.db.save_trade(result_trade, self.portfolio_id, self.client_id)
                res = None

            elif trade['type'] == 'deposit':
                self.process_cash_deposit_for_pnl(**trade)
                res = None

            else:
                res = self.trader.execute(
                    **trade,
                    portfolio_id=self.portfolio_id,
                    client_id=self.client_id,
                    cost_field='real_part_cost',
                    ticker_type=self.positions[trade['ticker']]['ticker_type'],
                )

            if res:
                item = res
                item['tradedate'] = trade['tradedate']
                item['portfolio_id'] = self.portfolio_id
                item['client_id'] = self.client_id
                portfolio_report_dict[item['tradedate']] = item

        # Сохраняем график доходности
        portfolio_report = [v for k, v in portfolio_report_dict.items()]
        if len(portfolio_report) > 0:
            self.save_portfolio_results(result_rows=portfolio_report)

        # Просто чтобы не ругался Сонар
        self.save_db_state()

        # Сохраняем актуальные позиции
        self.update_positions_in_db()

        # сохраняем историю сделок
        self.db.save_trade_bulk(self.trader.trades_results)

    # TODO: move to BackTest service
    def merge_bids(self, trade):
        # get_active clients_bids
        # TODO: replace to Bids module
        ticker = trade['ticker']
        bid = self.db.get_bid(client_id=self.client_id, portfolio_id=self.portfolio_id, ticker=ticker)

        # Если сделка прилетела по инструменту, по которому нет рекомендаций - ничего не делаем
        if bid is not None:
            cur_qty = int(bid.quik_trade_vol) if bid.quik_trade_vol else 0
            new_vol = int(cur_qty + trade['vol'])
            self.db.update_bids(bid_id=bid.id, **{'quik_trade_vol': new_vol})

    def process_cash_deposit_for_pnl(self, **kwargs):
        deposit = kwargs.get('value')
        self.asset += deposit
        self.cash += deposit
        self.cash_deposit += deposit

    def get_position_disparity(self, ticker, total_shares_cost):
        p = self.positions[ticker]
        if ticker == Currencies.CASH.value or p.portfolio_id != self.portfolio_id:
            position_disparity = 0
        else:
            part_cost_coefficient = p.cost / total_shares_cost if int(total_shares_cost) != 0 else 1
            if p.nom_part == 0 and p.recommend_part != 0:
                nominal_disparity = 1
            elif p.nom_part == 0 and p.recommend_part == 0:
                nominal_disparity = 0
            else:
                nominal_disparity = p.recommend_part / p.nom_part - 1

            position_disparity = abs(nominal_disparity * part_cost_coefficient)

        return position_disparity

    def calculate_accordance(self):
        """
        for i in range(len(df)):

            if abs(etalon - real) <= abs(etalon):
                delta_sum += abs(etalon - real)*price*lots
            else:
                delta_sum += abs(etalon)*price*lots


            total_sum += abs(etalon)*price*lots

        result = 1-delta_sum/total_sum

        :return:
        """
        total_sum = 0
        delta_sum = 0
        for position in self.shares_positions:
            # Если размер позиции меньше рекомендованной - считаем разницу

            if abs(position.recommend_part - position.nom_part) <= abs(position.recommend_part):
                delta_sum += abs(position.recommend_part - position.nom_part) * position.rur_price *\
                             position.shares_in_lot
            else:
                delta_sum += abs(position.recommend_part) * position.rur_price * position.shares_in_lot

            # Считаем общую
            total_sum += abs(position.recommend_part) * position.rur_price * position.shares_in_lot

        if int(total_sum) == 0:
            accordance = 0
        else:
            acc_prc = round((1 - delta_sum / total_sum) * 100, 2)
            # Не меньше 0%
            accordance = acc_prc if acc_prc > 0 else 0

        return accordance

    def update_asset_values(self, **kwargs):
        res = self.get_portfolio_results(**kwargs)

        # Меняем состояние портфеля
        self.asset = res['asset']
        self.shares = res['shares']
        self.last_eod = kwargs.get('tradedate')

        # Стартовый капитал считаем на по закрытию первого дня (это мы знаем только ко 2му закрытию)
        if self.start_asset > 0:
            self.income = ((self.shares + self.cash) / self.start_asset - 1) * 100

        return res

    def recalc_portfolio_position(self, **kwargs):
        """ Вызывается по событию закрытия дня или онлайн-переоценка по текущим ценам"""
        ticker = kwargs['ticker']
        # Если у нас нет позиций по этом инструменту - перерасчет не делаем
        if ticker not in self.positions:
            return

        # Определяем цену
        if self.ref.currency == Currencies.USDRUR:
            price = kwargs['rur_price']
        else:
            price = kwargs['price']

        if price is None:
            return None

        # Сначала берем комиссии по клиенту пока не считаем
        ticker_margin_short_size = self.get_margin_size(**kwargs) if self.client_id == 1 else 0
        self.margin_short_size += ticker_margin_short_size

        # Вычитаем из кэша комис
        self.cash -= ticker_margin_short_size

        position = self.positions[ticker]

        # Оценка бумаги на текущий момент (конец дня или новая котировка)
        position.price = price
        position.tradedate = kwargs['tradedate']
        position.usd_close = kwargs['usd_close']

        # Нереализонный profit - loss
        if (position.ticker.currency == Currencies.USD and self.ref.currency == Currencies.RUR and self.client_id != 1):
            price_delta = price - float(position.avg_open_price) * float(kwargs['usd_close'])
        else:
            price_delta = price - position.avg_open_price

        position.calculate_profit = price_delta * position.nom_part * position.ticker.shares_in_lot

        # для мастер портфеля и ДУ делаем по старому

        real_part_cost = position.nom_part * position.ticker.shares_in_lot * price
        cost = position.recommend_part * position.ticker.shares_in_lot * price

        if position.ticker.type == TickerTypes.BONDS:
            bond_cost = calc_bond_real_cost(price, kwargs['currentFacevalue'], kwargs['accruedInterest'])
            real_part_cost = position.nom_part * position.ticker.shares_in_lot * bond_cost
            cost = position.recommend_part * position.ticker.shares_in_lot * bond_cost

        position.real_part_cost = real_part_cost
        position.cost = cost

        # Меняем состояние портфеля
        res = self.update_asset_values(**kwargs)

        return res

    def save_portfolio_report(self, **kwargs):
        """ Обновляется вектор результатов в БД """
        if 'result_rows' in kwargs and len(kwargs['result_rows']) > 1:
            self.db.save_portfolio_results(report=None, report_list=kwargs['result_rows'])
        elif 'result_rows' in kwargs and len(kwargs['result_rows']) == 1:
            self.db.save_portfolio_results(report=kwargs['result_rows'][0], report_list=None)
        else:
            self.db.save_portfolio_results(report=kwargs['report'], report_list=None)

    def save_db_state(self):
        """ сохраняем состояние портфелям """
        self.db.set_current_state(self.save_field)

    def set_start_asset(self, **kwargs):
        """ Вычисляем начальный капитал, если не установлен """
        td = kwargs['tradedate']
        if isinstance(td, str):
            td = datetime.strptime(td, '%Y-%m-%d')
        if td > self.last_eod > datetime.utcfromtimestamp(0):
            summary = self.cash + self.shares
            self.start_asset = summary

    def update_portfolio_pnl(self):
        """ Рассчитываем и обновляется вектор результатов в БД, при изменении соотояния портфеля """
        self.apply_db_state()
        shares, cash = self.get_positions_sum()
        asset = shares + cash
        trade_date = datetime.now().strftime('%Y-%m-%d')
        self.save_portfolio_report(shares=shares,
                                   cash=cash,
                                   portfolio_id=self.portfolio_id,
                                   client_id=self.client_id,
                                   asset=asset,
                                   tradedate=trade_date)

    def update_positions_in_db(self):
        self.db.update_positions(self.positions)

    def set_client_allowed(self):
        self.db.set_client_allowed(self.client_id)
