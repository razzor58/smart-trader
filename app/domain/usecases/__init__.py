from app.domain.entities.business.portfolio import PortfolioExt
from app.domain.interfaces import IPortfolioDAO


class PortfolioUseCase:

    def __init__(self,
                 portfolio_id,
                 login,
                 portfolio_dao: IPortfolioDAO,
                 ):

        self.db = portfolio_dao
        self.portfolio = PortfolioExt(portfolio_id, login=login, db=self.db)
        self.portfolio_id = portfolio_id
        self.client_id = self.portfolio.client_id
