from app.domain.interfaces import IPortfolioDAO
from app.domain.usecases import PortfolioUseCase


class PortfolioDeleteUseCase(PortfolioUseCase):

    def __init__(self, portfolio_id: int, login: str, portfolio_dao: IPortfolioDAO):
        super().__init__(portfolio_id, login, portfolio_dao)

    def return_positions_to_person_portfolio(self):
        """ Переводим бумаги клиента под личное управление """
        self.portfolio.apply_db_state()

        for ticker, position in self.portfolio.positions.items():
            if position.portfolio_id == self.portfolio.portfolio_id:
                self.portfolio.positions[ticker].portfolio_id = 1

        self.portfolio.update_positions_in_db()

        return self.portfolio.positions

    def delete_portfolio(self):
        if self.portfolio_id != 1:
            self.portfolio.db.delete_portfolio(self.client_id, self.portfolio_id)

    def execute(self):
        # Переносим текущие позиции под личное управление
        self.return_positions_to_person_portfolio()

        # Удаляем запись в реестре подключенных портфелей
        self.delete_portfolio()
        self.db.clear_pnl_tables(client_id=self.client_id, portfolio_id=self.portfolio_id)
