from collections import deque, defaultdict
from datetime import datetime
from app.domain.constants import PositionTypes, QuikTradeTypes, Operation
from app.domain.entities.plain import Trade as PlainTrade


def singleton(class_):
    instances = {}

    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance


class Fill(object):
    def __init__(self, price):
        self.price = price
        self.quantity = 1


class Trade(object):
    def __init__(self, direction, quantity, start_date, **kwargs):
        self.direction = direction
        self.position_factor = -1 if 'short' in direction else 1
        self.ticker = kwargs.get('ticker', 0)
        self.quantity = quantity
        self.remain_quantity = kwargs.get('remain_quantity', None)
        self.vol_prc = kwargs.get('vol_prc', 0)
        self.price = kwargs.get('price', 0)
        self.open_date = start_date
        self.trade_date_time = kwargs.get('trade_date_time', None)
        self.close_date = None
        self.market_price = kwargs.get('market_price', 0)
        self.portfolio_id = kwargs.get('portfolio_id')
        self.client_id = kwargs.get('client_id')
        self.shares_in_lot = kwargs.get('shares_in_lot', 0)
        self.profit = kwargs.get('close_profit', 0)
        self.status = kwargs.get('status', 0)
        self.margin_long = kwargs.get('margin_long', 0)
        self.margin_short = kwargs.get('margin_short', 0)
        self.commission = kwargs.get('commission', 0)
        self.trade_num = kwargs.get('trade_num')
        self.value = kwargs.get('value')
        self.accruedint = kwargs.get('accruedint')
        self.i = 0

    def __iter__(self):
        return self

    def __str__(self):
        return "{} d:{}v:{}p: {}i: {}q: {}".format(
            self.ticker,
            self.open_date,
            self.quantity / 100,
            self.price,
            self.i,
            self.quantity,
        )

    def __next__(self):
        if self.i < abs(self.quantity):
            self.i += 1

            if self.position_factor < 0:
                remain_quantity = self.quantity + self.i*self.position_factor
            else:
                remain_quantity = self.quantity - self.i * self.position_factor

            return remain_quantity, Fill(self.price)
        else:
            raise StopIteration()

    def to_dict(self):
        d = self.__dict__
        res = dict()
        for k, v in d.items():
            if k in PlainTrade.__annotations__:
                if isinstance(v, float):
                    res[k] = round(v, 3)
                else:
                    res[k] = v
        return res


class DbTrade(Trade):

    def __init__(self, **kwargs):
        direction = kwargs['operation']
        quantity = kwargs['nom_part']
        start_date = kwargs.get('open_date', datetime.today())

        # Когда склеиваем
        shares_in_lot = kwargs.get('shares_in_lot', 1) or 1
        kwargs['price'] = kwargs.get('avg_open_price', 0)*shares_in_lot

        super().__init__(direction, quantity, start_date, **kwargs)


class TradeManager(object):
    def __init__(self, mode='virtual', portfolio=None, **kwargs):

        self.trades = defaultdict(list)
        # Если сделки уже есть в БД
        trades = kwargs.get('trades')
        if trades:
            for db_row in trades:
                if abs(db_row['nom_part']) > 0:
                    self.trades[db_row['ticker']].append(DbTrade(**db_row))

        self.trades_results = defaultdict(list)
        # Счетчик сделок
        self.counter = 0

        if mode == 'virtual':
            # Создаем виртуальный портфель
            self.portfolio = kwargs['cls'](portfolio_id=kwargs['portfolio_id'])
            self.portfolio.positions = kwargs['positions']
            self.portfolio.cash = kwargs['cash']
            self.portfolio.asset = kwargs['cash']
            self.init_positions()
        else:
            self.portfolio = portfolio

    def init_positions(self):

        # Вызов без параметров - создание позиции по кешу
        _ = self.portfolio.create_position()

        # Для расчета исторической доходности по портфелям
        if self.portfolio.client_id == 1:
            if self.portfolio.positions:
                for ticker in self.portfolio.positions:
                    row = self.portfolio.positions[ticker]
                    if ticker != 'CASH':
                        # Если нет начального объема - создаем пустую позицию
                        if row['acc_vol'] != 0 and round(row['vol'], 2) != 0.0:
                            self.execute(**self.portfolio.positions[ticker], client_id=self.portfolio.client_id)

    def define_quik_position_type(self, current_value, **kwargs):
        # По  умолчанию - сохраняем старую позициию
        position_type = self.portfolio.positions[kwargs['ticker']].position_type

        #
        result_position_size = current_value + int(kwargs['qty'])
        # Кроме случаев:
        # Если нет позиции по инструменту - берем позицию сдеки
        if position_type == PositionTypes.WAIT and kwargs['operation'] == QuikTradeTypes.BUY:
            position_type = PositionTypes.LONG
        elif position_type == PositionTypes.WAIT and kwargs['operation'] == QuikTradeTypes.SELL:
            position_type = PositionTypes.SHORT
        # Если противополжная по направлению сделка больше текущей позиции
        elif position_type == PositionTypes.LONG and kwargs['operation'] == QuikTradeTypes.SELL \
                and result_position_size < 0:
            position_type = PositionTypes.SHORT
        elif position_type == PositionTypes.SHORT and kwargs['operation'] == QuikTradeTypes.BUY \
                and result_position_size > 0:
            position_type = PositionTypes.LONG
        elif result_position_size == 0:
            position_type = PositionTypes.WAIT

        return position_type

    def get_trade_size(self, trade, algo_type=1, call_for_pnl=True):

        # Текущее состояние портфеля
        prev_asset = self.portfolio.asset

        # Данные по сделке
        ticker = trade['ticker']

        # TODO: Try to implement acc_vol as in AI
        acc_vol = trade['acc_vol'] if trade.get('vol_type') == 'pnl' else round(trade['acc_vol'] + trade['vol'], 2)
        vol = trade['vol']

        # Определяем цену по которой считать
        # Сравниваем валюту портфеля и валюту портфеля
        operation_currency = trade.get('currency', trade.get('ticker_currency', 'RUR'))
        if operation_currency == self.portfolio.ref.currency:
            price = trade['price']
        else:
            price = trade.get('rur_price', trade['price'])

        # Текущее состояние инструмента
        ticker_state = self.portfolio.positions[ticker].__dict__
        prev_max_part = ticker_state.get('max_part')
        operation = trade['operation']
        shares_in_lot = ticker_state.get('shares_in_lot', 1)
        prev_nom = ticker_state.get('nom_part') or 0
        if price is None:
            price = float(trade['close'] * ticker_state['usd_close'])

        # Далее все считаем по цене за лот
        price = price*shares_in_lot

        # Вычисляемые параметры на основе данных сделки и состояния портфеля
        # TODO: Важный момент max_part по умолчанию 1 - в документацию
        max_part = trade.get('max_part', 1) or prev_max_part
        max_part_delta = max_part - prev_max_part

        # Сигнал:  сделка
        if vol != 0 or trade.get('vol_type', 'pnl') != 'pnl':
            deal_direction = vol / abs(vol)
        # Сигнал:  Ребалансировка
        elif max_part_delta != 0:
            deal_direction = max_part_delta / abs(max_part_delta)
        # Сигнал: Конец дня
        else:
            deal_direction = 1

        # если  vol < 0 (продажа), то коэффициент 0,999 иначе  1,001
        ref_commission = float(trade.get('commission', 0.001))
        commission_cf = 1 + deal_direction * ref_commission

        if trade.get('vol_type', 'pnl') == 'pnl':
            # Основной кейс
            if algo_type == 1:

                if 'open' in operation:
                    nom_part = round((max_part * acc_vol * prev_asset) / price, 0)
                else:
                    nom_part = round(acc_vol / (acc_vol - vol) * prev_nom, 0)

            # для ALL портфеля
            else:
                nom_part = round((max_part * vol * prev_asset) / price, 0) + prev_nom

            trade_size = nom_part - prev_nom

        else:
            # TODO: неверный расчет nom_part для набора позиции
            nom_part = trade['acc_vol'] + trade['vol']
            trade_size = trade['vol']

        # Если по стратегии нужно открыть позицию - минимальная сделка = 1 лот
        if int(trade_size) == 0 and round(vol, 5) != 0.00000:
            nom_part = prev_nom + int(deal_direction)
            trade_size = int(deal_direction)

        # логика минимального открытия
        if operation == Operation(PositionTypes.LONG).OPEN and trade_size < 0 and call_for_pnl:
            nom_part = 1
            trade_size = 1

        if operation == Operation(PositionTypes.SHORT).OPEN and trade_size > 0 and call_for_pnl:
            nom_part = -1
            trade_size = -1

        # Итого кэш
        cash_for_change_position = -1 * (trade_size * price * commission_cf)
        commission_size = abs(ref_commission * trade_size * price)

        return nom_part, trade_size, cash_for_change_position, price, shares_in_lot, commission_size

    def get_avg_open_price(self, ticker):
        trades = self.trades[ticker]
        total_shares_price = sum(map(lambda x: (x.quantity*x.price), trades))
        total_quantity = sum(map(lambda x: x.shares_in_lot if x.quantity == 0 else x.quantity*x.shares_in_lot, trades))
        if total_quantity == 0:
            total_quantity = 1
        avg_price = total_shares_price/total_quantity
        return avg_price

    def close_hist_trades(self, ticker):
        for trade in self.trades_results[ticker]:
            trade.status = 1

    def get_rest(self, ticker):
        t = self.trades[ticker]
        s = sum(map(lambda x: x.quantity - x.i*x.position_factor, t))
        if s is None:
            s = 0
        return s

    def open_trade(self, **kwargs):

        # read param from df row
        ticker = kwargs['ticker']
        direction = kwargs['operation']
        position_type = kwargs['operation'].replace('open ', '').replace('close ', '')
        open_vol = kwargs['vol']
        date = kwargs['tradedate']
        cash_change = kwargs.get('cash_for_change_position', 0)

        # Переворотные сделки не открываем пока
        current_position = self.portfolio.positions[ticker]['position_type']
        if position_type not in ('deposit', current_position) and current_position != 'wait':
            return
        # данные для сделки
        trade_params = [direction, open_vol, date]

        # Сохраняем сделку расчетов
        self.trades[ticker].append(Trade(*trade_params, **kwargs))

        # Сохраняем сделку для истории
        result_trade = Trade(*trade_params,
                             remain_quantity=self.get_rest(ticker),
                             **kwargs
                             )

        self.trades_results[ticker].append(result_trade)

        # Пересчитываем портфель
        self.portfolio.change_position(ticker,
                                       trade_size=open_vol,
                                       cash_change=cash_change,
                                       avg_open_price=self.get_avg_open_price(ticker),
                                       vol_origin=kwargs['vol_origin'],
                                       position_type=position_type,
                                       commission=kwargs.get('commission', 0),
                                       cost_field=kwargs.get('cost_field', 'real_part_cost'),
                                       )
        return result_trade

    def fix_result(self, ticker, close_volume, close_date):
        close_profit = 0
        for d in self.trades[ticker]:
            try:
                while len(close_volume) > 0:
                    remain_quantity, fill = d.__next__()
                    profit = (close_volume.pop().price - fill.price)*d.position_factor
                    d.profit += profit
                    close_profit += profit
                    if remain_quantity == 0:
                        raise StopIteration

            except StopIteration:
                d.status = 1
                d.close_date = close_date
                continue
        return close_profit

    def close_trade(self, **kwargs):

        # Параметры сделки
        ticker = kwargs['ticker']
        direction = kwargs['operation']
        close_vol = int(kwargs['vol'])
        price = kwargs['price']
        close_date = kwargs['tradedate']
        cash_change = kwargs.get('cash_for_change_position', 0)

        # Вспомогательные переменные
        close_range = range(abs(close_vol))
        close_volume = deque([Fill(price) for i in close_range])

        # Квитируем часть объема по FIFO
        close_profit = self.fix_result(ticker, close_volume, close_date)

        # Удаляем закрытые сделки из коллекции
        self.trades[ticker] = [d for d in self.trades[ticker] if d.status == 0]

        # Сохраняем сделку для истории
        trade_params = [direction, close_vol, close_date]

        params = dict(**kwargs)
        params['close_profit'] = close_profit

        # вычисляем остаток по сделкам
        remain_quantity = self.get_rest(ticker)
        params['remain_quantity'] = remain_quantity

        # Если сделка полностью закрыта ставим ей соотв. статус и дату
        if int(remain_quantity) == 0:
            # Закрываем все сделки в истории по тикеру
            self.close_hist_trades(ticker)
            params['status'] = 1
            params['close_date'] = datetime.now().strftime('%Y-%m-%d')

        # создаем запись для истории
        result_trade = Trade(*trade_params, **params)

        # Сохраняем ее в массив результатов
        self.trades_results[ticker].append(result_trade)

        # Меняем состояние портфеля
        self.portfolio.change_position(ticker,
                                       trade_size=close_vol,
                                       cash_change=cash_change,
                                       avg_open_price=self.get_avg_open_price(ticker),
                                       vol_origin=kwargs['vol_origin'],
                                       fixed_profit=close_profit,
                                       commission=kwargs.get('commission', 0),
                                       cost_field=kwargs.get('cost_field'),
                                       )
        return result_trade

    def execute(self, **kwargs):
        operation_type = kwargs.get('type', 'trade')

        if self.portfolio.start_asset == 0:
            self.portfolio.set_start_asset(**kwargs)

        if operation_type == 'eod':
            res = self.portfolio.recalc_portfolio_position(**kwargs)
            return res
        elif operation_type == 'dvd':
            self.portfolio.add_divs(**kwargs)
        elif operation_type == 'trade':

            result_trade = None
            # Основной алгоритм расчета лотов из долей ИИ
            nom_part, trade_size, cash_for_change_position, price, shares_in_lot, commission_size = self.get_trade_size(
                trade=kwargs)

            # Доп. параметры сделки
            kwargs['vol_origin'] = kwargs.get('vol_origin', kwargs['vol'])
            kwargs['portfolio_id'] = kwargs.get('portfolio_id')
            kwargs['client_id'] = kwargs.get('client_id')
            kwargs['vol_prc'] = round(kwargs['vol']*kwargs['max_part']*100, 5) if kwargs['max_part'] is not None else 0
            kwargs['vol'] = trade_size
            kwargs['acc_vol'] = nom_part
            kwargs['shares_in_lot'] = shares_in_lot
            kwargs['cash_for_change_position'] = cash_for_change_position
            kwargs['commission'] = commission_size

            # Корректируем цену
            if kwargs['price'] is None:
                kwargs['price'] = float(kwargs['close'])

            # Это биржевая цена
            kwargs['market_price'] = kwargs['price']

            # Это цена за лот
            kwargs['price'] = price

            # Выполняем сделку
            trade_type = kwargs.get('operation', '')

            if 'open' in trade_type:
                result_trade = self.open_trade(**kwargs)
            elif 'close' in trade_type:
                result_trade = self.close_trade(**kwargs)

            if kwargs.get('return_trade', False):
                return result_trade

    def convert_quik_trade(self, **quik_trade):
        shares_in_lot = int(quik_trade['shares_in_lot'])
        return {
            'ticker': quik_trade['ticker'],
            'trade_date_time': quik_trade['trade_date_time'].replace('T', ' '),
            'market_price': quik_trade['price'],
            'value': quik_trade['value'],
            'accruedint': quik_trade['accruedint'],
            'price': float(quik_trade['price']*shares_in_lot),
            'shares_in_lot': shares_in_lot,
            'commission': float(quik_trade['commission'])*quik_trade['price']*shares_in_lot*float(quik_trade['vol']),
            'portfolio_id': self.portfolio.portfolio_id or quik_trade.get('portfolio_id'),
            'client_id': self.portfolio.client_id,
            'trade_num': quik_trade.get('trade_num')
        }

    def execute_quik_trade(self, **kwargs):
        # Обработка позиций
        operation = kwargs['operation']
        ticker = kwargs['ticker']
        qty = int(kwargs['vol']) if operation == 'Купля' else -1*int(kwargs['vol'])

        # Текущий объем позиции
        try:
            current_value = int(self.portfolio.positions[ticker]['nom_part'])
        except Exception:
            current_value = 0

        # Шорт или лонг ?
        quik_position_type = self.define_quik_position_type(current_value, **kwargs)

        # Создаем сделку по формату RAIS
        rais_trade = Trade(operation, qty, kwargs['tradedate'],
                           remain_quantity=self.get_rest(ticker),
                           **self.convert_quik_trade(**kwargs))

        # Выполняем сокращение позиции по сделкам
        if (operation == QuikTradeTypes.SELL and quik_position_type == PositionTypes.LONG.value) or \
                (operation == QuikTradeTypes.BUY and quik_position_type == PositionTypes.SHORT.value):
            close_range = range(abs(qty))
            close_volume = deque([Fill(rais_trade.price) for i in close_range])
            rais_trade.profit = self.fix_result(ticker, close_volume, close_date=kwargs['tradedate'])
            position_coefficient = 1
        # Набор позиции
        else:
            position_coefficient = -1
            # Сохраняем сделку расчетов
            self.trades[ticker].append(rais_trade)

        # Считаем остаток по позиции
        rais_trade.remain_quantity = self.get_rest(ticker=ticker)

        # Меняем состоянии портфеля
        cash_change = position_coefficient * float(kwargs['value'])
        avg_open_price = self.get_avg_open_price(ticker)
        vol_origin = self.portfolio.positions[ticker].vol or 0

        self.portfolio.change_position(ticker, trade_size=qty,
                                       cash_change=cash_change,
                                       avg_open_price=avg_open_price,
                                       vol_origin=vol_origin,
                                       fixed_profit=rais_trade.profit,
                                       commission=kwargs.get('commission', 0),
                                       quik_position_type=quik_position_type,
                                       cost_field='real_part_cost'
                                       )

        res = rais_trade if kwargs.get('return_trade', True) else None

        return res
