from app.domain.interfaces import IPortfolioDAO
from typing import Optional


class PortfolioHomePageUseCase:

    def __init__(self, login: str, portfolio_dao: IPortfolioDAO, portfolio_id: Optional[int] = None):
        self.db = portfolio_dao
        self.portfolio_id = portfolio_id

        self.login = login

    def execute(self):
        """ Список мастер портфелей """
        res = self.db.get_client_home_page(self.login, self.portfolio_id)
        if len(res) == 1:
            res = res[0]
        return res
