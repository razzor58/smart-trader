import unittest
import requests as r
from . import SERVICE_URL


class TestCheckPin(unittest.TestCase):
    """    Метод отправки пакетки в ProxyFix

   """
    resp = None

    def setUp(self):
        method = '/pin/checkPin'
        url = SERVICE_URL + method
        message = {
            "pin": 123123
        }
        self.resp = r.post(url, json=message).json()

    def test_check_pin_resp_is_valid(self):
        test_result = isinstance(self.resp, dict)
        self.assertTrue(test_result)

    def test_resp_data(self):
        """
        {
            "data": {
                "checkStatus": false,
                "dropStatus": false,
                "leftAttempts": 2
            },
            "success": true,
            "error": null
        }
        :return:
        """
        try:
            self.assertTrue(self.resp['data']['checkStatus'])
        except KeyError:
            print(self.resp)


if __name__ == '__main__':
    unittest.main()
