from app.domain.tools import get_chart_data
from app.domain.usecases import PortfolioUseCase
from app.domain.interfaces import IPortfolioDAO
from app.domain.constants import Currencies, VIRTUAL_CASH, TabNames, DividendTabNames
from app import settings


class StatsShowUseCase(PortfolioUseCase):

    def __init__(self,
                 portfolio_id: int,
                 login: str,
                 portfolio_dao: IPortfolioDAO) -> None:

        super().__init__(portfolio_id, login, portfolio_dao)
        self.round_digit = 7

    def get_divs(self, **kwargs):
        portfolio_id = kwargs['portfolio_id']
        div_mode = kwargs.get('div_mode')
        if div_mode == DividendTabNames.EXPECTED:
            rows = self.db.get_expected_dividends_for_api(portfolio_id, kwargs['client_id'])
        elif div_mode == DividendTabNames.RECEIVED:
            rows = self.db.get_received_dividends_for_api(portfolio_id, kwargs['client_id'])
        else:
            rows = []
        return rows

    def positions_to_api(self, positions):
        res = []
        asset = self.portfolio.state.start_asset
        for ticker, ticker_data in positions.items():
            # Кэш не отправляем
            if ticker != Currencies.SUR.value:
                # Соответствует или нет
                accordance = False if ticker_data['nom_part'] != ticker_data['recommend_part'] else True
                ticker_data.update({'accordance': accordance})

                if asset is None or int(asset) == 0:
                    recommend_part_prc = 0
                    nom_part_prc = 0
                else:
                    # Рекомендованный процент в портфеле
                    try:
                        recommend_part_prc = abs(round(
                            ticker_data['recommend_part'] * ticker_data['shares_in_lot'] * ticker_data[
                                'price'] / asset * 100, 2))

                        # Текущий процент в портфеле
                        real_part_cost = ticker_data['nom_part'] * ticker_data['shares_in_lot'] * ticker_data['price']

                    except Exception:
                        real_part_cost = 0
                        recommend_part_prc = 0
                        # TODO: проконтролировать ошибку
                        # logger.error(e)
                        # logger.error(f"ticker_data: {ticker_data}")
                        # logger.error(f"asset: {asset}")

                    nom_part_prc = abs(round(real_part_cost / asset * 100, 2))

                ticker_data.update({'nom_part_prc': nom_part_prc})
                ticker_data.update({'recommend_part_prc': recommend_part_prc})

                if ticker == VIRTUAL_CASH:
                    ticker_data['cost'] = self.portfolio.state.start_asset - self.portfolio.state.shares

                # Выводим только те бумаги, которые "не нулевые"
                if ticker_data['recommend_part'] != 0 or ticker_data['nom_part'] != 0 or ticker == VIRTUAL_CASH:
                    res.append(ticker_data)

        return res

    def get_partial_resp(self, data, **kwargs):
        last_id = kwargs.get('after')
        data_len = len(data)

        # если если записи
        if data_len > 0:
            # Минимальный размер страницы
            page_size = min([data_len, settings.RECORD_TO_PAGE])
            if not last_id:
                paged_data = data[:page_size]
                last_record_on_page = paged_data[-1]['id']
                has_more = len(data) > page_size
            else:
                filtered_data = [i for i, x in enumerate(data) if x['id'] == last_id]

                # Если передан несуществующий номер - берем либо первые N записей либо весь массив
                page_start = 0 if len(filtered_data) == 0 else filtered_data[0] + 1
                page_end = min([page_start + settings.RECORD_TO_PAGE, data_len])
                last_record_on_page = data[:page_end][-1]['id']
                paged_data = data[page_start:page_end]
                has_more = len(data) > page_end
        # если нет сделок
        else:
            paged_data = []
            has_more = False
            last_record_on_page = None

        partial_data = {
            'hasMore': has_more,
            'lastValue': last_record_on_page,
            'items': paged_data
        }

        return partial_data

    def resp_with_add_data(self, data, tab_name, **kwargs):
        state = self.portfolio.state

        resp = {
            'data': {
                'asset': state.start_asset,
                'synchronized': int(state.synchronized),
                'accordance': state.accordance or 0
            }
        }

        # Основной список
        resp['data'].update({'items': data})

        # для списков  - добавляем пагинацию
        if tab_name in (TabNames.TRADES, TabNames.DIVIDENDS):
            resp['data'].update(self.get_partial_resp(data, **kwargs))

        return resp

    def execute(self, **kwargs):
        tab_name = TabNames(kwargs.get('tab_name'))
        if tab_name == TabNames.STATS:
            stat = self.portfolio.get_portfolio_reports()
            data = get_chart_data(stat, self.round_digit)
        elif tab_name == TabNames.BIDS:
            data = self.portfolio.get_portfolio_bids()
        elif tab_name == TabNames.TRADES:
            data = self.portfolio.get_trades(for_stat=True)
        elif tab_name == TabNames.DIVIDENDS:
            data = self.get_divs(**kwargs, client_id=self.client_id)
        else:
            raw_data = self.portfolio.get_saved_positions(for_stats_api=True)
            data = self.positions_to_api(raw_data)

        return self.resp_with_add_data(data,
                                       tab_name,
                                       after=kwargs.get('after'))
