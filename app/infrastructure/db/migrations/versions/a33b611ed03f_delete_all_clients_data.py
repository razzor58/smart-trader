"""delete all clients data

Revision ID: a33b611ed03f
Revises: 6eabfe7cfc31
Create Date: 2020-06-30 18:55:14.148067

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a33b611ed03f'
down_revision = '6eabfe7cfc31'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
