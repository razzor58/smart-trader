from unittest.mock import patch
from pytest import fixture
from datetime import datetime
from app.domain.entities.plain import Portfolio
from app.domain.constants import Operation, PositionTypes, VIRTUAL_CASH, Currencies
from app import create_app
from app.domain.entities.plain import Client

PORTFOLIO_ID = 25
TEST_TICKER = 'GAZP'
TEST_LOGIN = 'owner'
TEST_CLIENT_CODE = 'B1/RKS101T'


@patch('app.infrastructure.db.domain_dao.SessionContext')
def get_db_mock(session_context_mock):
    """ Эмулируем данные вместо БД """

    db_mock = session_context_mock()
    db_mock.get_or_create_client.return_value = (1, 2)
    db_mock.get_portfolio_origin.return_value = Portfolio(dict(name='Test', currency=Currencies.RUR, risk_level=0,
                                                               backtest_type='KEEP', iir_enabled=1))
    db_mock.get_current_state.return_value = dict(cash=0, cash_deposit=0,
                                                  start_asset=0, total_divs=0, commission_size=0)

    db_mock.get_depo_limit.return_value = dict(SUR=0)
    db_mock.get_current_positions.return_value = {
        VIRTUAL_CASH: dict(nom_part=0,
                           cost=0,
                           real_part_cost=0,
                           ticker=VIRTUAL_CASH,
                           portfolio_id=PORTFOLIO_ID,
                           position_type=PositionTypes.WAIT,
                           shares_in_lot=1,
                           recommend_part=0,
                           id=1
                           ),
        TEST_TICKER: dict(nom_part=0,
                          operation=Operation(PositionTypes.LONG).OPEN,
                          acc_vol=0,
                          vol=0.5,
                          max_part=1,
                          ticker='GAZP',
                          real_part_cost=0,
                          portfolio_id=PORTFOLIO_ID,
                          tradedate=datetime.now(),
                          price=200,
                          rur_price=200,
                          fixed_profit=0,
                          position_type=PositionTypes.LONG,
                          recommend_part=0,
                          shares_in_lot=1,
                          id=2)
    }

    return db_mock


@fixture
def test_login():
    return TEST_LOGIN


@fixture
def test_client_code():
    return TEST_CLIENT_CODE


@fixture
def test_ticker():
    return TEST_TICKER


@fixture
def db_mock():
    """ Return values instead of Database calls """
    return get_db_mock()


@fixture
def portfolio_id():
    """ Return portfolio_id for test cases """
    return PORTFOLIO_ID


@fixture
def new_asset(asset_size):
    """ Return different asset sizes """
    return asset_size


@fixture
def client_risk_level(risk_level):
    """ Return different client risk level """
    return risk_level


@fixture
@patch('app.get_flask_metrics_class')
def app(mock):
    return create_app()


@fixture
def app_client():
    return Client({'id': 1, 'client_code': 'B1/123123', 'login': TEST_LOGIN})
