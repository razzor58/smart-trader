import itertools
from typing import Any
from datetime import datetime, timedelta
from collections import defaultdict
from sqlalchemy import case, join, outerjoin, or_, select, Integer
from sqlalchemy.sql.expression import desc, func, literal_column
from sqlalchemy.exc import IntegrityError

from app.infrastructure.db.meta.engine import CommonSession

from app.infrastructure.db.querysets import SessionQueries
from app.infrastructure.db.meta.tools import (get_query_rows, model_qs_as_dict, update_by_dict,
                                              row2dict, filter_values)
from app.infrastructure.db.meta.models import (
    Clients, ClientsPositions, ClientsPortfolios, ClientsBids, ClientsProfiles,
    Portfolio, PortfolioReports, PortfolioTickers, PortfolioPages, OnlineReports,
    Trades, Deposits, DepoLimits, Dividends, Tickers, Industries, MoneyLimits, Agreements, TradeCodes,
    Issuers, LastQuotes, TariffRules, QuikTrades, RiskProfiles, BidTriggers, SignalsTemplates
)

from app.domain.constants import BidRecord, BidStatus, Landings, Limits
from app.domain.interfaces import IPortfolioDAO
from app.infrastructure.tools import KafkaMessageNotUniqueException


def get_session():
    return SessionContext()


class SessionContext(CommonSession, IPortfolioDAO):

    def __init__(self):
        super().__init__()
        self.qs = SessionQueries(self.session)

    def session_commit(self):
        self.session.commit()

    def session_close(self):
        self.session.close()

    def commit_and_close(self):
        self.session.commit()
        self.session.close()

    def flush(self):
        self.session.flush()

    def get_rows(self, sql, as_dict=False):

        db_rows = [r for r in self.session.execute(sql)]
        if len(db_rows) > 0:
            if as_dict:
                res = [dict(r) for r in db_rows] if len(db_rows) > 0 else None
            else:
                res = [r for r in db_rows] if len(db_rows) > 0 else None
        else:
            res = []
        return res

    def set_client_allowed(self, client_id):
        self.update_record_by_filter(Clients,
                                     {'id': client_id},
                                     {'trading_allowed': True})

    def add_row(self, obj):
        try:
            self.session.add(obj)
            self.session_commit()
        except IntegrityError:
            self.session.rollback()
            raise KafkaMessageNotUniqueException

    def get_row(self, sql):
        db_rows = [r for r in self.session.execute(sql)]
        res = [r for r in db_rows][0] if len(db_rows) > 0 else None
        return res

    def create_positions(self, positions: Any):
        self.session.add_all([
            ClientsPositions(**position.save_fields) for name, position in positions.items()
        ])
        self.session.flush()

    def create_portfolio(self, portfolio: Any):

        portfolio = ClientsPortfolios(**portfolio.save_field)
        self.session.add(portfolio)
        self.session.flush()

        return portfolio.id

    def get_last_report_id(self):
        return self.session.query(func.max(OnlineReports.internalNum)).scalar() or 0

    def save_online_report(self, values):
        """Сохранение сообщения со статусом обработки биржевой заявки """
        report = self.session.query(OnlineReports) \
            .filter(OnlineReports.internalNum == values['internalNum']) \
            .first()

        if report:
            return False
        else:
            # В отчете могут придти много разных полей, не все из которых есть в БД
            self.session.add(OnlineReports(**filter_values(OnlineReports, values)))
            self.session_commit()
            return True

    def clear_pnl_tables(self, client_id=None, portfolio_id=None):
        """ Удаляются записи с результатами работы портфеля """
        self.session.query(ClientsPortfolios) \
            .filter(ClientsPortfolios.client_id == client_id) \
            .filter(ClientsPortfolios.portfolio_id == portfolio_id) \
            .delete()

        self.session.query(ClientsPositions) \
            .filter(ClientsPositions.client_id == client_id) \
            .filter(ClientsPositions.portfolio_id == portfolio_id) \
            .delete()

        self.session.query(ClientsBids) \
            .filter(ClientsBids.client_id == client_id) \
            .filter(ClientsBids.send_status.in_([BidRecord.NEW])) \
            .update({ClientsBids.send_status: BidRecord.ARCHIVE}, synchronize_session=False)

        self.session.query(PortfolioReports) \
            .filter(PortfolioReports.client_id == client_id) \
            .filter(PortfolioReports.portfolio_id == portfolio_id) \
            .delete()

        # Если выполняется бэктест стратегии - то удаляем дополительно и историю сделок
        if client_id == 1:
            self.session.query(Trades) \
                .filter(Trades.client_id == client_id) \
                .filter(Trades.portfolio_id == portfolio_id) \
                .delete()

    def get_all_old(self):
        sql = select([Portfolio])
        res = defaultdict(dict)
        for row in self.session.execute(sql):
            portfolio_id = row.id
            res[portfolio_id]['id'] = portfolio_id
            res[portfolio_id]['name'] = row.name
            res[portfolio_id]['recommended_cash'] = row.recommended_cash
            res[portfolio_id]['description'] = row.description
            res[portfolio_id]['income_avg'] = row.income_avg
            res[portfolio_id]['income_total'] = row.income_total
            res[portfolio_id]['max_dd'] = row.max_dd
            res[portfolio_id]['signals_cnt_avg'] = row.signals_cnt_avg

            if "structure" not in res[portfolio_id]:
                res[portfolio_id]['structure'] = dict()
            res[portfolio_id]['structure'][row.ticker] = row.hardcode_part

        return [v for k, v in dict(res).items()]

    def get_all(self):
        res = []
        sql = select([Portfolio])
        result_proxy = [r for r in self.session.execute(sql)]
        for row in result_proxy:
            res.append({
                "id": row[Portfolio.id],
                "name": row[Portfolio.name],
                'recommended_cash': row[Portfolio.recommended_cash],
                'income_avg': row[Portfolio.income_avg],
                'income_total': row[Portfolio.income_total],
                'max_dd': row[Portfolio.max_dd],
                'signals_cnt_avg': row[Portfolio.signals_cnt_avg],
            })
        return res

    def get_client_home_page(self, login, portfolio_id=None):
        return self.qs.get_client_home_page(login, portfolio_id)

    # for pnl loader -->
    def get_tickers(self, **kwargs):
        return self.qs.get_tickers(**kwargs)

    def get_volumes(self, **kwargs):
        return self.qs.get_volumes(**kwargs)

    def get_date_options(self, **kwargs):
        return self.qs.get_date_options(**kwargs)

    def get_quotes(self, **kwargs):
        return self.qs.get_quotes(**kwargs)

    def get_tariffs(self, **kwargs):
        return self.qs.get_tariffs(**kwargs)

    # <--- for pnl loader

    # admin main page
    def get_strategy_details(self, **kwargs):
        return self.qs.get_strategy_details(**kwargs)

    def get_portfolio_list(self, portfolio_id=None, client_id=None):
        if portfolio_id:
            if client_id:
                # Портфель конкретного клиента для подключения
                sql = select([ClientsPortfolios]) \
                    .where(ClientsPortfolios.portfolio_id == portfolio_id) \
                    .where(ClientsPortfolios.client_id == client_id)
            else:
                # Список для отправки сигнала
                sql = select([ClientsPortfolios]) \
                    .where(ClientsPortfolios.portfolio_id == portfolio_id)
        else:
            # Для переоценки в конце дня нужны все портфели
            sql = select([ClientsPortfolios])

        return self.get_rows(sql, as_dict=True)

    def get_client_portfolios(self, client_id):
        return get_query_rows(self.session.query(ClientsPortfolios.portfolio_id)
                              .filter(ClientsPortfolios.client_id == client_id).all())

    def get_master(self, portfolio_id):
        sql = select([Portfolio]).where(Portfolio.id == portfolio_id)
        result_proxy = [r for r in self.session.execute(sql)]
        if len(result_proxy) == 0:
            res = None
        else:
            row = result_proxy[0]
            res = {
                "id": row[Portfolio.id],
                "name": row[Portfolio.name],
                'recommended_cash': row[Portfolio.recommended_cash],
                'income_avg': row[Portfolio.income_avg],
                'income_total': row[Portfolio.income_total],
                'max_dd': row[Portfolio.max_dd],
                'signals_cnt_avg': row[Portfolio.signals_cnt_avg],
            }
        return res

    def get_master_structure(self):
        return dict([(row.id, row.name) for row in
                     self.session.query(Portfolio.id, Portfolio.name).order_by(desc(Portfolio.id)).all()])

    def get_all_from_table(self, table_type):

        if table_type == "tickers":
            res = model_qs_as_dict(self.session.query(Tickers)
                                   .filter(or_(Tickers.mat_date >= datetime.now(), Tickers.type == 'stock'))
                                   .all())
        elif table_type == "issuers":
            res = model_qs_as_dict(self.session.query(Issuers).all())

        elif table_type == "industries":
            res = model_qs_as_dict(self.session.query(Industries).all())

        elif table_type == "pages":
            qs = self.session.query(PortfolioPages.landing_page).distinct().all()
            res = get_query_rows(qs)
        else:
            res = []
        return res

    def get_portfolio_origin(self, portfolio_id=None, as_dict=False):
        if as_dict:
            if portfolio_id:
                res = row2dict(self.session.query(Portfolio).get(portfolio_id))
            else:
                res = model_qs_as_dict(self.session.query(Portfolio).order_by(Portfolio.id))
        else:
            res = self.get_row(select([Portfolio]).where(Portfolio.id == portfolio_id))
        return res

    def find_master_portfolio(self, fin_target_id):
        return self.session.query(Portfolio).filter(Portfolio.fin_target_uuid == fin_target_id).first()

    def update_master_portfolio(self, portfolio_id, portfolio_data):
        self.session.query(Portfolio).filter_by(id=portfolio_id).update(portfolio_data)

    def update_portfolio_tickers(self, portfolio_id, ticker, data):
        self.session.query(PortfolioTickers).filter_by(portfolio_id=portfolio_id, ticker=ticker).update(data)

    def delete_portfolio_tickers(self, portfolio_id, ticker):
        self.session.query(PortfolioTickers).filter_by(portfolio_id=portfolio_id, ticker=ticker).delete()

    def create_master_portfolio(self, _values):
        portfolio = Portfolio(**_values)
        self.session.add(portfolio)
        self.session.flush()
        return portfolio.id

    def create_portfolio_tickers(self, data):
        row = PortfolioTickers(**data)
        self.session.add(row)

    def get_master_portfolio_tickers(self, portfolio_id):
        return self.session.query(PortfolioTickers).filter(PortfolioTickers.portfolio_id == portfolio_id).all()

    def save_rows(self, table, rows):
        db_obj_list = []

        if table == 'pages':
            model = PortfolioPages
        else:
            model = None

        if model and len(rows) > 0:
            for row in rows:
                db_obj_list.append(model(**row))

            self.session.add_all(db_obj_list)

    # TODO: replace by python code
    def get_master_position(self, portfolio_id, trade_date=None, **kwargs):
        """ Запрос из usp_initPositions """
        positions = defaultdict(dict)
        params = [portfolio_id, trade_date]
        if trade_date is None:
            # TODO: replace by python code
            sql = """[dbo].[usp_initPositionsClients] '{}'""".format(portfolio_id)
        elif kwargs.get('only_begin_positions'):
            sql = """[dbo].[usp_getStartPositions] '{}'""".format(portfolio_id)
        else:
            sql = """[dbo].[usp_initPositions] '{}'""".format("','".join([str(e) for e in params if e is not None]))

        for row in self.get_rows(sql):
            positions[row.ticker] = dict(max_part=row.hardcode_part, tradedate=row.tradedate, acc_vol=0, status=0,
                                         open_date=None,
                                         vol=row.acc_vol, price=row.price, rur_price=row.rur_price, nom_part=0,
                                         avg_open_price=0,
                                         ticker=row.ticker, operation=row.operation, shares_in_lot=row.shares_in_lot,
                                         position_type=row.position_type, div_size=0, first_trade=True,
                                         fixed_profit=0, calculate_profit=0, cost=0, portfolio_id=portfolio_id,
                                         commission=row.commission,
                                         ticker_currency=row.ticker_currency, recommend_part=0, real_part_cost=0,
                                         last_signal_time=row.last_signal_time
                                         )
        return positions

    def get_portfolio_stat(self, portfolio_id, client_id, limit_rows=None):
        """Запрос из blackbox.dbo.portfolio_reports"""
        pr = PortfolioReports
        if limit_rows:
            sql = select([pr]) \
                .where(pr.portfolio_id == portfolio_id) \
                .where(pr.client_id == client_id) \
                .order_by(pr.tradedate.desc()) \
                .limit(limit_rows)
        elif portfolio_id is None:
            sql = select([pr]) \
                .where(pr.client_id == client_id) \
                .order_by(pr.tradedate.asc())

        else:
            sql = select([pr]) \
                .where(pr.portfolio_id == portfolio_id) \
                .where(pr.client_id == client_id) \
                .order_by(pr.tradedate)

        return self.get_rows(sql)

    def get_ticker(self, ticker):
        return self.session.query(Tickers).filter(Tickers.ticker == ticker).first()

    def get_client(self, login):
        return self.session.query(Clients).filter(Clients.login == login).first()

    def get_du_chart(self):
        """Запрос из blackbox.dbo.portfolio_reports"""
        sql = 'SELECT *  FROM [blackbox].[dbo].[du_income]'
        return self.get_rows(sql)

    def get_portfolio_bids(self, portfolio_id, client_id):
        """ Новый Запрос для вкладка заявки """
        t = Tickers
        b = ClientsBids
        qs = (self.session.query(
            b.id,
            b.bucket_num,
            t.code,
            t.currency,
            b.client_nominal,
            b.operation,
            b.portfolio_id,
            b.ex_value.label('quik_cost'),
            b.ax_price.label('quik_price'),
            t.title,
            t.type,
            b.client_accept_time.label('bid_create_time'),
            b.quik_trade_vol,
            b.shares_in_lot,
            b.exec_status,
            case(
                {2: True},
                value=b.exec_status,
                else_=False,
            ).label('status'),
            case([
                (b.exec_status == 2, BidStatus.EXECUTED),
                (b.exec_status == 1, BidStatus.PART_EXECUTED),
                (b.exec_status == 0, BidStatus.ACCEPTED)
            ],
                else_=b.exec_message
            ).label('status_message'),
            func.dense_rank().over(partition_by=b.client_id, order_by=desc(b.bid_time)).label('row_number')
        )
              .filter(t.ticker == b.ticker)
              .filter(b.client_id == client_id)
              .filter(b.portfolio_id == portfolio_id)
              .filter(b.send_status.notin_([BidRecord.ARCHIVE, BidRecord.NEW]))
              ).cte()

        main_query = (self.session.query(qs)
                      .filter(qs.c.row_number == 1)
                      .order_by(qs.c.bid_create_time.desc())
                      .all())

        return get_query_rows(main_query)

    def get_portfolio_id_for_manage(self, **kwargs):
        """Запрос из blackbox.dbo.portfolio_tickers"""
        portfolio_id = kwargs.get('portfolio_id')
        if portfolio_id:
            sql = select([Portfolio]).where(Portfolio.id == portfolio_id)
        else:
            sql = select([Portfolio]).where(Portfolio.status == 1)
        return self.get_rows(sql)

    def get_portfolios_tickers_with_prices(self, landing_page: Landings):
        now = datetime.now()
        sql = (self.session.query(Portfolio.id,
                                  Portfolio.name,
                                  PortfolioTickers.ticker,
                                  Tickers.title,
                                  Tickers.short_description.label('short_title'),
                                  PortfolioTickers.issuer,
                                  Industries.name.label('industry'),
                                  PortfolioTickers.full_description,
                                  PortfolioTickers.short_description,
                                  PortfolioTickers.target_price,
                                  PortfolioTickers.invest_period,
                                  PortfolioTickers.invest_period_step,
                                  PortfolioTickers.invest_period_end_date,
                                  PortfolioTickers.video,
                                  Tickers.has_offer,
                                  Tickers.offer_date,
                                  Tickers.ytm,
                                  Tickers.secur_code,
                                  Tickers.classcode,
                                  Tickers.type.label('ticker_type'),
                                  Tickers.currency,
                                  Tickers.isin,
                                  Tickers.shares_in_lot,
                                  LastQuotes.close)
               .filter(PortfolioTickers.portfolio_id == Portfolio.id)
               .filter(PortfolioTickers.ticker == Tickers.ticker)
               .filter(PortfolioTickers.industry_id == Industries.id)
               .filter(Tickers.secur_code == LastQuotes.securcode)
               .filter(Tickers.classcode == LastQuotes.classcode)
               .filter(PortfolioTickers.valid_from <= now)
               .filter(PortfolioTickers.valid_to >= now)
               .filter(PortfolioTickers.status.is_(None))
               .filter(Portfolio.id == PortfolioPages.portfolio_id)
               .filter(PortfolioPages.landing_page == landing_page.value)
               .order_by(PortfolioTickers.portfolio_id, PortfolioTickers.ticker)
               )

        qs = sql.all()

        return qs

    def get_portfolio_tickers(self, portfolio_id, ticker=None):
        now = datetime.now()
        qs = (self.session.query(PortfolioTickers.ticker,
                                 PortfolioTickers.hardcode_part,
                                 PortfolioTickers.full_description,
                                 PortfolioTickers.short_description,
                                 PortfolioTickers.invest_period,
                                 PortfolioTickers.invest_period_step,
                                 func.to_char(
                                     PortfolioTickers.invest_period_end_date, 'YYYY-MM-DD'
                                 ).label('invest_period_end_date'),
                                 PortfolioTickers.target_price,
                                 Tickers.title,
                                 PortfolioTickers.video,
                                 Portfolio.name)
              .filter(PortfolioTickers.ticker == Tickers.ticker)
              .filter(PortfolioTickers.portfolio_id == portfolio_id)
              .filter(PortfolioTickers.portfolio_id == Portfolio.id)
              .filter(PortfolioTickers.valid_from <= now)
              .filter(PortfolioTickers.valid_to >= now)
              .filter(PortfolioTickers.status.is_(None))
              )
        if ticker:
            qs = qs.filter(PortfolioTickers.ticker == ticker)

        rows = qs.all()
        res = get_query_rows(rows)
        return res

    def get_current_positions(self, portfolio_id, client_id=None, **kwargs):
        positions = {}
        p = ClientsPositions
        t = Tickers
        j = outerjoin(p, t, p.ticker == t.ticker)
        # columns_list = [p.id, p.client_id, p.portfolio_id, p.status, p.ticker, p.position_type,
        #                 p.nom_part, p.open_date, p.calculate_profit, p.fixed_profit,
        #                 p.cost, p.real_part_cost, p.avg_open_price, p.shares_in_lot,
        #                 p.price, p.rur_price, p.tradedate, p.div_size,
        #                 p.acc_vol, p.vol, p.max_part,
        #                 p.operation, p.ticker_currency, p.recommend_part,
        #                 p.is_signal_active, p.signal_size, p.last_signal_time
        #               , t.code, t.title, t.type]

        # для модельных портфелей
        try:
            client_id = int(client_id)
        except Exception:
            client_id = 1

        if int(client_id) == 1 or kwargs.get('for_stats_api', False):
            sql = select([p, t.code, t.title, t.type]).select_from(j) \
                .where(p.client_id == client_id) \
                .where(p.portfolio_id == portfolio_id)

        # для клиенстких портфелей запрашиваем все позиции
        else:
            sql = select([p, t.code, t.title, t.type]).select_from(j) \
                .where(p.client_id == client_id)

        # для админки  - метод PNL ALL, без группировки
        rows = self.get_rows(sql, as_dict=True)
        if portfolio_id is None:
            positions = rows

        # Иначе - стандартная групппировка
        else:
            for row in rows:
                positions[row['ticker']] = row
        return positions

    def get_raw_trades(self, portfolio_id, client_id=None, status=None):
        # trades = defaultdict(list)
        tr = Trades
        t = Tickers
        if status:
            sql = select([tr]) \
                .where(tr.client_id == client_id) \
                .where(tr.portfolio_id == portfolio_id) \
                .where(tr.status == 0) \
                .order_by(Trades.open_date)
        elif portfolio_id is None:
            sql = select([tr]) \
                .where(tr.client_id == client_id) \
                .where(tr.status == 0) \
                .order_by(tr.open_date)
        else:
            j = join(tr, t, tr.ticker == t.ticker)
            column_list = [tr.id, tr.client_id, tr.portfolio_id, tr.ticker, tr.direction,
                           tr.open_vol, tr.price, tr.vol_prc, tr.quantity, tr.remain_quantity,
                           tr.shares_in_lot, tr.market_price, tr.value, tr.accruedint, tr.open_date, tr.close_date,
                           tr.profit, tr.status, tr.margin_long, tr.margin_short,
                           tr.commission, tr.trade_num, t.title, t.currency, t.type]
            sql = select(column_list).select_from(j) \
                .where(tr.client_id == client_id) \
                .where(tr.portfolio_id == portfolio_id) \
                .order_by(tr.trade_date_time.desc())

        return self.get_rows(sql, as_dict=True)

    def get_trades(self, portfolio_id, client_id=None, status=None, for_stat=False):
        trades = defaultdict(list)
        rows = self.get_raw_trades(portfolio_id, client_id, status)
        if for_stat:
            trades = rows
        else:
            for row in rows:
                trades[row['ticker']].append(row)
        return trades

    def get_or_create_client(self, login):
        # Находим в БД клиента
        c = Clients
        p = ClientsProfiles
        j = outerjoin(c, p, c.id == p.client_id)
        # col_list = [c for c in Clients.c]
        # col_list.extend([p.profile_name, p.filling_datetime])

        sql = select([c, p.profile_name, p.filling_datetime]).select_from(j). \
            where(c.login == login). \
            where(or_(p.isActual.is_(True), p.isActual.is_(None)))

        client = self.get_row(sql)

        return client.id, client

    def get_positions_by_client_id(self, client_id, sec_code):
        p = ClientsPositions
        t = Tickers
        j = join(p, t, p.ticker == t.ticker)
        columns_list = [p.id, p.client_id, p.portfolio_id, p.status, p.ticker, p.position_type,
                        p.nom_part, p.open_date, p.calculate_profit, p.fixed_profit,
                        p.cost, p.real_part_cost, p.avg_open_price, p.shares_in_lot,
                        p.price, p.rur_price, p.tradedate, p.div_size,
                        p.acc_vol, p.vol, p.max_part,
                        p.operation, p.ticker_currency, p.recommend_part,
                        p.is_signal_active, p.signal_size, p.last_signal_time, t.code, t.title]
        sql = select(columns_list).select_from(j). \
            where(p.client_id == client_id). \
            where(or_(t.secur_code == sec_code, t.ticker == sec_code))
        return self.get_row(sql)

    def get_expected_dividends_for_api(self, portfolio_id, client_id):

        t = Tickers
        calendar = Dividends
        pt = ClientsPositions

        current_date = datetime.today()
        qs = (self.session.query(calendar.id,
                                 calendar.div_size.label("div_on_share"),
                                 (calendar.div_size * pt.nom_part * pt.shares_in_lot).label('div_total'),
                                 calendar.div_date,
                                 t.title,
                                 pt.ticker,
                                 t.code,
                                 t.currency)
              .filter(t.ticker == calendar.ticker)
              .filter(t.ticker == pt.ticker)
              .filter(pt.portfolio_id == portfolio_id)
              .filter(pt.client_id == client_id)
              .filter(pt.nom_part > 0)
              .filter(calendar.div_date >= current_date).order_by(calendar.id)
              ).all()

        return get_query_rows(qs)

    def get_received_dividends_for_api(self, portfolio_id, client_id):
        t = Tickers
        div = Dividends
        qs = (self.session.query(div.id,
                                 div.div_size.label("div_total"),
                                 div.div_date, t.title,
                                 t.ticker, t.code,
                                 t.currency)
              .filter(t.ticker == div.ticker)
              .filter(div.portfolio_id == portfolio_id)
              .filter(div.client_id == client_id)
              .order_by(div.id)
              ).all()

        return get_query_rows(qs)

    def check_trade(self, trade_num):
        """ Првоеряем что ранне не обрабатывали сделку """
        t = Trades
        exist_trades_count = self.session.query(t).filter(t.trade_num == trade_num).count()
        is_trade_new = True if exist_trades_count < 1 else False
        return is_trade_new

    def delete_portfolio(self, client_id, portfolio_id):
        self.session.query(ClientsPortfolios) \
            .filter(ClientsPortfolios.portfolio_id == portfolio_id) \
            .filter(ClientsPortfolios.client_id == client_id) \
            .delete()

    def check_current_portfolio(self, client_id):
        """ Список подключенных портфелей """
        ref_list = [row
                    for row in
                    self.session.query(ClientsPortfolios)
                        .filter(ClientsPortfolios.client_id == client_id)
                        .filter(ClientsPortfolios.portfolio_id != 1)
                    ]
        id_list = [p.portfolio_id for p in ref_list]
        cur_list = [p.currency for p in ref_list]
        return id_list, cur_list

    def check_trade_allowed(self, client_id):
        """ Првоеряем что ранне не обрабатывали сделку """
        t = Clients
        client = self.session.query(t).filter(t.id == client_id).first()
        return client.trading_allowed

    def get_client_by_code(self, client_code):
        # Находим в БД клиента
        sql = select([Clients]).where(Clients.client_code == client_code)
        return self.get_row(sql)

    def get_all_clients(self):
        # Находим в БД клиента
        sql = select([Clients])
        return self.get_rows(sql)

    def get_current_state(self, portfolio_id, client_id):
        # Находим в БД состояние портфеля
        current_time = datetime.now()
        a = ClientsPortfolios
        b = TariffRules

        j = join(a, b, a.tariff_id == b.tariff_id)

        sql = (select([a, b.commission])
               .select_from(j)
               .where(a.portfolio_id == portfolio_id)
               .where(a.client_id == client_id)
               .where(a.currency == b.currency)
               .where(b.date_from <= current_time)
               .where(b.date_to > current_time))

        return self.get_row(sql)

    def get_agreements(self, agg_id):
        return get_query_rows(self.session.query(Agreements.id,
                                                 Agreements.agreement_num,
                                                 TradeCodes.value)
                              .filter(Agreements.id == agg_id,
                                      TradeCodes.agreement_id == Agreements.id
                                      ).all())

    def get_agreements_data(self, login):
        return get_query_rows(self.session.query(Clients.login,
                                                 Clients.client_code,
                                                 Agreements.id,
                                                 Agreements.agreement_num,
                                                 TradeCodes.value,
                                                 MoneyLimits.current_bal.label('balance'),
                                                 MoneyLimits.curr_code.label('currency'))
                              .filter(Clients.login == login,
                                      Clients.id == Agreements.client_id,
                                      Agreements.id == TradeCodes.agreement_id,
                                      MoneyLimits.client_code == TradeCodes.value))

    def get_client_stat(self, client_id, client_code, login):
        return {
            'portfolios': model_qs_as_dict(self.session.query(ClientsPortfolios).
                                           filter(ClientsPortfolios.client_id == client_id).all()),
            'positions': model_qs_as_dict(self.session.query(ClientsPositions).
                                          filter(ClientsPositions.client_id == client_id).all()),
            'bids': model_qs_as_dict(self.session.query(ClientsBids).
                                     filter(ClientsBids.client_id == client_id).all()),
            'trades': model_qs_as_dict(self.session.query(Trades).
                                       filter(Trades.client_id == client_id).all()),
            'money_limits': model_qs_as_dict(self.session.query(MoneyLimits).
                                             filter(MoneyLimits.client_code == client_code).all()),
            'depo_limits': model_qs_as_dict(self.session.query(DepoLimits).
                                            filter(DepoLimits.client_code == client_code).all()),
            'aggreements': self.get_agreements_data(login),

        }

    def get_summary_stat(self):
        return {
            'clients': self.session.query(func.count(Clients.id)).first(),
            'portfolios': self.session.query(func.count(ClientsPortfolios.id)).first(),
            'portfolios_sum': self.session.query(func.sum(ClientsPortfolios.start_asset)).first(),
            'bids': (self.session.query(func.count(ClientsBids.id), ClientsBids.send_status)
                     .group_by(ClientsBids.send_status)).all(),
            'trades': self.session.query(func.count(ClientsBids.id)).first(),
        }

    def set_current_state(self, client_portfolio):
        # сохраняем в БД состояние портфеля
        current_portfolio = (self.session.query(ClientsPortfolios)
                             .filter(ClientsPortfolios.portfolio_id == client_portfolio['portfolio_id'])
                             .filter(ClientsPortfolios.client_id == client_portfolio['client_id'])
                             ).first()

        if current_portfolio:
            update_by_dict(current_portfolio, client_portfolio)
        else:
            self.session.add(ClientsPortfolios(**client_portfolio))

    def get_depo_limit(self, client_id):
        row_num_column = func.row_number().over(partition_by=(DepoLimits.client_code, DepoLimits.sec_code),
                                                order_by=desc(DepoLimits.create_date)).label('row_num')

        query = (self.session
                 .query(DepoLimits, Clients)
                 .filter(Clients.client_code == DepoLimits.client_code)
                 .filter(Clients.id == client_id)).add_column(row_num_column)

        rows = query.from_self().filter(row_num_column == 1).all()

        res = {}
        for row in rows:
            res[row[0].sec_code] = row[0].current_bal
        return res

    def get_client_trade(self, client_code, sec_code):
        t = QuikTrades
        res = self.session.query(t) \
            .filter(t.client_code == client_code) \
            .filter(t.sec_code == sec_code) \
            .first()
        tpl = res._asdict()
        return tpl

    def get_bid(self, **kwargs):
        bid_num = kwargs.get('num')
        ticker = kwargs.get('ticker')
        client_id = kwargs.get('client_id')
        portfolio_id = kwargs.get('portfolio_id')

        if bid_num:
            sql = select([ClientsBids]) \
                .where(ClientsBids.bucket_num == bid_num) \
                .where(ClientsBids.send_status == BidRecord.NEW)

        elif ticker:
            sql = select([ClientsBids]) \
                .where(ClientsBids.client_id == client_id) \
                .where(ClientsBids.portfolio_id == portfolio_id) \
                .where(ClientsBids.send_status == BidRecord.NEW) \
                .where(ClientsBids.ticker == ticker)
            return self.get_row(sql)

        else:
            sql = select([ClientsBids]) \
                .where(ClientsBids.client_id == client_id) \
                .where(ClientsBids.portfolio_id == portfolio_id) \
                .where(ClientsBids.send_status == BidRecord.NEW)

        return self.get_rows(sql, as_dict=True)

    def get_last_bids(self, **kwargs):
        """ Страница пакетной заявки: GET /trades/"""
        column_list = [
            ClientsBids.id,
            ClientsBids.bucket_num,
            ClientsBids.create_date,
            ClientsBids.portfolio_id,
            ClientsBids.client_id,
            ClientsBids.ticker,
            ClientsBids.operation,
            ClientsBids.nominal,
            ClientsBids.shares_in_lot,
            ClientsBids.client_nominal,
            Tickers.title,
            Tickers.classcode,
            (Tickers.shares_in_lot * ClientsBids.nominal).label('shares_count'),
            Tickers.code,
            Tickers.isin,
            Tickers.currency,
            Tickers.type,
            Tickers.short_description,
            Tickers.price_deviation,
            Tickers.min_step]

        conditions = [
            ClientsBids.client_id == kwargs.get('client_id'),
            ClientsBids.portfolio_id == kwargs.get('portfolio_id'),
            ClientsBids.send_status == BidRecord.NEW,
            Tickers.ticker == ClientsBids.ticker
        ]

        if kwargs.get('without_balance'):
            column_list.append(literal_column('0', type_=Integer).label('current_balance'))
        else:
            column_list.append(ClientsPositions.nom_part.label('current_balance'))
            conditions.extend([
                ClientsBids.client_id == ClientsPositions.client_id,
                ClientsBids.portfolio_id == ClientsPositions.portfolio_id,
                ClientsBids.ticker == ClientsPositions.ticker,
            ])
        return get_query_rows(self.session.query(*column_list).filter(*conditions))

    def get_bids_for_send(self, **kwargs):
        """ При отпавки поручения POST /trades/"""
        res = {}
        t = Tickers
        c = Clients
        b = ClientsBids
        qs = (self.session.query(
            b.ticker,
            b.id,
            b.bucket_num,
            b.operation,
            b.nominal,
            t.shares_in_lot,
            t.currency,
            t.type,
            t.min_step,
            t.scale,
            t.classcode,
            c.client_code,
            c.id.label('shares_count')
        )
              .filter(t.ticker == b.ticker)
              .filter(b.client_id == c.id)
              .filter(b.bucket_num == kwargs.get('bid_num'))
              .filter(b.send_status == BidRecord.NEW)
              ).all()

        for row in get_query_rows(qs):
            res[row['ticker']] = row

        return res

    def get_bid_by_id(self, bid_id):
        return self.session.query(ClientsBids).get(bid_id)

    def define_accounts_from_depo(self, **kwargs):
        res = {}
        row_num_col = func.row_number().over(partition_by=(DepoLimits.client_code, DepoLimits.sec_code),
                                             order_by=desc(DepoLimits.create_date)).label('row_num')

        qs = ((self.session
               .query(DepoLimits.sec_code, DepoLimits.trd_acc, DepoLimits.current_bal, DepoLimits.client_code)
               .add_columns(row_num_col)
               .filter(Clients.client_code == DepoLimits.client_code)
               .filter(Clients.id == kwargs.get('client_id'))
               .filter(DepoLimits.limit_kind == Limits.T2))
              .from_self()
              .filter(row_num_col == 1)).all()

        for row in qs:
            res[row.sec_code] = {
                "trd_acc": row.trd_acc,
                "current_bal": row.current_bal,
                "client_code": row.client_code,
            }

        return res

    def get_quik_limits(self, **kwargs):
        return self.qs.get_quik_limits(**kwargs)

    def get_money_limits(self, client_code: str):
        return model_qs_as_dict(self.session.query(MoneyLimits).filter(MoneyLimits.client_code == client_code))

    def get_client_risk_profile(self, client_id):
        t = Clients
        p = RiskProfiles
        j = outerjoin(t, p, t.risk_profile == p.profile_id)
        result = self.get_row(select([t.id, p.risk_level]).select_from(j).where(t.id == client_id))

        return result.risk_level if result is not None else 1

    def get_client_profile_details(self, client_id):
        t = Clients
        p = ClientsProfiles
        j = outerjoin(t, p, t.id == p.client_id)

        return self.get_row(select([t.id, p.filling_datetime, p.profile_name]).select_from(j).where(t.id == client_id))

    def update_clients_portfolios(self, condition, values):
        self.update_record_by_filter(ClientsPortfolios, condition, values)

    def update_client(self, condition, values):
        self.update_record_by_filter(Clients, condition, values)

    def update_bid_template(self, template_id, **values):
        self.update_record_by_filter(
            BidTriggers,
            {
                "id": template_id,
            },
            values,
        )

    def update_bids_by_api(self, bid_id, **values):
        self.update_record_by_filter(
            ClientsBids,
            {
                "id": bid_id,
            },
            values,
        )

    def update_bids_by_reporter(self, bid_id, **values):
        self.qs.update_bids_by_report(bid_id, **values)

    def save_trade(self, result_trade):
        self.session.add(Trades(**result_trade))

    def save_deposit_trade(self, deposit):
        self.session.add(Deposits(**deposit.__dict__))

    def save_trade_bulk(self, trades):
        ignore_fields = ('i', 'pnl_type')
        res = []
        res.extend([v for k, v in trades.items()])
        x = list(itertools.chain.from_iterable(res))
        result_trade_d = list(map(lambda x: {k: v for k, v in x.__dict__.items() if k not in ignore_fields}, x))
        self.session.execute(Trades.insert(), result_trade_d)

    def save_portfolio_results(self, report=None, report_list=None):
        t = PortfolioReports
        if report_list:
            self.session.execute(PortfolioReports.insert(), report_list)
        else:
            current_report = (self.session.query(t)
                              .filter(t.portfolio_id == report['portfolio_id'])
                              .filter(t.client_id == report['client_id'])
                              .filter(t.tradedate == report['tradedate'])).first()

            if current_report:
                update_by_dict(current_report, report)
            else:
                self.session.add(PortfolioReports(**report))

    def update_positions(self, positions):

        for ticker, position in positions.items():
            session_obj = self.session.query(ClientsPositions).get(position.id)
            update_by_dict(session_obj, position.__dict__)

    def update_position_fields(self, entity_filter, values):
        self.update_record_by_filter(ClientsPositions, entity_filter, values)

    def update_record_by_filter(self, model, entity_filter, values):
        self.session.query(model).filter_by(**entity_filter).update(values)

    def update_limit_from_exporter(self, client_id, curr_code, current_bal, event_time):
        """
        Обновляем исполненный объем по заявке
        """
        self.update_record_by_filter(
            ClientsPositions,
            {
                "client_id": client_id,
                "ticker": curr_code,
            },
            {
                "real_part_cost": current_bal,
                "tradedate": event_time,
            }
        )

    # TODO: should it be in this project ?
    def create_signals_templates(self, data):
        data['create_by'] = 'api'
        sql = SignalsTemplates.insert().returning(SignalsTemplates.c.id).values(**data)
        rowid = self.session.execute(sql).lastrowid
        return {
            "id": rowid,
            "status": "ok"
        }

    def get_signals_template(self, id):
        s = self.session.query(SignalsTemplates).get(id)
        result = {
            'error': s.status,
            'id': s.id,
            'ticker': s.ticker,
            'operation': s.operation,
            'size': s.change_position,
            'price': str(s.price)
        }
        return result

    # TODO: move to BackTest Project
    def get_pnl_track(self, portfolio_id, client_id=None, days_ago=None):
        current_time = datetime.utcnow()
        if days_ago:
            tradedate = current_time - timedelta(days=int(days_ago))
            tradedate = tradedate.replace(hour=0, minute=0, second=0, microsecond=0)
            trade_date = tradedate
        else:
            tradedate = None
            trade_date = datetime(day=1, month=1, year=2017)

        res = self.qs.get_track(portfolio_id=portfolio_id, trade_date=trade_date)

        return res

    def save_bids(self, bids_to_save):
        for bid in bids_to_save:
            self.session.add(ClientsBids(**bid))

    def update_bids(self, condition, values):
        self.update_record_by_filter(ClientsBids,
                                     condition, values)

    def remove_prev_bids(self, portfolio_id, client_id):
        t = ClientsBids
        self.session.query(t) \
            .filter(t.client_id == client_id) \
            .filter(t.portfolio_id == portfolio_id) \
            .filter(t.send_status == BidRecord.NEW) \
            .update({t.send_status: BidRecord.ARCHIVE}, synchronize_session=False)

    def get_portfolio_pages(self, page_id: int) -> dict:
        pp = PortfolioPages
        p = Portfolio
        qs = (self.session.query(
            pp.id,
            pp.portfolio_id,
            pp.landing_page,
            p.name,
            p.owner,
        ).filter(pp.portfolio_id == p.id))

        if page_id:
            qs.get(page_id)
        else:
            qs.all()

        return get_query_rows(qs)

    def create_portfolio_page(self, _values):
        portfolio_page = PortfolioPages(**_values)
        self.session.add(portfolio_page)
        self.session.flush()
        return portfolio_page.id

    def delete_portfolio_page(self, page_id):
        self.session.query(PortfolioPages).filter(PortfolioPages.id == page_id).delete()
