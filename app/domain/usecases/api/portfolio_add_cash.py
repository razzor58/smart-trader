from datetime import datetime

from app.domain.entities.plain import Deposit
from app.domain.constants import CoreError, VIRTUAL_CASH
from app.domain.responses import ErrorResponse
from app.domain.usecases import PortfolioUseCase


class PortfolioAddCashUseCase(PortfolioUseCase):

    def __init__(self, login, portfolio_id, portfolio_dao, payload):
        super().__init__(portfolio_id, login, portfolio_dao)

        self.new_asset_size = int(payload.get('new_asset_size', 0))
        self.deposit = int(payload.get('deposit', 0))
        self.portfolio_id = portfolio_id

    def set_new_asset_size(self, new_asset_size):
        """ Обновляем позицию CASH в состоянии портфеля в БД """

        asset_delta = new_asset_size - self.portfolio.start_asset
        self.portfolio.cash_deposit = asset_delta

        # Обновляем значение кэша в объекте и в БД
        if VIRTUAL_CASH in self.portfolio.positions:
            self.portfolio.positions[VIRTUAL_CASH].cost += asset_delta
            self.portfolio.update_positions_in_db()

        # обновляем поля объекта, чтобы затем сделать переоценку последнего дня.
        self.portfolio.asset = new_asset_size
        self.portfolio.cash += asset_delta
        self.portfolio.start_asset = new_asset_size

    def process_deposit_operation(self):
        """ Workflow изменения активов под управлением """

        # Берем состояние портфеля из БД
        self.portfolio.apply_db_state()

        # Обновляем позицию CASH в состоянии портфеля в БД
        if self.new_asset_size > 0:
            self.set_new_asset_size(self.new_asset_size)

        # Выставляем заявки на основе нового кэша
        if self.new_asset_size == 0 and self.deposit == 0:
            self.portfolio.create_bids(full_exit=True)
            self.portfolio.accordance = 0
            self.portfolio.start_asset = 0
        else:
            self.portfolio.create_bids(is_rebalanced=True)

            # Считаем % соответствия
            self.portfolio.accordance = self.portfolio.calculate_accordance()

        # обновляем рекомендованные доли в позициях
        self.portfolio.update_positions_in_db()

        # Сохраняем сделку в БД для истории пополнений
        self.db.save_deposit_trade(Deposit(portfolio_id=self.portfolio_id,
                                           client_id=self.client_id,
                                           new_asset_size=self.new_asset_size,
                                           deposit=self.deposit))

        # Сохраняем размер депозита в вектор доходности(для исключения "ступеньки" на графике)
        self.portfolio.save_portfolio_report(report={
            'tradedate': datetime.now().strftime('%Y-%m-%d'),
            'asset': self.portfolio.asset,
            'portfolio_id': self.portfolio.portfolio_id,
            'client_id': self.portfolio.client_id,
            'shares': self.portfolio.shares})

        # Меняем состояние портфеля в БД
        self.portfolio.save_db_state()

        # Меняем состояние позиций в БД
        self.portfolio.update_positions_in_db()

    def execute(self):

        # Проверка соответствия риск-профилю клиента
        if self.portfolio.risk_is_not_compatible():
            return ErrorResponse(error=CoreError.RISK_PROFILE_INCOMPATIBLE)

        # Суммы не могут быть отрицательными
        if self.new_asset_size >= 0 and self.deposit >= 0:
            self.process_deposit_operation()
