from app.domain.constants import STOCK_NAMES, Operation, PositionTypes, DEFAULT_PRICE_STEP
import time
from datetime import timedelta
from uuid import uuid4


def get_obj_to_save(instance, model):
    return dict(
        [(k, v) for k, v in instance.__dict__.items() if k in model.__annotations__ and k != 'id']
    )


def get_stock_name(code):
    if code in STOCK_NAMES:
        res = STOCK_NAMES[code]
    else:
        res = STOCK_NAMES['TQBR']
    return res


def calc_limit_price(order_detail, bid, side):
    """

    """
    last_price = order_detail['price']
    dev_prc = order_detail['dev_prc']
    min_step = bid.get('min_step') or DEFAULT_PRICE_STEP
    if min_step == 0.0:
        min_step = DEFAULT_PRICE_STEP

    scale = int(bid['scale'])

    # if buy
    if side == '1':
        bid_price = last_price + min_step * int(last_price * dev_prc / min_step)
    else:
        bid_price = last_price - min_step * int(last_price * dev_prc / min_step)

    return round(bid_price, scale)


def calc_bond_real_cost(raw_price, current_face_value, accrued_interest):
    if (accrued_interest is None
            or current_face_value is None
            or accrued_interest < 0
            or current_face_value < 0):

        bond_cost = 0
    else:
        bond_cost = float(raw_price) * float(current_face_value) / 100 + float(accrued_interest)

    return round(bond_cost, 2)


def define_exchange_destination(class_code, account):
    if class_code in ('NYSE_BEST', 'NASDAQ_BEST') and account == 'SP01-CL00000':
        res = 'SPBXM'
    else:
        res = class_code

    return res


def create_from_dict(obj, _dict):
    for key, value in _dict.items():
        setattr(obj, key, value)


def get_chart_data(stat, round_digit=4):
    start_asset = stat[0]['asset'] - stat[0]['cash_deposit']
    if start_asset == 0:
        start_asset = 1

    res = []
    for row in stat:
        dt1 = round(time.mktime((row['tradedate'] + timedelta(hours=4)).timetuple())) * 1000
        curr_asset = row['asset'] - row['cash_deposit']
        if curr_asset == 0:
            value = 0
        else:
            value = round((curr_asset / start_asset - 1) * 100, round_digit)
        res.append([dt1, value])
    return res


def define_bid_operation(pt: PositionTypes, trade_size):
    if pt in (PositionTypes.WAIT, PositionTypes.LONG) and trade_size > 0:
        operation = Operation(PositionTypes.LONG).OPEN

    elif pt in (PositionTypes.WAIT, PositionTypes.SHORT) and trade_size < 0:
        operation = Operation(PositionTypes.SHORT).OPEN

    elif pt == PositionTypes.LONG and trade_size < 0:
        operation = Operation(PositionTypes.LONG).CLOSE
    elif pt == PositionTypes.SHORT and trade_size > 0:
        operation = Operation(PositionTypes.SHORT).CLOSE

    else:
        operation = Operation(PositionTypes.LONG).OPEN

    return operation


def get_position_by_balance(balance):
    if balance > 0:
        return PositionTypes.LONG.value
    elif balance < 0:
        return PositionTypes.SHORT.value
    else:
        return PositionTypes.WAIT.value


def math_available(values):
    for val in values:
        if not isinstance(val, (int, float)):
            return False
    return True


def get_bucket_num():
    return str(uuid4()).lower()
