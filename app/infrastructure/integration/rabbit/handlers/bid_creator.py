import json
from app.config import rabbit_logger as log
from app.infrastructure.db.domain_dao import SessionContext
from app.domain.usecases.cli.create_bids_daily import CreateBidsForAllUseCase


class Worker(object):

    def __init__(self):
        # Обязательный параметр
        self.routing_key = 'rais.clients.clients_bids'

    @staticmethod
    def process(body=None):
        try:
            data = json.loads(body.decode('utf-8'))
            portfolio_id = data['portfolio_id']
            template_id = data['template_id']

            log.info(f"get create clients_bids for portfolio_id={portfolio_id}")

            # Create bids for all clients
            db = SessionContext()
            use_case = CreateBidsForAllUseCase(portfolio_dao=db)
            r = use_case.execute(portfolio_id, template_id)

            res = f"Success: {r.data.success_counter} clients. errors: {r.data.error_counter} time: {r.data.exec_time}"
            log.info(res)

        except Exception as e:
            log.error(e, exc_info=True)
