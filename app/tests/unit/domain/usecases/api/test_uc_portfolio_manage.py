import pytest
from unittest.mock import MagicMixin
from datetime import datetime
from app.domain.usecases.api.portfolio_manage import PortfolioManageUseCase
from app.domain.constants import TickerTypes, Currencies
from app.domain.entities.plain import Entity
from app.domain.entities.plain import Portfolio


class LandingRecord(Entity):
    DEFAULT_RECORD = {
        'id': '1',
        'name': 'p1',
        'ticker': '1',
        'title': '1',
        'short_title': '1',
        'issuer': '1',
        'industry': '1',
        'full_description': '1',
        'short_description': '1',
        'target_price': 200,
        'invest_period': '1',
        'invest_period_step': '1',
        'invest_period_end_date': '1',
        'has_offer': '1',
        'offer_date': datetime.now(),
        'ytm': '1',
        'secur_code': '1',
        'classcode': '1',
        'ticker_type': TickerTypes.BONDS,
        'currency': '1',
        'isin': '1',
        'close': 100,
        'shares_in_lot': 1,
        'video': 'https://youtu.be/b2U8sG6ikLQ?t=8'
    }

    def __init__(self, ticker_type, target, current, isin='1', shares_in_lot=1, currency=Currencies.RUR.value):
        super().__init__(self.DEFAULT_RECORD)
        self.ticker_type = ticker_type
        self.target_price = target
        self.close = current
        self.currency = currency
        self.isin = isin
        self.shares_in_lot = shares_in_lot


class DateOption(Entity):
    DEFAULT_RECORD = {
        'isin': '1',
        'currentFacevalue': 1000,
        'accruedInterest': 4.05,
    }

    def __init__(self, isin):
        super().__init__(self.DEFAULT_RECORD)
        self.isin = isin


class TestPortfolioHomePageUseCase:
    @pytest.fixture(autouse=True)
    def _get_context(self, db_mock, portfolio_id, test_ticker):
        self._db = db_mock
        self._portfolio_id = portfolio_id
        self._test_ticker = test_ticker
        self._use_case = PortfolioManageUseCase(portfolio_dao=db_mock)

    @pytest.mark.parametrize('api_version', [None, 'v1'])
    def test_portfolio_details_is_valid(self, api_version):
        self._db.get_portfolios_tickers_with_prices.return_value = [
            LandingRecord(TickerTypes.SHARES, 100, 200),
            LandingRecord(TickerTypes.BONDS, 0, 100),
            LandingRecord(TickerTypes.SHARES, 200, 100),
            LandingRecord(TickerTypes.SHARES, 200, 100, currency=Currencies.USD.value),
        ]
        data = self._use_case.get_details(api_version)

        assert isinstance(data, list)

    @pytest.mark.parametrize('api_version', [None, 'v1'])
    def test_portfolio_details_costs(self, api_version):
        self._db.get_portfolios_tickers_with_prices.return_value = [
            LandingRecord(TickerTypes.SHARES, 100, 200, '1', 1),
            LandingRecord(TickerTypes.BONDS, 0, 100, '2', 1),
            LandingRecord(TickerTypes.SHARES, 200, 100, '3', 2),
            LandingRecord(TickerTypes.SHARES, 200, 100, '4', 4, currency=Currencies.USD.value),
        ]
        self._db.get_date_options.return_value = [
            DateOption('2'),
            DateOption('3'),
        ]
        data = self._use_case.get_details(api_version)

        assert isinstance(data, list)
        assert len(data) == 1

        if api_version:
            count_list = []
            costs_list = []
            acc_list = []

            for pos in data[0].get('structure'):
                count_list.append(pos.get('count_in_lot'))
                costs_list.append(pos.get('costs'))
                acc_list.append(pos.get('acc_int'))
            assert count_list == [1, 2, 4, 1]
            assert costs_list == [1004.05, 200, 400, 200]
            assert acc_list == [4.05, None, None, None]

    def test_get_portfolio(self):
        """Описание портфелей"""
        data = self._use_case.get_portfolio(self._portfolio_id)
        assert isinstance(data, Portfolio)

    @pytest.mark.parametrize('payload', [{'name': 'test'}, {'name': 'test1', 'landing_page': 'top5'}])
    def test_create_portfolio(self, payload):
        """Описание портфелей"""
        data = self._use_case.create_portfolio(payload)
        assert isinstance(data, MagicMixin)

    def test_get_portfolio_tickers(self):
        data = self._use_case.get_portfolio_tickers(self._portfolio_id)
        assert isinstance(data, dict)

    def test_get_tickers_meta_data(self):
        data = self._use_case.get_tickers_meta_data(self._portfolio_id)
        assert isinstance(data, dict)

    def test_get_portfolios_meta_data(self):
        data = self._use_case.get_portfolios_meta_data()
        assert isinstance(data, dict)

    def test_update_portfolio(self):
        data = self._use_case.update_portfolio(self._portfolio_id, {})
        assert isinstance(data, MagicMixin)

    def test_save_tickers(self):
        data = self._use_case.save_tickers({})
        assert data is None

    def test_update_tickers(self):
        data = self._use_case.update_tickers(self._portfolio_id, self._test_ticker, {})
        assert isinstance(data, MagicMixin)

    def test_delete_tickers(self):
        data = self._use_case.delete_tickers(self._portfolio_id, self._test_ticker)
        assert isinstance(data, MagicMixin)
