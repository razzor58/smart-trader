from sqlalchemy import join
from datetime import datetime

from app.domain.constants import Currencies, TickerTypes
from app.infrastructure.db.meta.engine import CommonSession
from app.infrastructure.db.meta.tools import (get_query_rows, delete_by_filter, update_record_by_filter,
                                              filter_values, row2dict)
from app.infrastructure.db.meta.models import (Clients, ClientsPortfolios, Portfolio, WorkDays, LastQuotes,
                                               DateOptions, Tickers, PortfolioTickers,
                                               ClientsProfiles, Agreements, TradeCodes, Signals, SignalsHistory)


class SessionContext(CommonSession):

    def __init__(self):
        super().__init__()

    def get_client_count(self):
        return self.session.query(Clients).count()

    def get_clients_slice(self, offset, limit):
        return self.session.query(Clients.login).offset(offset).limit(limit).all()

    def create_client_if_not_exists(self, data):
        client = self.session.query(Clients).filter(Clients.login == data.get('login')).first()
        if not client:
            self.session.add(Clients(**data))
            return True
        else:
            return False

    def create_or_update_clients_data_if_not_exists(self, data):
        """  Сохраняет ответ по формату Proxy Catalog  """
        created = False
        # Ищем клиента
        client = self.session.query(Clients).filter(Clients.login == data.get('klogin')).first()

        # Если не найден - создаем
        if not client:
            client = Clients(login=data['klogin'],
                             master_id=data['clientId'],
                             fullname=" ".join([data['lastName'], data['firstName'], data['middleName']]))
            self.session.add(client)
            created = True

        # Проверяем все соглашения
        for agg in data['agreements']:
            agg_row = self.session.query(Agreements).filter(Agreements.agreement_id == agg['agreementId']).first()

            # Если не найдено - создаем
            input_trade_codes = agg['tradeCodes']
            if not agg_row:
                new_agg = Agreements(agreement_id=agg['agreementId'],
                                     agreement_num=agg['agreementNum'],
                                     is_overnight_enabled=agg['isOvernightEnabled'],
                                     tariff_id=agg['tariffId'],
                                     tariff_name=agg['tariffName'],
                                     update_date=agg['updateDate'])
                new_agg.trade_codes.extend([TradeCodes(value=code) for code in input_trade_codes])
                client.agreements.append(new_agg)
                self.session.add(new_agg)
            else:
                # Если согласшение уже есть - проверяем нужно ли добавить номера счетов
                # например если клиент подключил себе какие-то дополнительные площадки
                existing_trade_codes = [code.value for code in agg_row.trade_codes.all()]
                for new_code in input_trade_codes:
                    if new_code not in existing_trade_codes:
                        agg_row.trade_codes.append(TradeCodes(value=new_code))
                        self.session.add(agg_row)
        return created

    def delete_last_quotes(self, filter_data):
        delete_by_filter(self.session, LastQuotes, filter_data)

    def add_last_quotes(self, quotes):
        self.session.add_all([LastQuotes(**quote) for quote in quotes])

    def delete_profiles(self, filter_data):
        delete_by_filter(self.session, ClientsProfiles, filter_data)

    def add_profile(self, profile):
        self.session.add(ClientsProfiles(**profile))

    def get_portfolio_clients(self, portfolio_id, login):
        p = ClientsPortfolios
        c = Clients
        j = join(c, p, p.client_id == c.id)
        qs = (self.session.query(c.id).select_from(j)
              .filter(p.portfolio_id == portfolio_id)
              .filter(c.login == login)).all()

        return get_query_rows(qs)

    def get_available_strategies(self):
        qs = self.session.query(Portfolio).filter(Portfolio.risk_level.isnot(None))
        return get_query_rows(qs)

    def get_ticker_list(self, ticker_type=None):
        qs = (self.session
              .query(Tickers.isin, Tickers.trade_mode, Tickers.secur_code, Tickers.classcode, Tickers.ticker)
              .distinct(Tickers.isin, Tickers.trade_mode)
              .filter(Tickers.ticker == PortfolioTickers.ticker))

        if ticker_type:
            return get_query_rows(qs
                                  .filter(Tickers.type == ticker_type)
                                  .all())

        else:
            return get_query_rows(qs.all())

    def get_clients(self):
        return get_query_rows(self.session.query(Clients.id, Clients.master_id)
                              .filter(Clients.master_id.isnot(None))
                              .filter(Clients.risk_profile.is_(None))
                              .all()
                              )

    def get_ticker_list_for_ytm(self):
        t = Tickers
        pt = PortfolioTickers
        lq = LastQuotes
        qs = (self.session
              .query(t.isin, t.trade_mode, lq.close)
              .distinct(t.isin, t.trade_mode)
              .filter(t.ticker == pt.ticker)
              .filter(t.secur_code == lq.securcode)
              .filter(t.classcode == lq.classcode)
              .filter(t.type == TickerTypes.BONDS)
              .filter(pt.valid_to >= datetime.today())
              .filter(pt.valid_from <= datetime.today())
              )
        res = get_query_rows(qs)
        return res

    def get_work_day(self):
        rows = (self.session.query(WorkDays.dates, WorkDays.next_date)
                .filter(WorkDays.currency == Currencies.RUR.value)
                .filter(WorkDays.dates >= datetime.today())
                .order_by(WorkDays.dates.asc())
                .first())
        # r = get_query_rows(rows)
        return rows

    def update_calendar(self, dates_list, currency, data):
        self.session.query(WorkDays) \
            .filter(WorkDays.dates.in_(dates_list)) \
            .filter(WorkDays.currency == currency) \
            .delete(synchronize_session=False)
        work_days = [WorkDays(**row) for row in data]
        self.session.add_all(work_days)

    def update_tickers_data(self, filter_data, values):
        update_record_by_filter(
            self.session,
            Tickers,
            filter_data,
            values
        )

    def update_client_risk_profile(self, client_id, risk_profile):
        update_record_by_filter(self.session, Clients, {'client_id': client_id}, {'risk_profile': risk_profile})

    def save_date_option(self, data):
        self.session.add(DateOptions(**data))

    def update_signals(self, item):
        data = filter_values(Signals, item, ['Signal'])
        qs = self.session.query(Signals).filter(
            Signals.short_code == data['short_code']
        )
        current_signal = qs.first()

        if not current_signal:
            self.session.add(Signals(**data))
            return 1
        # Если сигнал свежее - текущий скидываем в историю и добавляем новый
        elif current_signal and data['epoch_time'] > current_signal.epoch_time:
            self.session.add(SignalsHistory(**row2dict(current_signal, no_pk=True, as_str=False)))
            qs.delete()
            self.session.add(Signals(**data))
            return 1
        else:
            return 0
