import logging
import sys
from datetime import datetime
from db.portfolio_dao import PortfolioDAO
from rest.main.controller import service_controller


logger = logging.getLogger()
fh1 = logging.StreamHandler(sys.stdout)
fh1.setLevel(logging.DEBUG)
fh1.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger.addHandler(fh1)

db = PortfolioDAO()

if __name__ == '__main__':
    print("==== Start process === ")
    start_time = datetime.now()
    # Все портфели портфели
    all_clients = db.get_all_clients()
    ok_cnt = 0
    err_cnt = 0
    skip_cnt = 0
    for client in all_clients:
        try:
            login = client['login']
            if login is not None:
                print("import client {0}".format(login))
                service_controller.import_client(login=login)
                ok_cnt += 1
            else:
                skip_cnt += 1
        except Exception as e:
            print(e)
            err_cnt += 1

    exec_time = datetime.now() - start_time
    print("====  End process. Updated: {0} Failed: {1}, Skiped: {3} exec_time: {2} ====".format(
        ok_cnt, err_cnt, exec_time, skip_cnt)
    )
