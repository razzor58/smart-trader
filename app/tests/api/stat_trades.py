import unittest
import requests as r
from . import PORTFOLIO_ID, SERVICE_URL


class TestStatTrades(unittest.TestCase):
    """    Проверка вкладки история сделок   """
    resp = None
    TAB_NAME = 'trade_history'

    def setUp(self):
        method = '/stat/{portfolio_id}/{tab_name}'.format(portfolio_id=PORTFOLIO_ID, tab_name=TestStatTrades.TAB_NAME)
        url = SERVICE_URL + method
        self.resp = r.post(url).json()

    def test_trades_resp_is_valid(self):
        test_result = isinstance(self.resp, dict)
        self.assertTrue(test_result)

    def test_trades_resp_is_ok(self):
        try:
            self.assertTrue(self.resp['success'])
        except Exception:
            print(self.resp)
            raise


if __name__ == '__main__':
    unittest.main()
