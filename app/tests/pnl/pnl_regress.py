import json
import os
from datetime import datetime
import trades_test as tt
from rest.main.service.portfolio_service import PortfolioService

# начало работы
start_time = datetime.now()
portfolios = {
    3: {
        "income": 25.33,
        "name": "US1",
        "commission_size": 10.78,
        "total_divs": 35.39,
    },
    4: {
        "income": 22.29,
        "name": "US2",
        "commission_size": 27.25,
        "total_divs": 114.1,
    },
    6: {
        "income": 64.97,
        "name": "US3",
        "commission_size": 56.13,
        "total_divs": 99.21,
    },
    25: {
        "income": 64.50,
        "name": "RU1",
        "commission_size": 819.76,
        "total_divs": 7367.52,
    },
    26: {
        "income": 48.47,
        "name": "RU2",
        "commission_size": 1386.08,
        "total_divs": 18511.2,
    },
    27: {
        "income": 43.09,
        "name": "RU3",
        "commission_size": 3706.24,
        "total_divs": 43897.38,
    },
}


def main():

    for portfolio_id, v in portfolios.items():
        print('=' * 72)
        print(f'PNL regress for: {portfolios[portfolio_id]["name"]}')
        # Запрос данных из кода
        ps = PortfolioService(portfolio_id=portfolio_id, login=None)
        ps.recalc_pnl(days_ago=None)
        pnl_data = ps.get_pnl()
        resp = json.loads(pnl_data)

        p = resp['portfolio']

        # Проверяем посчитанные значения эталонным данным
        for key, value in v.items():
            if key != "name":
                print('{:60}'.format(key) + 'calc: ' + str(p[key]) + ' etalon: ' + str(value))

        # Выполняем набор тестов  и получаем матрицу результатов
        error_df = tt.get_error_df(resp)
        for row in error_df.iterrows():
            s = row[1]
            s = s[s != 'good']
            if s.size > 0:
                print('{:60}'.format(tt.TEST_DESCRIPTION[s.name]) + 'failed:' + (s.index[0]))

            else:
                print('{:60}'.format(tt.TEST_DESCRIPTION[s.name]) + 'ok')

    # Завершаем работу
    exec_time = datetime.now() - start_time
    print('=' * 72)
    print('END. EXEC_TIME: {}'.format(exec_time))


if int(os.environ.get('STAGE_ENV', 0)) < 1:
    print('ERROR: test for STAGE_ENV only!')
else:
    main()
