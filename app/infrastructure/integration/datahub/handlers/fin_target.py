from datetime import datetime, timedelta
from math import ceil

from app.config import logger
from app.infrastructure.db.domain_dao import SessionContext
from app.domain.constants import Currencies, DATE_FORMAT
from app.infrastructure.tools import http_request

FIN_TARGET_API_URL = 'https://openapi.fintarget.ru/api/portfolio-service/v1/portfolios'


class TickerUpdater:
    def __init__(self, session: SessionContext, portfolio_id: int):
        self.session = session
        self.portfolio_id = portfolio_id
        self.non_updated_codes = []

    def _get_existing_tickers(self):
        for ticker in self.session.get_master_portfolio_tickers(self.portfolio_id):
            self.non_updated_codes.append(ticker.ticker)

    def update_positions(self, positions: list):
        self._get_existing_tickers()

        for ticker in positions:
            ticker_code = ticker.get('securCode')
            if ticker_code is None:
                logger.warning('Position has no securCode')
                continue
            template = self._build_ticker_from_template(
                self.portfolio_id,
                ticker_code,
                ticker.get('weight', 0)
            )
            if ticker_code in self.non_updated_codes:
                self.session.update_portfolio_tickers(self.portfolio_id, ticker_code, template)
                self.non_updated_codes.remove(ticker_code)
                logger.debug(f'Updated ticker {ticker_code} in Portfolio {self.portfolio_id}')
            else:
                self.session.create_portfolio_tickers(template)
                logger.debug(f'Created new ticker {ticker_code} in Portfolio {self.portfolio_id}')

        for old_ticker in self.non_updated_codes:
            logger.debug(f'Remove ticker {old_ticker} from Portfolio {self.portfolio_id}')
            self.session.delete_portfolio_tickers(self.portfolio_id, old_ticker)

    @staticmethod
    def _build_ticker_from_template(portfolio_id: int, secure_code: str, weight: float):
        return {
            'portfolio_id': portfolio_id,
            'ticker': secure_code,
            'hardcode_part': 1,
            'valid_from': datetime.today() - timedelta(days=1),
            'valid_to': datetime.today() + timedelta(days=3650),
            'current_vol': weight,
            'vol_update_time': datetime.now(),
            'show_on_site': 1,
        }


def build_portfolio_from_template(name: str, description: str, recommend_cash: float,
                                  forecast: float, currency: str, uuid: str):
    return {
        'owner': 'FinTarget',
        'name': name,
        'status': 1,
        'description': description,
        'start_calc_date': datetime.today().strftime(DATE_FORMAT),
        'recommended_cash': ceil(recommend_cash),
        'currency_cash': ceil(recommend_cash),
        'currency': Currencies.get_currency(currency),
        'fin_target_uuid': uuid,
        'iir_enabled': 0,
        'income_avg': forecast * 100,
    }


def process():
    response = http_request(FIN_TARGET_API_URL, 'GET')
    with SessionContext() as db_session:
        for ft_portfolio in response.get('result', []):
            if ft_portfolio.get('id') is None:
                continue
            template = build_portfolio_from_template(
                ft_portfolio.get('name'),
                ft_portfolio.get('description'),
                ft_portfolio.get('minBalance'),
                ft_portfolio.get('forecast', 0),
                ft_portfolio.get('currency'),
                ft_portfolio.get('id'),
            )
            stored_portfolio = db_session.find_master_portfolio(ft_portfolio.get('id'))
            if stored_portfolio is None:
                portfolio_id = db_session.create_master_portfolio(template)
                logger.info(f'Created new Portfolio: {portfolio_id}')
                ticker_updater = TickerUpdater(db_session, portfolio_id)
            else:
                db_session.update_master_portfolio(stored_portfolio.id, template)
                logger.debug(f'Updated Portfolio: {stored_portfolio.id}')
                ticker_updater = TickerUpdater(db_session, stored_portfolio.id)
            ticker_updater.update_positions(ft_portfolio.get('positions', []))
