from flask_restplus import Resource
from app.infrastructure.rest.tools import api_response, get_login_from_token, ApiErrors
from app.infrastructure.rest.schemas import ClientDto
from app.infrastructure.db.domain_dao import SessionContext
from app.domain.usecases.api.client_manage import ClientManageUseCase
from app.domain.usecases.api.portfolio_subscribed import PortfolioSubscriptionUseCase

api = ClientDto.api


@api.route('/accounts')
class ClientAccounts(Resource):
    @get_login_from_token
    def get(self, **kwargs):
        with SessionContext() as db:
            login = kwargs.get('k_login')
            use_case = ClientManageUseCase(db_session=db, login=login)
            return api_response(data=use_case.get_agreements_data(login))

    @api.expect(ClientDto.agreement)
    @get_login_from_token
    def post(self, **kwargs):

        agreement_id = api.payload.get('agreement_id')
        if not agreement_id:
            return api_response(error=ApiErrors.MISSING_PARAMETERS)

        with SessionContext() as db:
            login = kwargs.get('k_login')
            use_case = ClientManageUseCase(db_session=db, login=login)
            use_case.set_active_account(agreement_id)

            for portfolio in use_case.get_current_portfolios():
                subs_use_case = PortfolioSubscriptionUseCase(
                    portfolio_id=portfolio['id'],
                    login=login,
                    portfolio_dao=db,
                    payload={
                        'cash': portfolio['start_asset'],
                        'replace': True,
                        'portfolio_id': portfolio['portfolio_id']
                    }
                )
                subs_use_case.execute()

            return api_response()
