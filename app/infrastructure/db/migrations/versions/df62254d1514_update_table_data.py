"""update_table_data

Revision ID: df62254d1514
Revises: 6c7f4545e969
Create Date: 2020-06-04 14:39:21.164666

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'df62254d1514'
down_revision = '6c7f4545e969'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("UPDATE clients SET client_code=186048 where id=6;")
    op.execute("UPDATE clients SET client_code=139722 where id=7;")


def downgrade():
    op.execute("UPDATE clients SET client_code='TEST002' where id=6;")
    op.execute("UPDATE clients SET client_code='TEST001' where id=7;")
