import pytest
from app.domain.usecases.api.portfolio_homepage import PortfolioHomePageUseCase


class TestPortfolioHomePageUseCase:

    @pytest.mark.parametrize('master_portfolio_id', [None])
    def test_use_case_return_all(self, db_mock, master_portfolio_id, test_login):
        db_mock.get_client_home_page.return_value = [
                {'id': 1, "name": "test_strategy_1"},
                {'id': 2, "name": "test_strategy_2"}
            ]
        use_case = PortfolioHomePageUseCase(login=test_login,
                                            portfolio_id=master_portfolio_id,
                                            portfolio_dao=db_mock)
        data = use_case.execute()
        assert isinstance(data, list)

    @pytest.mark.parametrize('master_portfolio_id', [1])
    def test_use_case_return_single(self, db_mock, master_portfolio_id, test_login):
        db_mock.get_client_home_page.return_value = [
            {'id': 2, "name": "test_strategy_2"}
        ]
        use_case = PortfolioHomePageUseCase(login=test_login,
                                            portfolio_id=master_portfolio_id,
                                            portfolio_dao=db_mock)
        data = use_case.execute()
        assert isinstance(data, dict)
