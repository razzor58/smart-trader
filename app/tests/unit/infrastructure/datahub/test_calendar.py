from unittest.mock import patch
from app.infrastructure.integration.datahub.handlers.calendar import process


class TestCalendarJob:

    @patch('app.infrastructure.integration.datahub.handlers.calendar.SessionContext')
    @patch('app.infrastructure.integration.datahub.handlers.calendar.http_request')
    def test_job_is_ok(self, http_request, test_db):
        http_request.return_value = [{"date": "29.12.2020"}, {"date": "30.12.2020"}, {"date": "31.12.2020"}]
        process()
