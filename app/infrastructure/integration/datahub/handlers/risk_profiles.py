from app.config import logger as log
from app.infrastructure.db.infra_dao import SessionContext
from app.infrastructure.tools import KeyCloak, Realms
from app.infrastructure.helpers import get_profile, update_client_risk_profile


def process():
    with SessionContext() as db_session:
        client_list = db_session.get_clients()
        auth_header = KeyCloak(Realms.PERSEUS).get_auth_header()

        for client in client_list:
            client_id = client['id']
            profiles = get_profile(client_id, str(client['master_id']), auth_header)
            update_client_risk_profile(client_id, profiles, auth_header)

        log.info(f'Results: total_clients={len(client_list)}')
