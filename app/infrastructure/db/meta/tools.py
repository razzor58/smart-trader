
def update_by_dict(_obj, _dict):
    for key, value in _dict.items():
        if key in _obj.__dict__:
            setattr(_obj, key, value)


def update_record_by_filter(session, model, entity_filter, values):
    session.query(model).filter_by(**entity_filter).update(values)


def delete_by_filter(session, model, filter_data):
    session.query(model).filter_by(**filter_data).delete()


def get_query_rows(qs):
    return [r._asdict() for r in qs]


def row2dict(row, no_pk=False, as_str=True):
    d = {}
    for column in row.__table__.columns:
        if column.primary_key and no_pk:
            continue
        attr_value = getattr(row, column.name)
        d[column.name] = str(attr_value) if as_str else attr_value

    return d


def model_qs_as_dict(qs):
    return [row2dict(r) for r in qs]


def filter_column_names(model, ignore_fields=None):
    return [t for t in model.__table__.columns.keys() if t not in ignore_fields]


def filter_values(model, row, ignore_fields=None):
    """ оставляем толлько те колонки объекта, которые есть в таблице БД """
    ignore_field_list = ignore_fields or ['id']
    column_names = filter_column_names(model, ignore_field_list)
    result_row = {k: v for k, v in row.items() if k in column_names}
    return result_row
