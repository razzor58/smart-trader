import unittest
import requests as r
from . import SERVICE_URL


class TestGetTrades(unittest.TestCase):
    """ Метод возращает на фронт шаблон пакетной заявки с актуальными ценами """

    resp = None

    def setUp(self):
        method = '/trades/26'
        url = SERVICE_URL + method
        self.resp = r.get(url).json()

    def test_response_is_valid_json(self):
        test_result = isinstance(self.resp, dict)
        self.assertTrue(test_result)

    def test_response_is_valid_structure(self):
        test_result = 'success' in self.resp and 'data' in self.resp and 'error' in self.resp
        self.assertTrue(test_result)

    def test_no_zero_limits(self):
        test_result = self.resp['data']['currencyLimitRUR'] == 0 and self.resp['data']['currencyLimitUSD'] == 0
        self.assertFalse(test_result)

    def test_trades_not_empty(self):
        test_result = len(self.resp['data']['buy']['items']) == 0 and len(self.resp['data']['sell']['items']) == 0
        try:
            self.assertFalse(test_result)
        except Exception:
            print(self.resp)
            raise

    def test_buy_operations_and_side(self):
        all_results_is_correct = True

        for trade in self.resp['data']['buy']['items']:
            if trade['side'] != 'buy':
                all_results_is_correct = False
            if trade['operation'] not in ('open long', 'close short'):
                all_results_is_correct = False

        self.assertTrue(all_results_is_correct)

    def test_sell_operations_and_side(self):
        all_results_is_correct = True

        for trade in self.resp['data']['sell']['items']:
            if trade['side'] != 'sell':
                all_results_is_correct = False
            if trade['operation'] not in ('close long', 'open short'):
                all_results_is_correct = False

        self.assertTrue(all_results_is_correct)


if __name__ == '__main__':
    unittest.main()
