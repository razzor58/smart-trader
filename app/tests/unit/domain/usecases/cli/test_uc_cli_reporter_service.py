from app.infrastructure.integration.proxy_fix import report_listener
from app.domain.constants import BidOperation
from unittest.mock import patch

clOrdId = '15604'
test_report = {
    'internalNum': '3127',
    'clOrdId': clOrdId,
    'execRepForBasicOrder': {
        'execRep': {
            'ordType': '1',
            'ordStatus': '2'
        },
        'execRepDetailNormalOrder': {
            'msgType': '8',
            'handlInst': '1',
            'ordStatus': '2',
            'execType': 'F',
            'orderQty': 66000.0,
            'cumQty': 96000.0,
            'clOrdId': clOrdId,
            'symbol': 'РусГидро',
            'idSource': '8',
            'securityId': 'HYDR',
            'side': '1',
            'ordType': '1',
            'avgPx': 0.5682,
            'execTransType': '0',
            'orderId': '191224-TQBR-19040094572112',
            'execId': 'TQBR-3LEdXL-B-1-1-N',
            'trdMatchId': '191224-TQBR-3062190127',
            'lastPx': 0.5682,
            'account': 'L01-00000F00',
            'currency': 'RUB',
            'clientId': 'B1/RKS101T',
            'text': f'RA_{clOrdId}',
            'tradeNum': '3062190127',
            'transactTime': '20191224-11:17:12.928',
            'tradeDate': '20191224',
            'orderNum': '19040094572',
            'exValue': 11364.0,
            'exUserId': 'NU0058905102',
            'dealSettlementCode': 'Y2',
            'tradeType': 'T',
            'commType': '3',
            'timeInForce': '0',
            'futSettDate': '20191226',
            'securityExchange': 'TQBR',
            'traderId': 'NU0058905102',
            'currCode': 'RUB',
            'userId': '161470',
            'tradeTimeGMT': '-3:00:00',
            'firmId': 'NC0058900000',
            'tradingSessionSubId': '3',
            'requestId': '4301',
            'lastLiquidityInd': 2
        }
    }
}


class TestReporterUseCase:

    @patch('app.infrastructure.db.domain_dao.get_session')
    @patch('grpc.intercept_channel')
    def test_reporter_service_is_ok(self, test_intercept_channel, test_db_session):
        report_listener.run()

    @patch('app.infrastructure.db.domain_dao.get_session')
    def test_process_message_is_ok(self, test_db_session):
        report_listener.process_message(test_report, test_db_session)

    def test_opposit_operations_is_correct(self):
        assert BidOperation.get_opposite('open long') == 'close long'
        assert BidOperation.get_opposite('open short') == 'close short'
        assert BidOperation.get_opposite('close long') == 'open long'
        assert BidOperation.get_opposite('close short') == 'open short'
