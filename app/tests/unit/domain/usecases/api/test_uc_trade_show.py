import pytest
from datetime import datetime

from app.domain.entities.plain import Client
from app.domain.usecases.api.trade_show import TradeShowUseCase
from app.domain.constants import Operation, PositionTypes, Currencies, TickerTypes, BidOperation


class Prices:
    current_time = 1588178090
    DATA = {'GAZP': {'price': 220, 'time': current_time},
            'USD000UTSTOM': {'price': 72.5, 'time': current_time},
            'SBER': {'price': 420, 'time': current_time},
            'RU000A0ZYCK6': {'price': 101.2, 'time': current_time}
            }


class TestBids:
    EMPTY_LIST = []
    TEST_LIST = [{
        'bucket_num': 1,
        'ticker': 'GAZP',
        'operation': Operation(PositionTypes.LONG).OPEN,
        'nominal': 1,
        'shares_in_lot': 1,
        'client_nominal': 1,
        'title': 'Gazprom',
        'classcode': 'TQBR',
        'shares_count': 1,
        'isin': 'RU0034AZAAGA',
        'code': 'RU0034AZAAGA',
        'currency': Currencies.RUR.value,
        'type': TickerTypes.SHARES,
        'short_description': 1,
        'price_deviation': 0.01,
        'min_step': 1,
        'current_balance': 0,
    },
        {
            'bucket_num': 1,
            'ticker': 'SBER',
            'operation': Operation(PositionTypes.LONG).CLOSE,
            'nominal': 1,
            'shares_in_lot': 1,
            'client_nominal': 1,
            'title': 'SBERBANK',
            'classcode': 'TQBR',
            'shares_count': 1,
            'isin': 'RU0034AZAAGB',
            'code': 'RU0034AZAAGB',
            'currency': Currencies.RUR.value,
            'type': TickerTypes.SHARES,
            'short_description': 1,
            'price_deviation': 0.01,
            'min_step': 1,
            'current_balance': 0,
        },
        {
            'bucket_num': 1,
            'ticker': 'USDRUB',
            'operation': Operation(PositionTypes.LONG).OPEN,
            'nominal': 1,
            'shares_in_lot': 1,
            'client_nominal': 1,
            'title': 'Gazprom',
            'classcode': 'TQBR',
            'shares_count': 1,
            'isin': 'USD000UTSTOM',
            'code': 'USD000UTSTOM',
            'currency': Currencies.RUR.value,
            'type': TickerTypes.SHARES,
            'short_description': 1,
            'price_deviation': 0.01,
            'min_step': 1,
            'current_balance': 0,
        },
        {
            'bucket_num': 1,
            'ticker': 'RU000A0ZYCK6',
            'operation': Operation(PositionTypes.LONG).OPEN,
            'nominal': 1,
            'shares_in_lot': 1,
            'client_nominal': 1,
            'title': 'OFZ 25083',
            'classcode': 'TQOB',
            'shares_count': 1,
            'isin': 'RU000A0ZYCK6',
            'code': 'RU000A0ZYCK6',
            'currency': Currencies.RUR.value,
            'type': TickerTypes.BONDS,
            'short_description': 1,
            'price_deviation': 0.01,
            'min_step': 1,
            'current_balance': 0,
        }
    ]


@pytest.fixture
def bids(value):
    return value


class TestTradeShowUseCase:

    @pytest.fixture(autouse=True)
    def _get_context(self, db_mock, test_ticker, bids):
        db_mock.get_bids.return_value = [{
            'ticker': test_ticker, 'operation': 'open long', 'nom_part': 1
        }]

        db_mock.get_or_create_client.return_value = (1, Client({
            'agreement_date': datetime.today(),
            'filling_datetime': datetime.today(),
            'fullname': 'test client',
            'agreement_number': 1231312,
            'client_code': 'B1/12313',
            'profile_name': 'Professional'
        }))

        db_mock.get_last_bids.return_value = bids
        db_mock.get_money_limits.return_value = [
            {'curr_code': Currencies.SUR.value, 'current_bal': 1000},
            {'curr_code': Currencies.USD.value, 'current_bal': 0},
        ]
        db_mock.get_date_options.return_value = [
            {'isin': 'RU000A0ZYCK6', 'currentFacevalue': 1001.2, 'accruedInterest': 13.3}]

    @pytest.mark.parametrize('bids', [TestBids.EMPTY_LIST, TestBids.TEST_LIST])
    def test_trade_show_is_executed(self, db_mock, portfolio_id, test_login, bids):
        use_case = TradeShowUseCase(login=test_login,
                                    portfolio_id=portfolio_id,
                                    portfolio_dao=db_mock)
        data = use_case.execute(prices=Prices.DATA)

        assert isinstance(data, dict)

    @pytest.mark.parametrize('bids', [TestBids.EMPTY_LIST, TestBids.TEST_LIST])
    def test_get_side_is_ok(self, bids):
        assert BidOperation.side_is('open long') == 'buy'
        assert BidOperation.side_is('close long') == 'sell'
        assert BidOperation.side_is('open short') == 'sell'
        assert BidOperation.side_is('close short') == 'buy'
