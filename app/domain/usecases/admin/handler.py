from app.domain.interfaces import IPortfolioDAO


class AdminUseCase:

    def __init__(self, db: IPortfolioDAO):
        self.db = db

    def get_main_page(self):
        return self.db.get_strategy_details()
