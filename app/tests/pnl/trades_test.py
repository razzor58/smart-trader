import pandas as pd

TEST_DESCRIPTION = {'vol_test': 'Сумма всех vol равна current acc_vol*max_part',
                    'long_test': 'Сумма всех лонгов должна быть неотрицательна',
                    'short_test': 'Сумма всех шортов должна быть неположительна',
                    'deals_test': 'Каждая сдлека должна быть непустой по лотам',
                    'profit_test': 'Прибыль по закрытым сдлекам не может быть нулевой',
                    'sign_sell_test': 'Все продажи должны иметь знак минус',
                    'sign_buy_test': 'Все покупки должны иметь знак плюс',
                    'count_one_lot_deals': 'Нет сделок с одним лотом'}


def get_error_df(resp):
    """
    :param resp: Ответ по формату https://rais.broker.ru/api/v3/pnl/25'
    :return: error_df - DataFrame c ошибками
    """
    positions = pd.DataFrame.from_records(resp['positions'])
    positions = positions.T.drop(['client_id', 'open_date', 'status', 'operation', 'id', 'portfolio_id', 'ticker'],
                                 axis=1)

    res = []
    for ticker in resp['trades']:
        temp = pd.DataFrame.from_records(resp['trades'][ticker])
        res.append(temp)

    trades = pd.concat(res)
    trades['open_date'] = pd.to_datetime(trades['open_date'])
    trades = trades.set_index('open_date')
    trades['profit'] = trades['profit'].replace('', 0).bfill()
    trades['quantity'] = trades['quantity'].replace('', 0).bfill()
    trades['commission'] = trades['commission'].replace('', 0).bfill()
    trades['vol_prc'] = trades['vol_prc'].replace('', 0).bfill()

    ticker_list = list(trades['ticker'].unique())

    cols = list(TEST_DESCRIPTION.keys())

    def error_finder(trades_df, positions_df, tickers):
        vol_prc = 'vol_prc'
        quantity = 'quantity'
        profit = 'profit'
        direction = 'direction'
        dict_control = {}
        tick_col = trades_df['ticker']
        deal_col = trades_df[quantity]
        prof_col = trades_df[profit]
        direct_col = trades_df[direction]
        for _ticker in tickers:
            vols = trades_df[tick_col == _ticker][vol_prc]
            vols_long = trades_df[(tick_col == _ticker) & ((direct_col == 'close long') | (direct_col == 'open long'))][
                vol_prc]
            vols_short = trades_df[(tick_col == _ticker) & (
                    (direct_col == 'close short') | (direct_col == 'open short'))][vol_prc]
            curr_pos = positions_df.loc[_ticker, 'acc_vol']
            max_p = positions_df.loc[_ticker, 'max_part']
            non_zero_deals = trades_df[(tick_col == _ticker) & (deal_col != 0)][quantity]
            deals = trades_df[tick_col == _ticker][quantity]
            zero_profit = \
                trades_df[(tick_col == _ticker) & (prof_col == 0) & (
                 (direct_col == 'close long') | (direct_col == 'close short'))][
                    profit]  # закрытые лонги или шорты
            sign_sell = \
                trades_df[(tick_col == _ticker) & (deal_col >= 0) & (
                 (direct_col == 'open short') | (direct_col == 'close long'))][
                    quantity]
            sign_buy = \
                trades_df[(tick_col == _ticker) & (deal_col <= 0) & (
                 (direct_col == 'open long') | (direct_col == 'close short'))][
                    quantity]
            count_of_1 = trades_df[(tick_col == _ticker) & (deal_col == 1)][quantity]

            # nested func

            def test_result(result):
                return 'good' if result else 'bad'

            def vol_control(x, y, z):
                return test_result(round(x.sum(), 1) == round(y * z * 100, 1))

            def count_is_equal(x, y):
                return test_result(x.count() == y.count())

            def is_exists(x):
                return test_result(x.count() == 0)

            def sum_not_negative(x):
                return test_result(x.sum() >= -0.00001)

            def sum_not_positive(x):
                return test_result(x.sum() <= 0.00001)

            # create summary dict
            dict_control[_ticker] = [vol_control(vols, curr_pos, max_p),
                                     sum_not_negative(vols_long),
                                     sum_not_positive(vols_short),
                                     count_is_equal(non_zero_deals, deals),
                                     is_exists(zero_profit),
                                     is_exists(sign_sell),
                                     is_exists(sign_buy),
                                     is_exists(count_of_1)]
        return dict_control

    error_dict = error_finder(trades,
                              positions,
                              ticker_list)

    error_df = pd.DataFrame(error_dict, index=cols)

    return error_df
