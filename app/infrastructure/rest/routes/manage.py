from flask_restplus import Resource
import json

from app.infrastructure.integration.rabbit.producer import Producer
from app.infrastructure.rest.schemas import MasterDto
from app.infrastructure.rest.tools import check_portfolio_id, ApiMessage, api_response
from app.infrastructure.db.domain_dao import SessionContext

api = MasterDto.master_trade_api
_master_trade = MasterDto.master_trade


@api.route('/create_signal/<int:portfolio_id>', doc=False)
class TradeMaster(Resource):

    @api.expect(_master_trade)
    @check_portfolio_id
    def post(self, portfolio_id):
        """
            Создать сигналы всем клиентам портфеля
            в момент отправки сигнала через админку RAIS
        """
        with SessionContext() as db:

            # TODO: to use_case
            db.update_position_fields({
                'portfolio_id': portfolio_id,
                'client_id': 1
            }, **api.payload)

        producer = Producer()
        message = {
            "portfolio_id": portfolio_id,
            "template_id": api.payload['template_id']
        }
        producer.publish(json.dumps(message))

        return api_response(data={'message': ApiMessage.BID_TASK_CREATED})
