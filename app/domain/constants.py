from enum import IntEnum, IntFlag, Enum

LANDING_PORTFOLIOS = [162, 163, 164, 165, 166]

# TODO: MARGIN_SHORT_SIZE make dynamic
MARGIN_SHORT_SIZE = 0.04383
DEFAULT_PRICE_STEP = 0.01

DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'

DATE_FORMAT = '%Y-%m-%d'
DEFAULT_USA_SECTION = 'SPBXM'

VIRTUAL_CASH = 'CASH'
REAL_PORTFOLIO_ID = 1


class TestClient:
    ACCOUNT = 'B1/RKS101T'
    ID = 2072


class TabNames(Enum):
    DIVIDENDS = 'dividends'
    TRADES = 'trade_history'
    BIDS = 'bids'
    STATS = 'stats'
    POSITIONS = 'positions'

    @classmethod
    def list(cls):
        return [member for name, member in cls.__members__.items()]


class DividendTabNames:
    EXPECTED = 'expected'
    RECEIVED = 'received'


class Currencies(Enum):
    USD = 'USD'
    RUR = 'RUR'
    RUB = 'RUB'
    SUR = 'SUR'
    CASH = 'CASH'
    USDRUB = 'USDRUB'
    USDRUR = 'USDRUR'
    USD000000TOD = 'USD000000TOD'
    USD000UTSTOM = 'USD000UTSTOM'

    @staticmethod
    def get_currency(currency_code):
        if currency_code == Currencies.SUR.value:
            return Currencies.RUR.value
        return currency_code


class CurrencyCodes:
    USD = '840'
    RUR = '810'


class TickerTypes:
    BONDS = 'bonds'
    SHARES = 'stock'
    MONEY = 'money'
    CURRENCY = 'currency'


class SyncStatus(IntFlag):
    SYNCHRONIZED = 1
    NOT_SYNCHRONIZED = 0


class ResponsesTypes(IntEnum):
    SUCCESS = 0
    ERROR = 1


class TariffsTypes:
    DEFAULT = 1


class OperationTypes(Enum):
    OPEN = 'open'
    CLOSE = 'close'


class PositionTypes(Enum):
    LONG = 'long'
    SHORT = 'short'
    WAIT = 'wait'


class TradeTypes:
    BUY = 'buy'
    SELL = 'sell'


class QuikTradeTypes:
    BUY = 'Купля'
    SELL = 'Продажа'


class ProxyFixTradeTypes:
    BUY = "1"
    SELL = "2"


class Operation:
    OPEN: str
    CLOSE: str

    def __init__(self, position_type):
        self.OPEN = 'open ' + position_type.value
        self.CLOSE = 'close ' + position_type.value


class BidOperation:
    @classmethod
    def side_is(cls, operation, is_quik_format=False):
        if operation in [Operation(PositionTypes.LONG).OPEN, Operation(PositionTypes.SHORT).CLOSE]:
            return ProxyFixTradeTypes.BUY if is_quik_format else TradeTypes.BUY
        else:
            return ProxyFixTradeTypes.SELL if is_quik_format else TradeTypes.SELL

    @classmethod
    def get_opposite(cls, operation):
        position_type = PositionTypes.SHORT if PositionTypes.SHORT.value in operation else PositionTypes.LONG

        if OperationTypes.CLOSE.value in operation:
            return Operation(position_type).OPEN
        else:
            return Operation(position_type).CLOSE


class Limits:
    T2 = 'T2'


class Landings(Enum):
    TOP5 = 'top5'
    AI = 'ai'
    ALL = 'all'
    SHOWCASE = 'showcase'


class PnlView:
    VOL_TYPE_VALUE = "pnl"
    VOL_TYPE_FIELD = "vol_type"
    TYPE_FIELD = "type"
    TRADE = "trade"
    DIVIDEND = "divs"
    QUOTES = "eod"

    ORDER_INDEXES = {
        QUOTES: 0,
        TRADE: 1,
        DIVIDEND: 2,
    }


class BidRecord:
    NEW = "NEW"
    SEND = "SEND"
    SEND_ERR = "SEND_ERR"
    ACCEPT_ERR = "ACCEPT_ERR"
    ACCEPT = "ACCEPT"
    PART = "PART"
    COMPLETE = "COMPLETE"
    CANCELED = "CANCELED"
    ARCHIVE = "ARCHIVE"


class BidStatus:
    EXECUTED = "Исполнена"
    NOT_EXECUTED = "Не исполнена"
    PART_EXECUTED = "Частично исполнена"
    ACCEPTED = "Принята к исполнению"


class ExecStatus:
    NEW = 0
    PARTIALLY_FILL = 1
    FILL = 2
    CANCELED = 4
    REPLACED = 5
    PENDING_CANCEL = 6
    REJECTED = 8
    PENDING_REPLACE = 'E'
    PENDING_NEW = 'A'


class CoreError:
    COMMON_EXCEPTION = 'Ошибка. Пожалуйста, повторите операцию позже'
    NO_QUIK_LIMITS = 'Не подключен тариф'
    TRADE_NOT_ALLOWED_FOR_CLIENT = 'Торговля для указанного клиента не активирована'
    TRADE_SESSION_CLOSE = 'Торговая сессия закрыта'
    NEW_ASSET_TOO_SMALL = 'Сумма меньше рекомендованной'
    PORTFOLIO_NOT_FOUND = 'Портфель не подключен'
    PORTFOLIO_NOT_ALLOWED_TO_SUBSCRIBE = 'Выбранный портфель не совместим с вашими текущими портфелями'
    PORTFOLIO_UNKNOWN = 'Неизвестный портфель'
    EXPRESS_API_ERROR = 'Не удалось получить текущие цены'
    BONDS_PRICE_NOT_FOUND = 'Нет актуальный цен облигаций'
    RISK_PROFILE_INCOMPATIBLE = 'Портфель не соответствует вашему инвестиционному профилю. Выберите другой портфель, ' \
                                'или заполните анкету инвестиционного профилирования'
    USER_UNKNOWN = 'Неизвестный пользователь'


STOCK_NAMES = {
    'TQBR': 'торговая система ЗАО "ФБ ММВБ" (сектор рынка Основной рынок)',
    'NYSE_BEST': 'ПАО «Санкт-Петербургская биржа» (иностранные ценные бумаги)',
    'NASDAQ_BEST': 'ПАО «Санкт-Петербургская биржа» (иностранные ценные бумаги)'
}


def get_default_account(ticker_type, ticker_currency):
    if ticker_type == TickerTypes.CURRENCY:
        return 'M20067800001'
    elif ticker_currency == Currencies.USD.value:
        return 'SP01-CL00000'
    else:
        return 'L01-00000F00'
