from flask import request as fr
from base64 import b64decode
from app.config import logger
from app.infrastructure.db.domain_dao import SessionContext
from app.infrastructure.tools import KeyCloak, Realms
from app.domain.constants import CoreError
from app.settings import API_ALLOWED_USERS


SUCCESS_RESP = {"message": "success", "status": 200}

SIDE_RUS = {
    'buy': 'Покупка',
    'sell': 'Продажа'
}

TYPES_RUS = {
    'bonds': 'Облигация',
    'stock': 'Акция',
    'currency': 'Валюта'
}

db_session = SessionContext()

KeyCloakRealm = KeyCloak(Realms.BROKER, load_public_key=True)


class InvalidTokenException(Exception):
    pass


class PermissionDeniedException(Exception):
    pass


class ApiErrors:
    COMMON_EXCEPTION = 'Ошибка. Пожалуйста, повторите операцию позже'
    NO_QUIK_LIMITS = 'Не подключен тариф'
    TRADE_NOT_ALLOWED_FOR_CLIENT = 'Торговля для указанного клиента не активирована'
    TRADE_SESSION_CLOSE = 'Торговая сессия закрыта'
    NEW_ASSET_TOO_SMALL = 'Сумма меньше рекомендованной'
    PORTFOLIO_NOT_FOUND = 'Портфель не подключен'
    PORTFOLIO_NOT_ALLOWED_TO_SUBSCRIBE = 'Выбранный портфель не совместим с вашими текущими портфелями',
    PORTFOLIO_UNKNOWN = 'Неизвестный портфель'
    EXPRESS_API_ERROR = 'Не удалось получить текущие цены'
    RISK_PROFILE_INCOMPATIBLE = 'Портфель не соответствует вашему инвестиционному профилю. Выберите другой портфель, ' \
                                'или заполните анкету инвестиционного профилирования'
    UNAUTHORIZED = 'Авторизация неуспешна'
    PERMISSION_DENIED = 'Доступ запрещен'
    MISSING_PARAMETERS = 'Некорректные параметры запроса'


class ApiMessage:
    BID_TASK_CREATED = 'Запущен процесс создания пакетных заявок'


def get_verified_login(access_token):
    try:
        login = KeyCloakRealm.get_verified_token(access_token)['k_login']
    except Exception as e:
        logger.error(e, exc_info=True)
        logger.error(f'token={access_token}')
        raise InvalidTokenException(str(e))

    return login


def api_response(data=None, success=True, error=None, error_code=None, **kwargs):
    """
        SUCCESS_RESP_MODEL: (https://jira.bcs.ru:4443/jira/browse/BROK-6110)
        {
            "data": null, // object or null - данные ответа
            "success": false, // bool - статус обработки метода api
            // если ошибка ...
            "error": { // object or null - данные ошибки
                "code": 2, // int - внутренний статус-код ошибки
                "status": "rate_limit", // string or null - системное описание ошибки
                "message": "На вашем аккаунте превышен лимит обращений к системе. ",
                // string or null - НЕ технический статус.
                // Сообщение для пользователя с понятным описанием и рекомендацией дальнейшего действия
                (при необходимости)
                "url": null // string or null - url ссылки на страницу с дополнительной информацией (опционально)
            }
        }
    """
    if error is not None:
        success = False
        error_obj = {
            "code": error_code,
            "status": None,
            "message": str(error),
            "url": None
        }
    else:
        error_obj = None

    resp = {
        "data": data,
        "success": success,
        "error": error_obj
    }

    if error_code:
        final_resp = (resp, error_code)
        logger.error(final_resp)
    else:
        final_resp = resp
        logger.info(final_resp)

    return final_resp


def decode_header(data):
    site_login_from_header = data.get('HTTP_X_LOGIN')
    if site_login_from_header is not None:
        site_login = b64decode(site_login_from_header).decode()
    else:
        site_login = None
    return site_login


def auth_user(user):
    access_token = fr.headers.environ.get('HTTP_X_JWT')
    return get_verified_login(access_token) == user


def auth_app(func):
    def function_wrapper(*args, **kwargs):
        access_token = fr.headers.environ.get('HTTP_X_JWT')
        if get_verified_login(access_token) not in API_ALLOWED_USERS:
            raise PermissionDeniedException
        return func(*args, **kwargs)

    return function_wrapper


def get_login_from_token(func):
    def function_wrapper(*args, **kwargs):
        # Входные параметры
        params = fr.view_args
        url_method = fr.path

        try:
            portfolioId = int(fr.args.get('portfolioId'))
        except Exception:
            portfolioId = None

        # HTTP_X_RequestId - выставляет NGINX
        session_id = fr.headers.environ.get('HTTP_X_RequestId') or 'unknown_request_id'

        # HTTP_X_JWT - выставляет на БКС Экспрессе
        access_token = fr.headers.environ.get('HTTP_X_JWT')

        if access_token:
            k_login = get_verified_login(access_token)
        else:
            k_login = 'k-213213'

        # Логируем запрос
        logger.info("req {0} {1}: ".format(session_id, k_login) + " " + url_method + " " + str(params))

        # Передаем полученное значение в параметры запроса
        kwargs.update({"k_login": k_login, "access_token": access_token, "session_id": session_id,
                       "portfolio_id_from_qs": portfolioId})
        return func(*args, **kwargs)

    return function_wrapper


def check_portfolio_id(func):
    """
    Проверяем, есть ли запрошенный портфель в справочнике портфелей
    """

    def function_wrapper(*args, **kwargs):

        portfolio_list = db_session.get_available_strategies()
        portfolio_id = int(kwargs.get('portfolio_id'))

        if portfolio_id in portfolio_list or portfolio_id is None:
            return func(*args, **kwargs)
        else:
            return api_response(error=CoreError.PORTFOLIO_UNKNOWN)

    return function_wrapper


def check_portfolio_id_client(func):
    """
    Проверяем, есть ли запрошенный портфель у клиента
    """

    def function_wrapper(*args, **kwargs):
        portfolio_id = int(kwargs.get('portfolio_id'))
        login = kwargs.get('k_login')
        portfolio_list = db_session.get_portfolio_clients(portfolio_id, login)

        if len(portfolio_list) > 0:
            return func(*args, **kwargs)
        else:
            return api_response(error=CoreError.PORTFOLIO_NOT_FOUND)

    return function_wrapper


def get_form_data(func):
    def function_wrapper(*args, **kwargs):
        post_data = fr.form.to_dict()
        if len(post_data) > 0:
            kwargs.update({'post_data': post_data})
        return func(*args, **kwargs)

    return function_wrapper


def exception_handler(e):
    error = ApiErrors.COMMON_EXCEPTION

    if isinstance(e, InvalidTokenException):
        error = ApiErrors.UNAUTHORIZED

    session_id = fr.headers.environ.get('HTTP_X_RequestId') or 'unknown'
    logger.error(f"err {session_id}:  {e}", exc_info=True)

    return api_response(error=error, error_code=500)
