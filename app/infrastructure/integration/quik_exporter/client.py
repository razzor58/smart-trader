import io
import sys
import xml.etree.ElementTree as Et
from app import settings
from app.config import logger as log
from app.domain.usecases.cli.exporter_service import ExporterService
from app.infrastructure.tools import KafkaMessageNotUniqueException
from app.infrastructure.integration.quik_exporter.tools import (get_consumer, MESSAGE_CONFIG, TAG_PREFIX, db_session,
                                                                save_messages, update_limits, parse_xml_root,
                                                                get_dict_from_value_list, MessageType, get_client_code)


def main(service_param):
    """
    Kafka - consumer для полчучения данных по сделкам и лимитам клиентов
    Для локального запуска настроить прослушивание тестового кластера командой:
    oc port-forward -n bm-kfk bm-kfk-common-kafka-0 9990:9990
    """

    topic = MESSAGE_CONFIG[service_param]['topic']
    client_tag = 'trd_acc_id' if service_param == 'futures_holding' else 'client_code'

    log.info(f'== SERVICE_PARAM: {service_param} TOPIC: {topic} CLIENT_TAG: {client_tag} ==')

    # Получаем объект подключения
    consumer = get_consumer(topic, service_param)

    # Чтение из очереди
    for message in consumer:
        try:
            # Запоминаем указатель
            last_offset = message.offset

            # Читаем сообщение
            source = io.BytesIO(message.value)

            # Создаем xml структуру
            tree = Et.parse(source)
            root = tree.getroot()

            # Определяем тип сообщения
            root_name = root.tag.replace(TAG_PREFIX, '')

            # Ненужные сообщения пропускаем
            if root_name not in MESSAGE_CONFIG:
                log.info(f'Unexpected message: {root_name}')
                continue

            # Сохраняем в БД
            db_row = parse_xml_root(root)
            message_data = get_dict_from_value_list(root_name, db_row)
            if root_name in [MessageType.QUIK_TRADE.value, MessageType.FUTURES_HOLDINGS.value]:
                client = db_session.get_client_by_code(client_code=get_client_code(root, client_tag))
                if client and client.trading_allowed:
                    try:
                        save_messages(root_name, [message_data])
                    except KafkaMessageNotUniqueException:
                        continue

            if root_name in [MessageType.MONEY_LIMITS.value, MessageType.DEPO_LIMITS.value]:
                update_limits(root_name, message_data)
            # Обновляем состояние клиентского портфеля
            try:
                log.debug(f'try to apply {root_name}')
                if root_name == MessageType.QUIK_TRADE.value:
                    ExporterService.apply_quik_trade_from_kafka(message_data, db_session)
                elif root_name == MessageType.MONEY_LIMITS.value:
                    ExporterService.apply_quik_limit_from_kafka(message_data, db_session)

                db_session.session_commit()

            except Exception as e:
                log.error(message_data)
                log.error(e, exc_info=True)

            # Логируем количество обработанных сообщений
            if last_offset % settings.KAFKA_LOGS_BATCH_SIZE == 0:
                log.info(f'last_offset: {last_offset}')

        except Exception as e:
            log.error(e, exc_info=True)
            log.error('Exit on error')
            sys.exit()

        finally:
            db_session.session_close()
