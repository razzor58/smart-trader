import unittest
import requests as r
from . import PORTFOLIO_ID, SERVICE_URL


class TestServiceAddZeroCash(unittest.TestCase):
    """    Подключение клиента на услугу

   """
    resp = None

    def setUp(self):
        method = '/service/add'
        url = SERVICE_URL + method
        message = {
            "portfolio_id": PORTFOLIO_ID,
            "cash": 200000,
            "replace": True
        }
        self.resp = r.post(url, json=message).json()

    def test_service_add_resp_is_valid(self):
        test_result = isinstance(self.resp, dict)
        self.assertTrue(test_result)

    def test_service_add_resp_is_ok(self):
        self.assertTrue(self.resp['success'])


if __name__ == '__main__':
    unittest.main()
