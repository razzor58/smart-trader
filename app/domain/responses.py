# coding: utf-8
from typing import Any, Optional
from app.domain.constants import CoreError, ResponsesTypes
from app.domain.interfaces import IResponse
from app.domain.entities.plain import ScheduleResult


class Response(IResponse):
    data: Any
    success: bool
    error: Optional[CoreError]

    def get_type(self) -> ResponsesTypes:
        return ResponsesTypes.SUCCESS if self.success else ResponsesTypes.ERROR


class SchedulerResponse(Response):
    data: ScheduleResult
    success: bool
    error: str

    def __init__(self, data: ScheduleResult):
        self.data = data
        self.success = self.data.success_counter > self.data.error_counter


class SuccessResponse(Response):

    def __init__(self, data: Optional[Any] = None):
        self.data = data
        self.success = True
        self.error = None


class ErrorResponse(Response):

    def __init__(self, error: CoreError):
        self.data = None
        self.success = False
        self.error = error
