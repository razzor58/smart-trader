from unittest.mock import patch, MagicMock
from app.infrastructure.integration.datahub.handlers.tickers_ytm import process


class TestTickersYtmJob:
    @patch('app.infrastructure.integration.datahub.handlers.tickers_ytm.KeyCloak')
    @patch('app.infrastructure.integration.datahub.handlers.tickers_ytm.SessionContext')
    @patch('app.infrastructure.integration.datahub.handlers.tickers_ytm.http_request')
    def test_job_is_ok(self, http_request, test_db, test_token):
        mo = MagicMock()
        mo.get_ticker_list.return_value = [{'isin': 'RU000A0JR1Y7', 'trade_mode': None, 'close': 101.0}]
        test_db.return_value.__enter__.return_value = mo
        http_request.return_value = [{'id': 'RU000A0JR1Y7', 'dataList': [{'is_offer': False,
                                                                          'date': '2020-09-15T00:00:00Z',
                                                                          'yield': 5.0939,
                                                                          'dur': 117}]}]
        process()
