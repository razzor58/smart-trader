from app.infrastructure.db.domain_dao import SessionContext
from unittest.mock import patch
from pytest import fixture


class TestDomainDao:

    @fixture(autouse=True)
    @patch('app.infrastructure.db.meta.engine.Session')
    def _get_context(self, mock_session):
        self.session = SessionContext()

    def test_define_depo_accounts(self):
        self.session.define_accounts_from_depo(client_id=4)
