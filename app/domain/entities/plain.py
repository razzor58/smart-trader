from datetime import datetime, timedelta
from uuid import uuid4
from typing import Optional, Any
from app.domain.constants import SyncStatus, TickerTypes
from app.domain.tools import create_from_dict


class Entity:
    def __init__(self, _dict: Any):
        create_from_dict(self, _dict)


class ClientsPortfolio:
    id: int
    create_date: datetime
    client_id: int
    portfolio_id: int
    cash: int
    date_from: datetime
    date_to: datetime
    status: int
    start_asset: float
    shares: float
    asset: float
    income: float
    last_eod: datetime
    last_changed: datetime
    cash_deposit: float
    total_divs: float
    commission_size: float
    total_long_size: float
    total_short_size: float
    margin_short_size: float
    margin_long_size: float
    start_collateral: float
    min_collateral: float
    fullness: float
    tariff_id: int
    currency: str
    accordance: float
    asset_for_stranegy: float
    synchronized: SyncStatus

    def __init__(self, client_portfolio: Any):
        create_from_dict(self, client_portfolio)


class BidTriggers:
    id: int
    create_date: datetime
    portfolio_id: int
    portfolio_name: str
    action_time: datetime
    signal_id: int
    position_type: str
    operation: str
    price: float
    ticker: str
    code: str
    classcode: str
    vol: float
    acc_vol: float
    max_part: float
    comment: str
    result_code: int
    result: str
    status: int
    position_id: int
    complete: int
    total: int


class Client(Entity):
    id: int
    login: str
    fullname: str
    tariff_id: uuid4
    appended: datetime
    master_id: uuid4
    source: str
    client_code: str
    agreement_number: str
    agreement_date: datetime
    agreement_id: str
    trading_allowed: bool
    create_by: str
    risk_profile: uuid4
    landing_page: str


class ClientsBids:
    id: int
    bucket_num: uuid4
    create_date: datetime
    portfolio_id: int
    client_id: int
    login: str
    signal_id: int
    ticker: str
    operation: str
    nominal: int
    shares_in_lot: int
    bid_time: datetime
    send_status: str
    exec_status: int
    exec_message: str
    type: str
    quik_trade_vol: int
    trade_num: int
    order_num: int
    create_by: str
    client_nominal: int
    client_accept_time: datetime
    ax_price: float
    ex_value: float
    side: int


class ClientsPositions:
    id: int
    client_id: int
    portfolio_id: int
    status: int
    ticker: str
    position_type: str
    nom_part: int
    open_date: datetime
    calculate_profit: float
    fixed_profit: float
    cost: float
    real_part_cost: float
    avg_open_price: float
    shares_in_lot: int
    price: float
    rur_price: float
    tradedate: datetime
    div_size: float
    acc_vol: float
    vol: float
    max_part: float
    operation: str
    ticker_currency: str
    ticker_type: str
    recommend_part: int
    is_signal_active: bool
    signal_size: int
    last_signal_time: datetime

    def __init__(self, position: Any):
        create_from_dict(self, position)


class ClientsProfiles:
    id: int
    client_id: int
    questionary_id: uuid4
    template_id: uuid4
    filling_datetime: datetime
    confirmed: bool
    isActual: bool
    profile_id: uuid4
    profile_name: str


class ClientsMS:
    id: int
    login: str
    tariff_id: uuid4
    appended: datetime
    master_id: uuid4
    source: str
    client_code: str
    agreement_id: uuid4


class Deposit:
    id: int
    tradedate: datetime
    client_id: int
    portfolio_id: int
    asset_size: float
    create_date: datetime
    status: int
    commission: float
    from_depo_limit: bool
    deposit: float

    def __init__(self,
                 client_id: int,
                 portfolio_id: int,
                 new_asset_size: float,
                 tradedate: Optional[datetime] = datetime.now(),
                 deposit: Optional[float] = 0):

        self.client_id = client_id
        self.portfolio_id = portfolio_id
        self.asset_size = new_asset_size
        self.tradedate = tradedate
        self.deposit = deposit
        self.create_date = datetime.now()


class Dividend:
    id: int
    client_id: int
    portfolio_id: int
    ticker: str
    currency: str
    div_size: float
    div_date: datetime


class MarketTrades:
    id: int
    client_id: int
    portfolio_id: int
    ticker: str
    direction: str
    open_vol: float
    price: float
    vol_prc: float
    quantity: int
    remain_quantity: int
    shares_in_lot: int
    market_price: float
    open_date: datetime
    close_date: datetime
    profit: float
    status: int


class PortfolioReport(Entity):
    id: int
    tradedate: datetime
    cash: float
    cash_with_divs: float
    cash_deposit: float
    asset: float
    asset_with_divs: float
    asset_no_reinvest: float
    asset_no_reinv_divs: float
    asset_rur: float
    income: float
    income_no_reinvest: float
    income_no_reinv_divs: float
    total_divs: float
    total_long_size: float
    total_short_size: float
    commission_size: float
    commission_size_divs: float
    start_collateral: float
    min_collateral: float
    start_collateral_divs: float
    min_collateral_divs: float
    margin_short_size: float
    margin_long_size: float
    fullness: float
    portfolio_id: int
    shares: float
    client_id: int


class PortfolioState:
    id: int
    tradedate: datetime
    cash: float
    cash_with_divs: float
    asset: float
    asset_with_divs: float
    asset_no_reinvest: float
    asset_no_reinv_divs: float
    income: float
    income_no_reinvest: float
    income_no_reinv_divs: float
    total_divs: float
    total_long_size: float
    total_short_size: float
    commission_size: float
    commission_size_divs: float
    start_collateral: float
    min_collateral: float
    start_collateral_divs: float
    min_collateral_divs: float
    margin_short_size: float
    margin_long_size: float
    fullness: float
    portfolio_id: int


class RiskProfile:
    id: int
    profile_id: uuid4
    profile_name: str
    risk_level: int
    group_name: str


class Trade:
    id: int
    client_id: int
    portfolio_id: int
    ticker: str
    direction: str
    open_vol: float
    price: float
    vol_prc: float
    quantity: int
    remain_quantity: int
    shares_in_lot: int
    market_price: float
    open_date: datetime
    close_date: datetime
    profit: float
    status: int
    margin_long: float
    margin_short: float
    commission: float
    trade_num: int
    trade_date_time: datetime
    value: float
    accruedint: float


class DateOption:
    id: int
    isin: str
    tradedate: str
    error: str
    couponDate: str
    couponPeriod: int
    couponRate: float
    payPerBond: float
    offerDate: str
    buyBackPrice: str
    currentFacevalue: float
    accruedInterest: float
    listingLevel: int
    sumCouponsMty: float
    sumCouponsOffer: str
    lastFacevalueChangeDate: str


class LastQuote:
    id: int
    classcode: str
    securcode: str
    tradedate: datetime
    tradecount: int
    high: float
    low: float
    open: float
    close: float
    value: float
    lasttrade_date: datetime
    date: datetime
    port: int
    short_code: str
    usd_close: float


class Portfolio:
    id: int
    name: str
    start_calc_date: str
    valid_from: datetime
    valid_to: datetime
    status: int
    recommended_cash: int
    income_avg: float
    income_total: float
    max_dd: float
    signals_cnt_avg: int
    currency: str
    description: str
    show_on_site: bool
    expert_id: int
    tariff: int
    currency_cash: int
    bids_autoconfirm: int
    owner: str
    tariff_id: int
    backtest_type: str
    signal_subscribed: int
    row_num: int
    risk_level: int
    risk_profile: str
    iir_enabled: int

    def __init__(self, portfolio: Any):
        create_from_dict(self, portfolio)


class PortfolioPage:
    id: int
    portfolio_id: int
    landing_page: str


class PortfolioResult:
    id: int
    tradedate: datetime
    cash: float
    cash_with_divs: float
    asset: float
    asset_with_divs: float
    asset_no_reinvest: float
    asset_no_reinv_divs: float
    income: float
    income_no_reinvest: float
    income_no_reinv_divs: float
    total_divs: float
    total_long_size: float
    total_short_size: float
    commission_size: float
    commission_size_divs: float
    start_collateral: float
    min_collateral: float
    start_collateral_divs: float
    min_collateral_divs: float
    margin_short_size: float
    margin_long_size: float
    fullness: float
    portfolio_id: int


class PortfolioTicker(Entity):
    id: int
    portfolio_id: int
    ticker: str
    hardcode_part: float
    valid_from: datetime
    valid_to: datetime
    max_part: float
    current_vol: float
    vol_update_time: datetime
    show_on_site: bool


class Quotes(Entity):
    id: int
    classcode: str
    securcode: str
    tradedate: datetime
    tradecount: int
    high: float
    low: float
    open: float
    close: float
    value: float
    lasttrade_date: datetime
    date: datetime
    port: int
    short_code: str
    usd_close: float


class Rates:
    id: int
    currency: str
    margin_type: str
    date_from: datetime
    date_to: datetime
    min_deal_size: int
    max_deal_size: int
    margin: float
    comission: float


class Report:
    id: int
    create_date: datetime
    timestamp: datetime
    ticker: str
    title: str
    algo: str
    deals: int
    signals: int
    return_sum: float
    return_reinvest: float
    return_average: float
    dd_max: float
    dd_time: float
    dd_avg: float
    dd_var95: float
    bp_0perc: float
    bp_1perc: float
    bp_2perc: float
    bp_3perc: float
    bp_0dd: float
    bp_1dd: float
    bp_2dd: float
    bp_3dd: float


class Signals:
    id: int
    create_date: datetime
    short_code: str
    signal: str
    operation: str
    vol: float
    acc_vol: float
    open: float
    close: float
    hi: float
    lo: float
    time: datetime
    volume: str
    analyzed_id: int
    upload_flag: bool
    deal_price: float
    position_type: str
    price: float


class SignalsRais(Entity):
    id: int
    create_date: datetime
    portfolio_id: int
    portfolio_name: str
    expert_portfolio_id: int
    signal_time: datetime
    signal_id: int
    position_type: str
    operation: str
    price: float
    ticker: str
    code: str
    secur_code: str
    classcode: str
    expert_ticker_id: int
    expert_operation_id: int
    vol: float
    acc_vol: float
    hardcode_part: float
    change_position: float
    acc_position: float
    comment: str
    result_code: int
    result: str
    status: int
    position_id: int


class SignalsTemplates:
    id: int
    create_date: datetime
    portfolio_id: int
    portfolio_name: str
    expert_portfolio_id: int
    signal_time: datetime
    signal_id: int
    position_type: str
    operation: str
    priceh: float
    ticker: str
    code: str
    secur_code: str
    classcode: str
    expert_ticker_id: int
    expert_operation_id: int
    vol: float
    acc_vol: float
    hardcode_part: float
    change_position: float
    acc_position: float
    comment: str
    result_code: int
    result: str
    status: int
    create_by: str


class TariffRules(Entity):
    __tablename__ = 'tariff_rules'

    id: int
    tariff_id: int
    currency: str
    date_from: datetime
    date_to: datetime
    asset_from: int
    asset_to: int
    commission: float


class Tariffs:
    id: int
    name: str
    status: int


class Ticker(Entity):

    id: int
    create_date: datetime
    code: str
    currency: str
    decimals: int
    isin: str
    short_code: str
    stock: str
    ticker_id: int
    title: str
    liquid: int
    ticker: str
    classcode: str
    secur_code: str
    type: str
    shares_in_lot: int
    short_description: str
    price_deviation: int
    shortname: str
    trade_mode: int
    mat_date: datetime
    scale: int
    min_step: float

    def __init__(self, currency=None, **kwargs):

        self.name = kwargs['ticker']
        self.shares_in_lot = kwargs['shares_in_lot']
        self.type = kwargs.get('type', TickerTypes.SHARES)
        self.currency = currency


class DepoLimits:
    id: int
    event_time: datetime
    document_id: uuid4
    create_date: datetime
    client_code: str
    sec_code: str
    firm_id: str
    trd_acc: str
    load_date: datetime
    limit_kind: str
    locked_buy: float
    locked_buy_value: float
    locked_sell: float
    locked_sell_value: float
    current_bal: float
    current_limit: float
    open_bal: float
    open_limit: float
    firm_name: str
    sec_name: str
    awg_position_price: float


class FutureHoldings:
    id: int
    event_time: datetime
    document_id: uuid4
    create_date: datetime
    firm_id: str
    sec_code: str
    trd_acc_id: str
    type: str
    trade_date: datetime
    avr_posn_price: float
    cbpl_planned: int
    cbpl_used: int
    varmargin: float
    position_value: float
    open_buys: float
    open_sells: float
    start_buy: float
    start_net: float
    start_sell: float
    today_buy: float
    today_sell: float
    total_net: float
    firm_name: str
    sec_name: str
    mat_date: str
    total_varmargin: str
    real_varmargin: str
    session_status: str


class MoneyLimits:
    id: int
    event_time: datetime
    document_id: uuid4
    create_date: datetime
    load_date: datetime
    client_code: str
    curr_code: str
    firm_id: str
    tag: str
    limit_kind: str
    leverage: str
    locked_value_coef: float
    current_bal: float
    current_limit: float
    locked: float
    open_bal: float
    open_limit: float
    firm_name: str
    locked_margin_value: float


class OnlineReports:
    id: int
    create_date: datetime
    internalNum: int
    msgType: int
    handlInst: int
    ordStatus: int
    execType: str
    orderQty: float
    cumQty: float
    leavesQty: float
    lastQty: float
    clOrdId: str
    symbol: str
    idSource: int
    securityId: str
    side: int
    ordType: int
    avgPx: float
    execTransType: int
    orderId: str
    execId: str
    trdMatchId: str
    lastPx: float
    account: str
    currency: str
    clientId: str
    text: str
    tradeNum: str
    transactTime: str
    tradeDate: str
    orderNum: str
    exValue: float
    exUserId: str
    dealSettlementCode: str
    tradeType: str
    commType: int
    timeInForce: str
    futSettDate: str
    securityExchange: str
    traderId: str
    currCode: str
    userId: str
    tradeTimeGMT: str
    exchangeOrderId: str
    firmId: str
    tradingSessionSubId: int
    requestId: int
    lastLiquidityInd: int


class QuikTrades:
    id: int
    event_time: datetime
    document_id: uuid4
    create_date: datetime
    trade_num: float
    trade_date: datetime
    operation: str
    class_code: str
    trade_date_time: datetime
    order_num: float
    sec_code: str
    account: str
    client_code: str
    qty: float
    qty_pcs: float
    price: float
    value: float
    repo_value: float
    accruedint: float
    settle_code: str
    settle_date: datetime
    settle_currency: str
    exchange_code: str
    trades_time_ms: int
    price2: float
    repo2value: float
    accruedint2: float
    repo_term: int
    ts_commission: float
    linked_trade: float
    kind: str
    kindFlag: int
    r_flags: int


class WorkDays:
    id: int
    dates: datetime
    next_date: datetime
    prev_date: datetime


class CommonLimit:
    nom_part: int
    last_close: float

    def __init__(self, nom_part=0, **kwargs):
        self.nom_part = nom_part
        self.last_close = kwargs.get('last_close') or 0


class ScheduleResult:
    exec_time: timedelta
    success_counter: int
    error_counter: int
    total: int

    def __init__(self, exec_time, success_counter, error_counter, total):
        self.exec_time = exec_time
        self.success_counter = success_counter
        self.error_counter = error_counter
        self.total = total
