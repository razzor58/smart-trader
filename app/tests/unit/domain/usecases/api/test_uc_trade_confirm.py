import pytest
from uuid import uuid4
from datetime import datetime

from app.domain.constants import CoreError, Currencies, TickerTypes
from app.domain.constants import Operation, PositionTypes, BidOperation
from app.domain.responses import SuccessResponse
from app.domain.usecases.api.trade_confirm import TradeConfirmUseCase


class CustomPayload:
    bid = {'ticker': 'GAZP',
           'sharesCount': 10,
           'nominal': 1,
           'price': 230,
           'dev_prc': 1.01,
           'type': TickerTypes.SHARES,
           'currency': Currencies.RUR.value
           }

    ADMIN_SITE_FORMAT = {
        'data': {'buy': {'items': [bid]}, 'sell': {'items': []}},
        'num': uuid4()
    }
    EXPRESS_FORMAT = {
        'data': [bid],
        'num': uuid4()
    }


@pytest.fixture
def test_payload(payload_value):
    return payload_value


class TestTradeConfirmUseCase:

    @pytest.fixture(autouse=True)
    def _get_context(self, db_mock, test_ticker, portfolio_id, test_payload):
        self._db = db_mock
        self._db.get_bids_for_send.return_value = {
            test_ticker: {
                'ticker': test_ticker,
                'id': 1,
                'bucket_num': 'asdada',
                'operation': Operation(PositionTypes.LONG).OPEN,
                'nominal': 1,
                'shares_in_lot': 1,
                'min_step': 1,
                'scale': 1,
                'type': TickerTypes.SHARES,
                'currency': Currencies.RUR.value,
                'classcode': 'TQBR',
                'client_code': 1,
                'shares_coun': 1,
            }}

        self._use_case = TradeConfirmUseCase(login='owner',
                                             portfolio_id=portfolio_id,
                                             portfolio_dao=db_mock,
                                             payload=test_payload,
                                             )

    @pytest.mark.parametrize('test_payload', [CustomPayload.EXPRESS_FORMAT])
    def test_stock_is_close(self, test_payload):
        data = self._use_case.execute(execute_time=datetime.now().replace(hour=9))
        assert data.error == CoreError.TRADE_SESSION_CLOSE

    @pytest.mark.parametrize('test_payload', [CustomPayload.EXPRESS_FORMAT])
    def test_trade_is_not_allowed(self, test_payload):
        self._db.check_trade_allowed.return_value = False
        data = self._use_case.execute(execute_time=datetime.now().replace(hour=11))
        assert data.error == CoreError.TRADE_NOT_ALLOWED_FOR_CLIENT

    @pytest.mark.parametrize('test_payload', [CustomPayload.ADMIN_SITE_FORMAT, CustomPayload.EXPRESS_FORMAT])
    def test_trade_confirm_is_executed(self, test_payload):
        data = self._use_case.execute(execute_time=datetime.now().replace(hour=11))
        assert isinstance(data, SuccessResponse)

    @pytest.mark.parametrize('test_payload', [(1, CustomPayload.EXPRESS_FORMAT)])
    def test_handle_response(self, test_payload):
        try:
            self._use_case.handle_response({
                'data': {
                    'details': {
                        131: {
                            'send_status': 'OK',
                            'client_nominal': 1
                        }
                    }
                }
            })
        except Exception as e:
            pytest.fail(f"test_handle_response failed with error: {e}")

    @pytest.mark.parametrize('test_payload', [CustomPayload.EXPRESS_FORMAT])
    def test_use_default_depo_accounts(self, test_payload):
        self._db.define_accounts_from_depo.return_value = {}
        data = self._use_case.execute(execute_time=datetime.now().replace(hour=11))
        assert isinstance(data, SuccessResponse)

    @pytest.mark.parametrize('test_payload', [CustomPayload.EXPRESS_FORMAT])
    def test_get_quik_side_is_ok(self, test_payload):
        assert BidOperation.side_is('open long', is_quik_format=True) == '1'
        assert BidOperation.side_is('close short', is_quik_format=True) == '1'
        assert BidOperation.side_is('close long', is_quik_format=True) == '2'
        assert BidOperation.side_is('open short', is_quik_format=True) == '2'
