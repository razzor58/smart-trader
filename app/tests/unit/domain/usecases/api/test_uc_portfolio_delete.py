from app.domain.usecases.api.portfolio_delete import PortfolioDeleteUseCase


class TestPortfolioHomePageUseCase:

    def test_execute(self, db_mock, portfolio_id, test_login):
        use_case = PortfolioDeleteUseCase(login=test_login,
                                          portfolio_id=portfolio_id,
                                          portfolio_dao=db_mock)
        data = use_case.execute()
        assert data is None
