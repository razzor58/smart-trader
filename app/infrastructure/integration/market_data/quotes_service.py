# -*- coding: cp1251 -*-
from requests import Request
from requests import Session
import pytz
import json
from app.config import logger
from app import settings

'''
https://api.bcs.ru/quotes-partner/v1/351AAE82-ABA8-4688-8C58-ECCAFEFB63D1?symbols=WMT&mode=real
[dbo].[usp_quotes_get_online_info]
Ans:
{
       {"t", (long) (time_stm.ToUniversalTime() - utils.epoch).TotalSeconds}
      ,{"ch", Math.Round(last_day_candle.close.Value - prev_day_candle.close.Value, 3)}
      ,{"chp", Math.Round(last_day_candle.close.Value/prev_day_candle.close.Value - 1, 4)}
      ,{"lp", last_day_candle.close.Value}
      ,{"ask", last_day_candle.close.Value}
      ,{"bid", last_day_candle.close.Value}
      ,{"open", last_day_candle.open.Value}
      ,{"high", last_day_candle.high.Value}
      ,{"low", last_day_candle.low.Value}
      ,{"prev_close", prev_day_candle.close.Value}
      ,{"vol", last_day_candle.value.Value}
      ,{"scale", scale ?? 2}
}
'''


class Worker(object):

    def __init__(self):
        # ������������ ���������
        self.session = Session()
        self.tz_info = pytz.timezone('Europe/Moscow')

    def _request(self, params, **kwargs):
        req = dict()
        method = 'GET'
        req['params'] = params
        req['url'] = settings.QUOTES_SERVICE_URL

        req_for_send = Request(method, **req).prepare()

        logger.info("{0}: {1}".format(method, req_for_send.url))
        try:
            response = self.session.send(req_for_send,
                                         proxies=settings.HTTPS_PROXY,
                                         timeout=settings.REQUEST_TIMEOUT)
        except Exception as e:
            raise e

        return response

    def get_ticker_price(self, tickers):
        result = {}
        params = {
                'symbols': ",".join(list(tickers)),
                'mode': 'real'
            }
        response = self._request(params)
        body = json.loads(response.content)
        if body['s'] == 'error':
            logger.error(body['errmsg'])
        else:
            for item in body['d']:
                try:
                    result[item['n']] = {'price': item['v']['ask'], 'time':  item['v']['t']}
                except Exception as e:
                    logger.error(e, exc_info=True)
        return result
