from collections import defaultdict
from copy import deepcopy
from datetime import datetime
from app import settings
from app.domain.entities.business.trader import TradeManager
from app.domain.entities.plain import ClientsPositions
from app.domain.constants import VIRTUAL_CASH, BidRecord, PositionTypes
from app.domain.tools import define_bid_operation, get_bucket_num


class Bid(object):
    """ Реализация механики создания пакетной заявки:
          - Получаем данные по текущим позициям из БД
          - Получаем доли мастер-портфеля на нужную дату
          - Считаем лоты из долей
          - Вычисляем дельту и сохраняем дельту - выставляем заявки
      """
    def __init__(self, portfolio):
        self.portfolio = portfolio
        self.db = self.portfolio.db

    @staticmethod
    def define_scenario_by_operation(current_state, new_state):
        client_position = current_state[1]

        # Определяем данные по интеллекту полю операция "open long"
        # Позиция = long/close Направление - long/close
        position_bb = new_state['operation'].split(' ')[1]
        direction_bb = new_state['operation'].split(' ')[0]

        if client_position == position_bb:
            scenario = 'B1'
        elif client_position == 'wait':
            if direction_bb == 'close':
                scenario = 'B2'
            else:
                scenario = 'B3'
        else:
            scenario = 'B2'

        return scenario

    def get_signal_size_by_scenario(self, current_state, new_state):
        """
        Сценарии заливки сигнала https://jira.bcs.ru:4443/jira/browse/BROK-6414
        """
        scenario = self.define_scenario_by_operation(current_state, new_state)
        direction = new_state['operation'].split(' ')[0]

        rec_part = new_state['nom_part']
        prev_rec = current_state[2]
        nom_part = current_state[0]

        # блок №1
        if scenario == 'B1':
            if direction == 'open':
                qty = rec_part - prev_rec
            else:
                if abs(nom_part) >= abs(prev_rec):
                    qty = rec_part - prev_rec
                else:
                    qty = min(abs(nom_part), abs(rec_part - prev_rec))

        # Блок №2
        elif scenario == 'B2':
            qty = rec_part - nom_part

        # Блок №3
        elif scenario == 'B3':
            qty = rec_part - prev_rec

        # Сюда не должны попадать
        else:
            qty = 0

        return qty

    def get_delta(self, old_positions, new_positions):
        """ Считает разницу между двумя наборами позиций для выставления заявок"""
        new_positions_items = new_positions.items()
        current_state = dict(
            map(
                lambda x: (x[0], (x[1].nom_part, x[1].position_type, x[1].recommend_part)),
                old_positions.items()
            )
        )
        delta = defaultdict(dict)

        for ticker, new_position in new_positions_items:

            # Если по позиции есть активный сигнал - то исполняем только его.
            if new_position['is_signal_active']:
                delta[ticker]['qty'] = self.get_signal_size_by_scenario(current_state[ticker], new_position)
                delta[ticker]['type'] = 's'
            elif ticker in old_positions:
                delta[ticker]['qty'] = new_position['nom_part'] - old_positions[ticker].nom_part
                delta[ticker]['type'] = 'p'
            else:
                delta[ticker]['qty'] = new_position['nom_part']
                delta[ticker]['type'] = 'p'

        result_delta = {
            "current_state": current_state,
            "delta": delta
        }
        return result_delta

    def save(self, delta):
        """ Сохраняем пакетку """
        bids_to_save = []

        bucket_num = get_bucket_num()
        for ticker, v in delta.items():

            # Только для позиций из портфеля
            if ticker in self.portfolio.positions:
                if int(v['qty']) != 0 and ticker != VIRTUAL_CASH:
                    item = {
                        'operation': define_bid_operation(PositionTypes(self.portfolio.positions[ticker].position_type),
                                                          v['qty']),
                        'create_date': datetime.now(),
                        'bid_time': datetime.now().strftime('%Y-%m-%d'),
                        'ticker': ticker,
                        'nominal': v['qty'],
                        'send_status': BidRecord.NEW,
                        'shares_in_lot': self.portfolio.positions[ticker].shares_in_lot,
                        'client_id': self.portfolio.client_id,
                        'portfolio_id': self.portfolio.portfolio_id,
                        'bucket_num': bucket_num,
                        'type':  v['type']
                    }
                    bids_to_save.append(item)

        self.db.save_bids(bids_to_save)

    @staticmethod
    def is_signal_expired(last_signal_time):
        this_moment = datetime.now()
        signal_days_ago = (this_moment - last_signal_time).days
        is_signal_time = signal_days_ago > settings.SIGNAL_ACTIVE_DAYS
        return is_signal_time

    def is_signal_active(self, ticker):

        try:
            last_signal_time = self.portfolio.positions[ticker]['last_signal_time']
        except Exception:
            last_signal_time = None

        if last_signal_time is None:
            res = False
        elif self.is_signal_expired(last_signal_time):
            res = False
        else:
            res = True

        return res

    def remove_prev(self):
        """Когда надо удалить заявки"""
        self.db.remove_prev_bids(self.portfolio.portfolio_id, self.portfolio.client_id)

    def is_same_portfolio(self, position: ClientsPositions):
        return position.portfolio_id == self.portfolio.portfolio_id

    def find_positions_to_change(self, **kwargs):
        """
        Выбираем только те позиции по которым, есть изменение позиции
        (либо vol - сигнал, либо acc_vol - кода увеличился кэш)
        предполагается, что vol меняется в момент появления сигнала по мастер портфелю.
        position_filter = 'acc_vol' if kwargs.get('is_rebalanced', False) else 'vol'

        # position_to_change = [
            v for k, v in master_portfolio_table.positions.items() if float(v[position_filter]) != 0
        ]
        :return:
        """
        # Для любых случаем ищем в НЕвалютных позициях, которые относятся к текущему портфелю
        position_to_change_source = self.portfolio.shares_positions

        # Если изменился размер инвест части
        if kwargs.get('is_rebalanced'):
            position_to_change = [v.__dict__ for v in position_to_change_source if (v.acc_vol != 0 or v.vol != 0)]

        # Если размер инвест части приравнен к 0 - клиент выводит средства из под управления
        elif kwargs.get('full_exit'):
            position_to_change = []
            for position in position_to_change_source:
                if position.nom_part != 0:
                    item = deepcopy(position)
                    item.nom_part = 0
                    position_to_change.append(item.__dict__)

        # Первоначальное подключение с учетом реальных лимитов
        elif kwargs.get('client_import'):
            # Проставляем признак того, что работаем с рельаным портфелем клиента
            for position in position_to_change_source:
                position.client_import = True

            position_to_change = [p.__dict__ for p in position_to_change_source if (p.vol != 0 or p.nom_part != 0)]

        # Просто сигнал
        else:
            position_to_change = [p.__dict__ for p in position_to_change_source if p.vol != 0]

        return position_to_change

    def create(self, **kwargs):
        """
        Создание пакетной заяки
        Обновление поля "рекомендованная доля" в позициях портфеля.
        Сценарии, которые используют этот вызов должны далле создать или обновить позиции портфеля в БД
        """
        # Позиции на текущий момент
        old_positions = dict([(k, v) for k, v in deepcopy(self.portfolio.positions).items()
                              if self.is_same_portfolio(v)])

        # Новые позиции
        new_positions = defaultdict(dict)

        # Определяем список бумаг, по которым нужно создать пакетную заявку
        position_to_change = self.find_positions_to_change(**kwargs)

        # Создаем трейдера для пересчета позиций и подтягиваем историю сделок
        t = TradeManager(portfolio=self.portfolio, mode='real', trades=position_to_change)

        # Проставляем размер рекомендованно доли для каждой бумаги из списка
        rec_sum = 0
        for position in position_to_change:
            ticker = position['ticker']

            # Основной алгоритм расчета лотов по капиталу, max доли в портфеле и накопленной позиции в интеллекте
            result_tuple = t.get_trade_size(position)
            nom_part = 0 if kwargs.get('full_exit', False) else result_tuple[0]
            signal_size = result_tuple[1]

            # Записываем рекомендованный объем покупки в массив для сравнения
            new_positions[ticker]['nom_part'] = nom_part
            new_positions[ticker]['operation'] = self.portfolio.positions[ticker].operation

            # Записываем эталонную долю и рекомендованный объем покупки в состояние портфеля
            self.portfolio.positions[ticker].recommend_part = nom_part
            self.portfolio.positions[ticker].signal_size = signal_size
            new_positions[ticker]['signal_size'] = signal_size

            recommend_share_cost = float(nom_part * position['price'] * position['shares_in_lot'])
            self.portfolio.positions[ticker].cost = recommend_share_cost

            # Проверяем - активный ли сейчас сигнал
            # TODO: Вернуться потом когда появится время протестить нормально
            # is_signal_active = self.is_signal_active(ticker)
            is_signal_active = False
            self.portfolio.positions[ticker].is_signal_active = is_signal_active
            new_positions[ticker]['is_signal_active'] = is_signal_active

            # суммируем изменения по кэшу
            rec_sum += abs(recommend_share_cost)

        # Меняем CASH
        new_cash = 0 if kwargs.get('full_exit') else round(self.portfolio.asset - rec_sum, 2)
        self.portfolio.positions[VIRTUAL_CASH].cost = new_cash

        # Вычисляем дельту между позициями "до" и "после" работы трейдера
        delta = self.get_delta(old_positions, new_positions)['delta']

        # Архивируем старые заявки
        self.remove_prev()

        # Сохраняем пакетную заявку
        self.save(delta)
