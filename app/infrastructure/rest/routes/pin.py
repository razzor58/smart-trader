from flask_restplus import Resource
from requests.exceptions import Timeout
import requests as r
import uuid
import os

from app.config import logger
from app import settings

from app.infrastructure.rest.schemas import PinDto
from app.infrastructure.rest.tools import get_form_data
from app.infrastructure.rest.tools import api_response
from app.infrastructure.rest.tools import get_login_from_token

TEST_USER = 'k-213213'
TEST_USERS = ['k-213213', 'test1', 'test2', 'test3', 'test4', 'test5', 'test6', 'test7', 'TestClientAi', 'Test2']

STAGE_ENV = True if os.environ.get('STAGE_ENV', 0) == 1 else False

api = PinDto.pin_api
MS_TRADE_PIN_URL_TEST = settings.TRADE_PIN_URL_TEST
MS_TRADE_PIN_URL = settings.TRADE_PIN_URL

KEY_CLOAK_URL = settings.KEY_CLOAK_BROKER_TEST_URL


def get_test_access_token():
    post_params = {
        'username': settings.KEY_CLOAK_BROKER_TEST_USER,
        'password': settings.KEY_CLOAK_BROKER_TEST_PASSWORD,
        'grant_type': 'password',
        'client_id': settings.KEY_CLOAK_BROKER_TEST_CLIENT_ID,
        'client_secret': settings.KEY_CLOAK_BROKER_TEST_CLIENT_SECRET,
    }
    try:
        resp = r.post(KEY_CLOAK_URL,
                      data=post_params,
                      timeout=settings.REQUEST_TIMEOUT,
                      proxies=settings.HTTPS_PROXY).json()
        if len(resp.get('error', '')) > 0:
            access_token = None
        else:
            access_token = resp['access_token']
    except Timeout as e:
        access_token = None
        resp = {'error_description': "Нет подключения к серверу авторизации"}
        logger.error("Timeout: " + str(e))

    except Exception as e:
        access_token = None
        resp = {'error_description': str(e)}
        logger.error("Exception: " + str(e))

    return TEST_USER, access_token, resp


def ms_resp(path, message, k_login=None, access_token=None, **kwargs):
    # Общие параметры
    session_id = kwargs.get('session_id', 'unknown_session')
    req_id = str(uuid.uuid4()).lower()
    resp = ''
    # Если в тестовой среде нет jwt токена, то идем в тестовый кеykloak за токеном для TEST_USER
    if k_login is None or k_login in TEST_USERS:
        k_login, access_token, kk_resp = get_test_access_token()

    if access_token is None:
        return api_response(error=f'Авторизация для клиента {k_login} неуспешна')

    # Добавляем k-логин в запрос
    message.update({"klogin": k_login})

    # Значения по умолчанию
    error_code = None
    error_message = None

    # Выполняем запрос
    try:
        if STAGE_ENV or k_login in TEST_USERS:
            service_url = MS_TRADE_PIN_URL_TEST
        else:
            service_url = MS_TRADE_PIN_URL

        final_url = service_url + path + '?reqId=' + req_id

        logger.debug("int {}: ms_req: ".format(session_id) + final_url + " data:" + str(message))

        resp = r.post(final_url,
                      json=message,
                      headers={'Authorization': "Bearer " + access_token},
                      timeout=settings.REQUEST_TIMEOUT,
                      proxies=settings.HTTPS_PROXY)

        ms_data = resp.json()

        logger.debug("int {0} {1}: ms_ans: ".format(session_id, k_login) + final_url + " data:" + str(ms_data))

        errors = ms_data.get('errors', [])
        data = ms_data.get('data')
        if len(errors) > 0:
            error_code = errors[0]['code']
            error_message = errors[0]['message']

        if int(ms_data.get('status', 200)) == 401:
            error_message = 'Срок действия сессии истек. Пожалуйста, авторизуйтесь на сайте повторно'

    except Exception as e:
        errors = "TradePinError: " + str(e)
        data = None
        logger.info(resp)
        logger.error("int {0} {1}: ms_err: ".format(session_id, k_login) + str(e), exc_info=True)

    success = False if errors else True
    return api_response(data=data, success=success, error=error_message, error_code=error_code, k_login=k_login)


@api.route('/addPIN')
class AddPinClass(Resource):

    @api.expect(PinDto.add_pin_model)
    @get_login_from_token
    @get_form_data
    def post(self, **kwargs):
        """Добавить торговый пароль"""
        payload = kwargs.get('post_data', api.payload)
        message = {
            'pin': payload.get('pin'),
            'smsSource': 'BCS Broker',
        }
        return ms_resp('add-pin', message, **kwargs)


@api.route('/confirmSMS')
class ConfirmSMSClass(Resource):

    @api.expect(PinDto.confirm_sms_model)
    @get_login_from_token
    @get_form_data
    def post(self, **kwargs):
        """подтвердить торговый пароль смс кодом"""
        payload = kwargs.get('post_data', api.payload)

        message = {
            'code': payload.get('code')
        }
        return ms_resp('confirm-sms', message, **kwargs)


@api.route('/resendSMS')
class ResendSMSClass(Resource):

    @get_login_from_token
    def post(self, **kwargs):
        """Отправить смс"""
        message = {}

        return ms_resp('resend-sms', message, **kwargs)


@api.route('/checkPin')
class CheckPinClass(Resource):

    @api.expect(PinDto.check_pin_model)
    @get_login_from_token
    @get_form_data
    def post(self, **kwargs):
        """Проверка пароля"""
        payload = kwargs.get('post_data', api.payload)
        message = {
            'pin': payload.get('pin'),
        }
        return ms_resp('check-pin', message, **kwargs)


@api.route('/deletePin')
class DeletePinClass(Resource):

    @get_login_from_token
    def post(self, **kwargs):
        """Удаление пароля"""

        message = {}
        return ms_resp('delete-pin', message, **kwargs)


@api.route('/havePin')
class HavePinClass(Resource):

    @get_login_from_token
    def post(self, **kwargs):
        """Проверка наличия пароля"""

        message = {}
        return ms_resp('have-pin', message, **kwargs)
