"""adding 'video' field to portfolio_tickers

Revision ID: e4831b3dc5d2
Revises: 6e561aab7e54
Create Date: 2020-06-09 09:36:52.223643

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e4831b3dc5d2'
down_revision = '6e561aab7e54'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('portfolio_tickers', sa.Column('video', sa.String(length=200), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('portfolio_tickers', 'video')
    # ### end Alembic commands ###
