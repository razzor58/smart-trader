from requests import Request, Session, get
from app import settings
from jwt import get_unverified_header, decode
from jwt.algorithms import RSAAlgorithm
from json import dumps
from app.config import logger


def http_request_raw(base_url, method, **kwargs):
    req = dict()
    req['url'] = base_url
    req['params'] = kwargs.get('params')
    req['headers'] = kwargs.get('headers')
    req['json'] = kwargs.get('json')
    req['data'] = kwargs.get('data')

    req_for_send = Request(method, **req).prepare()
    session = Session()
    raw_response = session.send(req_for_send)
    raw_response.raise_for_status()

    return raw_response


def http_request(base_url, method, **kwargs):
    return http_request_raw(base_url, method, **kwargs).json()


def normalize_json(data):
    item = {}
    for key, value in data.items():
        if isinstance(value, dict):
            item.update(dict([(k, v) for k, v in value.items()]))
        else:
            item[key] = value
    return item


class Realms:
    PERSEUS = 'perseus'
    BROKER = 'Broker'
    RU_DATA = 'ru_data'


class KeyCloak:
    def __init__(self, realm: Realms, load_public_key=False):
        self.realm = realm
        self.base_url = settings.KEY_CLOAK_BASE_URL_INT if realm == Realms.PERSEUS else settings.KEY_CLOAK_BASE_URL
        self.public_keys = self.get_public_keys() if load_public_key else {}

        if realm == Realms.RU_DATA:
            self.token_field = 'token'
            self.url = settings.RU_DATA_AUTH_URL
            self.proxy = None
        else:
            self.token_field = 'access_token'
            self.url = f'{self.base_url}{realm}/protocol/openid-connect/token'
            self.proxy = None

    def get_public_keys(self):
        public_keys = {}
        try:
            jwk_keys = get(f'{self.base_url}{self.realm}/protocol/openid-connect/certs').json()
            for jwk in jwk_keys['keys']:
                kid = jwk['kid']
                public_keys[kid] = RSAAlgorithm.from_jwk(dumps(jwk))
        except Exception as e:
            logger.error(e, exc_info=True)
            logger.error('KeyCloak public key is unreachable, API Auth will not work')
        return public_keys

    def get_verified_token(self, access_token):
        kid = get_unverified_header(access_token)['kid']
        return decode(access_token,
                      key=self.public_keys[kid],
                      algorithms=['RS256'],
                      options={'verify_aud': False})

    def get_auth_header(self):
        if self.realm == Realms.RU_DATA:
            request_body = {
                'json': {
                    'login': settings.RU_DATA_LOGIN,
                    'password': settings.RU_DATA_PASSWORD
                }
            }
        elif self.realm == Realms.PERSEUS:
            request_body = {
                'data': {
                    'username': settings.KEY_CLOAK_PERSEUS_USER,
                    'password': settings.KEY_CLOAK_PERSEUS_PASSWORD,
                    'grant_type': 'password',
                    'client_id': settings.KEY_CLOAK_PERSEUS_CLIENT_ID,
                    'client_secret': settings.KEY_CLOAK_PERSEUS_CLIENT_SECRET
                }}
        else:
            request_body = {
                'data': {
                    'username': settings.KEY_CLOAK_BROKER_USER,
                    'password': settings.KEY_CLOAK_BROKER_PASSWORD,
                    'grant_type': 'password',
                    'client_id': settings.KEY_CLOAK_BROKER_CLIENT_ID,
                    'client_secret': settings.KEY_CLOAK_BROKER_CLIENT_SECRET
                }}

        response = http_request(self.url, 'POST', proxies={'https': self.proxy}, **request_body)

        return {'Authorization': f'Bearer {response[self.token_field]}'}


class KafkaMessageNotUniqueException(Exception):
    pass
