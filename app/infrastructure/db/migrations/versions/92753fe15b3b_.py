"""empty message

Revision ID: 92753fe15b3b
Revises: 36d8667dbed6
Create Date: 2020-03-28 23:08:15.989521

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '92753fe15b3b'
down_revision = '36d8667dbed6'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('clients_portfolios', sa.Column('synchronized', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('clients_portfolios', 'synchronized')
    # ### end Alembic commands ###
