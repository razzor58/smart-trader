import unittest
import requests as r
from . import PORTFOLIO_ID, SERVICE_URL


class TestStatPostions(unittest.TestCase):
    """    Проверка вкладки состав   """
    resp = None
    TAB_NAME = 'positions'

    def setUp(self):
        method = '/stat/{portfolio_id}/{tab_name}'.format(portfolio_id=PORTFOLIO_ID, tab_name=TestStatPostions.TAB_NAME)
        url = SERVICE_URL + method
        self.resp = r.post(url).json()

    def test_positions_resp_is_valid(self):
        test_result = isinstance(self.resp, dict)
        self.assertTrue(test_result)

    def test_positions_resp_is_ok(self):
        try:
            self.assertTrue(self.resp['success'])
        except Exception:
            print(self.resp)
            raise


if __name__ == '__main__':
    unittest.main()
