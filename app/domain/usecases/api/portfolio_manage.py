from datetime import datetime
from collections import defaultdict

from app.domain.interfaces import IPortfolioDAO
from app.domain.constants import Landings, Currencies, DEFAULT_USA_SECTION, TickerTypes


class PortfolioManageUseCase:
    def __init__(self, portfolio_dao: IPortfolioDAO):
        self.db = portfolio_dao

    @staticmethod
    def calc_expected_income(target, last_price):

        if float(target) == 0.0 or float(last_price) == 0.0:
            res = 100
        else:
            res = round((target / last_price - 1) * 100, 2)
        if res < 0:
            res = 0

        return res

    def get_portfolio(self, portfolio_id):
        return self.db.get_portfolio_origin(portfolio_id, as_dict=True)

    def create_portfolio(self, values):
        if 'landing_page' in values:

            landing_page = values['landing_page']
            del values['landing_page']
        else:
            landing_page = Landings.ALL.value

        p_id = self.db.create_master_portfolio(values)

        self.db.save_rows('pages', [{'landing_page': landing_page, 'portfolio_id': p_id}])
        return p_id

    def update_portfolio(self, portfolio_id, data):
        return self.db.update_master_portfolio(portfolio_id, data)

    def save_tickers(self, data):
        self.db.create_portfolio_tickers(data)

    def update_tickers(self, portfolio_id, ticker, data):
        return self.db.update_portfolio_tickers(portfolio_id, ticker, data)

    def delete_tickers(self, portfolio_id, ticker):
        return self.db.update_portfolio_tickers(portfolio_id, ticker, {"status": 1})

    def get_portfolio_tickers(self, portfolio_id, ticker=None):
        data = {
            "tickers": self.db.get_portfolio_tickers(portfolio_id, ticker),
            "portfolio_name": self.db.get_portfolio_origin(portfolio_id).name
        }
        return data

    def get_tickers_meta_data(self, portfolio_id):
        data = {
            "tickers": self.db.get_all_from_table('tickers'),
            "industries": self.db.get_all_from_table('industries'),
            "issuers": self.db.get_all_from_table('issuers'),
            "portfolio_name": self.db.get_portfolio_origin(portfolio_id).name
        }
        return data

    def get_portfolios_meta_data(self):
        data = {
            "landing_pages": self.db.get_all_from_table('pages'),
        }
        return data

    @staticmethod
    def sorted_structure(v):
        sort_field = 'ytm' if v['id'] in (3, 4) else 'expected_income'
        v['structure'] = sorted(v['structure'], key=lambda x: x[sort_field], reverse=True)

        return v

    def get_details(self, api_version):

        portfolios = defaultdict(dict)

        rows = self.db.get_portfolios_tickers_with_prices(Landings.TOP5)
        date_options = {}

        for do_row in self.db.get_date_options(trade_date=datetime.today(), isin_list=[row.isin for row in rows]):
            date_options[do_row.isin] = do_row

        for row in rows:
            portfolio_id = row.id
            portfolios[portfolio_id]['id'] = portfolio_id
            portfolios[portfolio_id]['name'] = row.name

            if "structure" not in portfolios[portfolio_id]:
                portfolios[portfolio_id]['structure'] = []

            target = row.target_price or 0
            last_price = row.close or 0
            sec_code = row.ticker if row.currency == Currencies.USD.value else row.secur_code

            if row.currency == Currencies.USD.value and row.ticker_type != TickerTypes.BONDS:
                class_code = DEFAULT_USA_SECTION
            else:
                class_code = row.classcode

            if row.ticker_type == TickerTypes.BONDS and row.offer_date is not None:
                end_date = row.offer_date.strftime('%Y-%m-%d')
            else:
                end_date = None

            if row.ticker_type == TickerTypes.BONDS:
                ticker_do = date_options.get(row.isin)
                if ticker_do:
                    lot_costs = ticker_do.currentFacevalue * last_price / 100 + ticker_do.accruedInterest
                    accrued_interest = ticker_do.accruedInterest
                else:
                    lot_costs = 0
                    accrued_interest = None
            else:
                lot_costs = row.shares_in_lot * last_price
                accrued_interest = None

            data_item = {
                'title': row.title,
                'secur_code': sec_code,
                'classcode': class_code,
                'isin': row.isin,
                'currency': row.currency,
                'ticker_type': row.ticker_type,
                'invest_period': row.invest_period,
                'invest_period_step': row.invest_period_step,
                'invest_period_end_date': end_date,
                'video': row.video,
                'full_description': row.full_description,
                'short_description': row.short_description,
                'target_price': target,
                'industry': row.industry,
                'issuer': row.issuer,
                'last_price': last_price,
                'expected_income': self.calc_expected_income(target, last_price),
                'ytm': row.ytm,
                'has_offer': row.has_offer,
                'count_in_lot': row.shares_in_lot,
                'costs': lot_costs,
                'acc_int': accrued_interest,
            }

            if api_version is None:
                del data_item['video']
                del data_item['count_in_lot']
                del data_item['costs']
                del data_item['acc_int']

            portfolios[portfolio_id]['structure'].append(data_item)

        return [self.sorted_structure(v) for k, v in portfolios.items()]
