from pytest import fixture
from time import time
from unittest.mock import patch, MagicMock
from app.settings import APP_URL_PREFIX, KEY_CLOAK_BROKER_USER
from app.infrastructure.rest.tools import ApiErrors


class TestPnlRoute:

    @fixture(autouse=True)
    def _get_context(self, client):
        self.client = client

    @patch('app.infrastructure.rest.tools.KeyCloakRealm')
    @patch('app.infrastructure.rest.routes.pnl.SessionContext')
    def test_pnl_is_authorized(self, test_db, test_keycloak, test_login, app_client):
        mo = MagicMock()
        mo.get_client_stat.return_value = {}
        mo.get_or_create_client.return_value = (1, app_client)
        test_db.return_value.__enter__.return_value = mo

        test_keycloak.get_verified_token.return_value = {'exp': time() + 1000, 'k_login': KEY_CLOAK_BROKER_USER}

        resp = self.client.get(f'{APP_URL_PREFIX}/pnl/{test_login}', headers=[('X_JWT', 'token')])
        assert resp.status_code == 200

    @patch('app.infrastructure.rest.routes.pnl.SessionContext')
    def test_pnl_is_not_authorized(self, test_login):
        resp = self.client.get(f'{APP_URL_PREFIX}/pnl/{test_login}').json
        assert resp['error']['message'] == ApiErrors.UNAUTHORIZED
