import pytest
from app.domain.usecases.api.stats_show import StatsShowUseCase
from app.domain.constants import SyncStatus
from app.domain.constants import TabNames, DividendTabNames


@pytest.fixture
def tab_name(name_of_tab):
    """ Return tab name from passed params """
    return name_of_tab


@pytest.fixture
def div_mode(tab_name_for_dividends):
    """ Return tab name from passed params """
    return tab_name_for_dividends


class TestPortfolioAddCashUseCaseMain:

    @pytest.fixture(autouse=True)
    def _get_context(self, db_mock, portfolio_id, new_asset):
        db_mock.get_current_state.return_value = dict(cash=0, cash_deposit=0, shares=0,
                                                      synchronized=SyncStatus.SYNCHRONIZED, accordance=0,
                                                      start_asset=new_asset, total_divs=0, commission_size=0)

        db_mock.get_trades.return_value = [{'id': 1}, {'id': 2}]

        self._use_case = StatsShowUseCase(login='owner',
                                          portfolio_id=portfolio_id,
                                          portfolio_dao=db_mock,
                                          )

    @pytest.mark.parametrize('tab_name, new_asset',
                             [(t, 10000) for t in TabNames.list()])
    def test_tab_name_has_data(self, portfolio_id, tab_name, new_asset):
        data = self._use_case.execute(tab_name=tab_name,
                                      after=None,
                                      div_mode=None,
                                      portfolio_id=portfolio_id,
                                      )

        assert 'items' in data['data']

    @pytest.mark.parametrize('div_mode, new_asset', [(DividendTabNames.RECEIVED, 10000),
                                                     (DividendTabNames.EXPECTED, 10000)])
    def test_dividends_mode_is_valid(self, portfolio_id, div_mode, new_asset):
        data = self._use_case.execute(tab_name=TabNames.DIVIDENDS,
                                      after=None,
                                      div_mode=div_mode,
                                      portfolio_id=portfolio_id,
                                      )

        assert 'items' in data['data']

    @pytest.mark.parametrize('new_asset', [10000])
    def test_paging_is_ok(self, portfolio_id, new_asset):
        data = self._use_case.execute(tab_name=TabNames.TRADES,
                                      after=1,
                                      div_mode=None,
                                      portfolio_id=portfolio_id,
                                      )

        assert data['data']['hasMore'] is False

    @pytest.mark.parametrize('new_asset', [0])
    def test_zero_position_has_zero_accordance(self, portfolio_id, new_asset):
        data = self._use_case.execute(tab_name=TabNames.POSITIONS,
                                      after=None,
                                      div_mode=None,
                                      portfolio_id=portfolio_id,
                                      )

        assert 'items' in data['data']
