from copy import deepcopy
from pathlib import Path
from datetime import datetime
from flask_restplus import Resource

from app import settings
from app.config import logger

from app.infrastructure.integration.proxy_fix.orders import send_market_orders
from app.infrastructure.integration.market_data.quotes_service import Worker
from app.infrastructure.tools import Realms, KeyCloak, http_request, http_request_raw
from app.infrastructure.rest.tools import api_response, SIDE_RUS, TYPES_RUS, get_login_from_token
from app.infrastructure.rest.schemas import TradeDto, parse_get_params
from app.infrastructure.db.domain_dao import SessionContext

from app.domain.usecases.api.trade_show import TradeShowUseCase
from app.domain.usecases.api.trade_confirm import TradeConfirmUseCase
from app.domain.responses import ErrorResponse

APP_DIR = Path(Path(__file__).parent.parent.parent.parent)

api = TradeDto.trade_api
_trade = TradeDto.trade


def get_prices(tickers, k_login, **kwargs):
    start_time = datetime.now()
    w = Worker()
    prices = w.get_ticker_price(tickers)

    exec_time = datetime.now() - start_time
    logger.info(f'int {k_login}: express api exec time : {exec_time}')
    return prices


def get_operation_data(k_login, portfolio_id, **kwargs):
    with SessionContext() as db:

        # Ищем активные пакетки
        use_case = TradeShowUseCase(portfolio_id, k_login, db)

        # Запрашиваем текущие цены c курсом доллара
        prices = get_prices(use_case.tickers, k_login, **kwargs)

        # Формируем итоговые данные с учетом цен
        return use_case.execute(prices)


@api.route('/<int:portfolio_id>')
class TradePersonal(Resource):

    @get_login_from_token
    def get(self, portfolio_id=None, k_login=None, **kwargs):
        """ Данные по пакетной заявке """

        logger.info(f'get_trades: portfolio_id: {portfolio_id}')

        # Если portfolio_id нет в location - берем из query_string
        if portfolio_id is None:
            portfolio_id = parse_get_params('portfolioId')

        # Запрашиваем данные пакетки с ценами
        operation_data = get_operation_data(k_login, portfolio_id, **kwargs)

        return api_response(data=operation_data)

    @api.expect(_trade)
    @get_login_from_token
    def post(self, portfolio_id, **kwargs):
        """ Отправить пакетку на биржу """

        # Читаем входные параметры
        login = kwargs.get('k_login')

        # Если portfolio_id нет в location - берем из query_string
        if portfolio_id is None:
            portfolio_id = parse_get_params('portfolioId')

        # Открываем сессию БД
        with SessionContext() as db:

            # Читаем данные из БД по клиенту
            use_case = TradeConfirmUseCase(portfolio_id=portfolio_id,
                                           login=login,
                                           portfolio_dao=db,
                                           payload=api.payload,
                                           )

            # Формируем список одиночных заявок в рамках пакетки
            uc_response = use_case.execute()

            if isinstance(uc_response, ErrorResponse):
                return api_response(error=uc_response.error, error_code=403)

            # выполняем отправку на биржу
            exchange_resp = send_market_orders(uc_response.data)

            # Проставляем заявкам статус отправки
            use_case.handle_response(exchange_resp)

        return api_response(data=exchange_resp['data'], error=exchange_resp['error'])


@api.route('/get_document/<int:portfolio_id>')
class GetPdfUrl(Resource):
    @get_login_from_token
    def get(self, portfolio_id, k_login, **kwargs):
        """ Получить ссылку на документ """

        # Запрашиваем данные пакетки с ценами
        trade = get_operation_data(k_login, portfolio_id, **kwargs)

        # Формируем массив рекомендаций
        orders = deepcopy(trade['sell']['items'])
        orders.extend(trade['buy']['items'])

        # Формируем параметры отчета
        values = {
            'client': {'value': trade['client']},
            'agreementNumber': {'value': trade['agreementNumber']},
            'tradingCode': {'value': trade['tradingCode']},
            'client_profile': {'value': trade['client_profile']},
            'profile_fill_date': {'value': trade['profile_fill_date']},
            'contract_date': {'value': trade['profile_fill_date']},
            'app_version': {'value': settings.APP_VERSION},
            'accept_date': {'value': trade['date']},
            'num': {'value': trade['num']},
            'bid_time': {'value': trade['bid_time']},
            'RecTable': {'value': {
                'columns': ['n', 'code', 'isin', 'title', 'short_description', 'currency', 'price',
                            'side_rus', 'tradeOrganization', 'sec_code', 'type', 'qty'],

                'types': ['integer', 'string', 'string', 'string', 'string', 'string', 'double',
                          'string', 'string', 'string', 'string', 'integer'],
                'rows': [[ind + 1,
                          i['code'],
                          i['isin'],
                          i['title'],
                          i['short_description'],
                          i['currency'],
                          i['price'],
                          SIDE_RUS[i['side']],
                          i['tradeOrganization'],
                          i['ticker'],
                          TYPES_RUS[i['type']],
                          i['sharesCount']
                          ] for ind, i in enumerate(orders)]
            }, 'type': 'table'}
        }

        # Формируем запрос в сервис печати
        # Exception будет перехвачен в http_exception_handler
        resp = http_request_raw(
            settings.REPORT_SERVICE_URL,
            'POST',
            headers=KeyCloak(Realms.PERSEUS).get_auth_header(),
            json={
                'fileExtension': 'pdf',
                'templates': [
                    {
                        'code': 'rais_recommendation_v5',
                        'owner': 'SVC-MicroSRV-ef-DIP',
                        'values': values,
                    }
                ]
            }
        )

        # сохраняем ответ на файловую систему
        filename = f"{trade['num']}.pdf"
        with open(Path(settings.DOCUMENT_ROOT, filename), 'wb') as f:
            f.write(resp.content)

        # отдаем ответ со ссылкой на документ
        file_link = f'{settings.DOCUMENT_URL}/{filename}'
        return api_response(data={'url': file_link})


@api.route('/update_document_template')
class TemplateCreator(Resource):
    def post(self):
        """
            Сервис печати - https://jira.bcs.ru:4464/confluence/pages/viewpage.action?pageId=122139261
            Swagger - https://printservice.okd.t-global.bcs/swagger-ui.html
            TODO: метод вызывался вручную, но в будущем прикутим его в админку
        """

        return api_response(
            data=http_request(
                settings.REPORT_SERVICE_TEMPLATE_URL,
                'POST',
                headers=KeyCloak(Realms.PERSEUS).get_auth_header(),
                json={
                    'code': 'rais_recommendation_v5',
                    'fileBase64': api.payload.get('encoded_document'),
                    'name': 'Индивидуальная инвестиционная рекомендация',
                    'owner': settings.KEY_CLOAK_PERSEUS_USER,
                    'type': 'docx'
                }
            )
        )
