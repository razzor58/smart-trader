import sys
from datetime import datetime
from dateutil import parser as dparser
from enum import Enum
from kafka import KafkaConsumer

from app.domain.constants import DATETIME_FORMAT, Limits
from app.infrastructure.db.meta.models import QuikTrades, DepoLimits, MoneyLimits, FutureHoldings
from app.infrastructure.db.meta.tools import update_by_dict, filter_column_names
from app.infrastructure.db.domain_dao import SessionContext
from app import settings
from app.config import logger

TAG_PREFIX = f"{{{settings.KAFKA_XML_NAMESPACE}}}"
NS = {'ms': settings.KAFKA_XML_NAMESPACE}
IGNORE_FIELDS = ['id', 'create_date']


class MessageType(Enum):
    QUIK_TRADE = 'quik_trade'
    MONEY_LIMITS = 'money_limits'
    DEPO_LIMITS = 'depo_limits'
    FUTURES_HOLDINGS = 'futures_holding'


MESSAGE_CONFIG = {
    MessageType.QUIK_TRADE.value: {
        'model': QuikTrades,
        'fields': filter_column_names(QuikTrades, IGNORE_FIELDS),
        'topic': settings.KAFKA_TRADES_TOPIC
    },
    MessageType.MONEY_LIMITS.value: {
        'model': MoneyLimits,
        'fields': filter_column_names(MoneyLimits, IGNORE_FIELDS),
        'topic': settings.KAFKA_MONEY_LIMITS_TOPIC
    },
    MessageType.DEPO_LIMITS.value: {
        'model': DepoLimits,
        'fields': filter_column_names(DepoLimits, IGNORE_FIELDS),
        'topic': settings.KAFKA_DEPO_LIMITS_TOPIC
    },
    MessageType.FUTURES_HOLDINGS.value: {
        'model': FutureHoldings,
        'fields': filter_column_names(FutureHoldings, IGNORE_FIELDS),
        'topic': settings.KAFKA_FUTURE_HOLDINGS_TOPIC
    }
}

db_session = SessionContext()


def get_consumer(topic, service_param):
    return KafkaConsumer(topic,
                         bootstrap_servers=settings.KAFKA_SERVERS,
                         security_protocol='SASL_PLAINTEXT',
                         api_version=(0, 10),
                         sasl_mechanism='SCRAM-SHA-256',
                         sasl_plain_username=settings.KAFKA_USERNAME,
                         sasl_plain_password=settings.KAFKA_PASSWORD,
                         auto_offset_reset='earliest',
                         enable_auto_commit=True,
                         auto_commit_interval_ms=settings.KAFKA_AUTO_COMMIT_INTERVAL_MS,
                         max_poll_records=settings.KAFKA_MAX_POLL_RECORDS,
                         group_id=f'{settings.KAFKA_GROUP_NAME}_{service_param}')


def get_clients_list():
    """ Создаем словарь со списком клиентов """
    res = {}
    clients_rows = db_session.get_all_clients()
    clients = set([client.client_code for client in clients_rows])
    # Клиенты тарифа
    for client_code in clients:

        if client_code:

            # коды клиентов для фьчей
            if client_code[0:2] == '82':
                res['SPBFUT' + client_code[2:]] = 1
            else:
                res[client_code] = 1

    # Добавляем тестовые счета
    for user in settings.PROXY_FIX_TEST_USERS:
        res[user] = 1

    return res


def save_messages(message_name, row_list):
    model = MESSAGE_CONFIG[message_name]['model']
    for row in row_list:
        obj = model(**row)
        db_session.add_row(obj)


def update_limits(limit_name: str, row: dict):
    if row.get('limit_kind') != Limits.T2 or 'current_bal' not in row:
        return

    if limit_name == MessageType.DEPO_LIMITS.value:
        unique_code_field = 'sec_code'
    elif limit_name == MessageType.MONEY_LIMITS.value:
        unique_code_field = 'curr_code'
    else:
        raise Exception('unknown limit')

    client_code = row.get('client_code')
    unique_code = row.get(unique_code_field)

    if not client_code or not unique_code:
        raise Exception('unable to find client')

    model = MESSAGE_CONFIG[limit_name]['model']
    q_filter = {
        'client_code': client_code,
        unique_code_field: unique_code,
    }
    limit = db_session.session.query(model).filter_by(**q_filter).first()

    if not limit:
        db_session.session.add(model(**row))
    else:
        if row.get('current_bal') == limit.current_bal:
            return
        update_by_dict(limit, row)

    db_session.session_commit()


def get_dict_from_value_list(message_name, message_data):
    field_list = MESSAGE_CONFIG[message_name]['fields']
    zip_obj = zip(field_list, message_data)

    trade_dict = dict(zip_obj)
    return trade_dict


def parse_xml_root(root):
    """ Парсим тело сообщение в набор полей """
    if root.find('ms:system_block', NS):
        item = []
        for child in root.find('ms:system_block', NS).getchildren():
            name = child.tag.replace(TAG_PREFIX, '')
            if name == 'event_time':
                date_str = dparser.parse(child.text)
                item.append(date_str.strftime(DATETIME_FORMAT))
            if name == 'document_id':
                item.append(child.text)
    else:
        item = [None, None]

    item.extend([child.text for child in root.find('ms:body', NS).getchildren()])

    return tuple(item)


def get_client_code(root, client_tag):
    """ Парсим поле client_code """

    client_code = root.find('ms:body', NS).find('ms:{}'.format(client_tag), NS).text

    return client_code


def do_exit_on_time():
    """ Выходим из цикла Перезапускаем  """
    if datetime.now().hour == settings.KAFKA_LOGS_TIME_TO_RESTART:
        logger.info("Exit on SCHEDULE. Systemd should restart ")
        sys.exit()
