from datetime import datetime
from requests.exceptions import HTTPError

from app.config import logger
from app.domain.constants import PositionTypes
from app.settings import BLACK_BOX_API_KEY, BLACK_SIGNAL_LIMIT
from app.infrastructure.tools import http_request, normalize_json
from app.infrastructure.db.infra_dao import SessionContext


BLACK_BOX_URL = 'http://sblackbox.ru/api/analyzed-signals'
LONG = PositionTypes.LONG.value
SHORT = PositionTypes.SHORT.value


def process():
    signals_created = 0

    with SessionContext() as db_session:
        ticker_list = db_session.get_ticker_list()
        for ticker in ticker_list:
            short_code = ticker['ticker']
            try:
                data = http_request(**{
                    'base_url': BLACK_BOX_URL,
                    'method': 'GET',
                    'params': {
                        'key': BLACK_BOX_API_KEY,
                        'limit': BLACK_SIGNAL_LIMIT,
                        'short_code': short_code
                    },
                })
            except HTTPError as exp:
                if exp.response.status_code == 404:
                    logger.info(f'signals for short_code={short_code} not found')
                    continue
                else:
                    raise exp

            signal = data.get('data')
            if signal:
                item = normalize_json(signal[0])
                item.update({
                    'short_code': short_code,
                    'epoch_time': item['time'],
                    'position_type': LONG if LONG in item['operation'] else SHORT,
                    'time': datetime.fromtimestamp(item['time'])
                })
                created = db_session.update_signals(item)
                signals_created += created

    logger.info(f'signals_results: total tickers={len(ticker_list)}, new signals={signals_created}')
