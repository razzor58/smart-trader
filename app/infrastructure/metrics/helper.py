from prometheus_flask_exporter import PrometheusMetrics
from prometheus_client import multiprocess, CollectorRegistry


class MultiMetrics(PrometheusMetrics):
    def __init__(self, app, path='/metrics',
                 export_defaults=True, defaults_prefix='flask',
                 group_by='path', buckets=None, static_labels=None,
                 excluded_paths=None, **kwargs):

        registry = CollectorRegistry()
        multiprocess.MultiProcessCollector(registry)

        super(MultiMetrics, self).__init__(app, path=path,
                                           export_defaults=export_defaults, defaults_prefix=defaults_prefix,
                                           group_by=group_by, buckets=buckets, static_labels=static_labels,
                                           excluded_paths=excluded_paths, registry=registry, **kwargs)


def get_flask_metrics_class(is_multiprocess):
    if is_multiprocess:
        return MultiMetrics

    return PrometheusMetrics
