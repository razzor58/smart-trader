"""one limit per client

Revision ID: 6e561aab7e54
Revises: df62254d1514
Create Date: 2020-06-05 15:28:59.413599

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = '6e561aab7e54'
down_revision = 'df62254d1514'
branch_labels = None
depends_on = None


def upgrade():
    op.execute("DELETE FROM depo_limits WHERE limit_kind!='T2';")
    op.execute("DELETE FROM money_limits WHERE limit_kind!='T2';")
    op.execute(
        'DELETE FROM depo_limits WHERE id NOT IN (SELECT DISTINCT ON (client_code, sec_code) id FROM depo_limits ORDER BY client_code, sec_code, event_time DESC);'
    )
    op.execute(
        'DELETE FROM money_limits WHERE id NOT IN (SELECT DISTINCT ON (client_code, curr_code) id FROM money_limits ORDER BY client_code, curr_code, event_time DESC);'
    )


def downgrade():
    pass
