from __future__ import print_function
import os
import grpc
from grpc._channel import _Rendezvous as GRPCException

from app.infrastructure.integration.proxy_fix import GrpcService_pb2_grpc, GrpcService_pb2, \
    header_manipulator_client_interceptor
from app.config import logger as log
from app import settings


log.info("==Load order client")
log.info("Connection details:")
log.info('SERVICE_URL: {}'.format(settings.PROXY_FIX_SERVICE_URL))
log.info('PROXY: {}'.format(os.environ.get('https_proxy', 'no set')))


def send_market_orders(orders_to_send, is_cancel=False):
    """
    Отправка рыночной заявки в сервис proxyFIX
    :param orders_to_send: Словарь с данными для отправки на заявок биржу.
    :return:
    """
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    res = dict(data={'details': {}}, error=None)
    try:

        # Параметры подключения
        header_adder_interceptor = header_manipulator_client_interceptor.header_adder_interceptor(
            'client_token', settings.PROXY_FIX_CLIENT_TOKEN
        )

        # Ставим коннект
        with grpc.insecure_channel(settings.PROXY_FIX_SERVICE_URL) as channel:
            intercept_channel = grpc.intercept_channel(channel,
                                                       header_adder_interceptor)

            # Вызов микросервисов
            stub = GrpcService_pb2_grpc.ProxyFixServiceStub(intercept_channel)

            for order in orders_to_send:
                # Отправляем заявки по одной
                log.info("send: {}".format(order))

                # В зависимости от типа заявки - разные вызовы сервера
                if is_cancel:
                    response = stub.CancelOrder(GrpcService_pb2.CancelOrderDataRequest(**order))
                elif order['ordType'] == '2':
                    response = stub.CreateNewLimitOrder(GrpcService_pb2.NewLimitOrderDataRequest(**order))
                else:
                    response = stub.CreateMarketOrder(GrpcService_pb2.NewMarketOrderDataRequest(**order))

                log.info("resp: " + response.result)

                # пишем ответ по каждой заявке
                res['data']['details'][order['clOrdId']] = {
                    'send_status': response.result,
                    'client_nominal': order.get('marketLot', 0)
                }

    except GRPCException as e:
        res['error'] = e.details()
        log.error(e)

    return res
