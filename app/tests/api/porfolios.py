import unittest
import requests as r
from . import PORTFOLIO_ID, SERVICE_URL


class TestPorfolioList(unittest.TestCase):
    """
      Метод списка портфелей для главной странице торгового кабинета

      {
            "portfolio_id": 28,
            "name": "US+RU: Умный портфель All",
            "recommended_cash": 2000000,
            "income_avg": 41.06,
            "currency": "USDRUR",
            "subscribed": 0,
            "accordance": 0,
            "asset": 0,
            "synchronized": null
        },


    """
    resp = None

    def setUp(self):
        method = '/portfolios/client'
        url = SERVICE_URL + method
        self.resp = r.get(url).json()

    def test_portfolios_resp_is_valid(self):
        test_result = isinstance(self.resp, dict)
        self.assertTrue(test_result)

    def test_portfolios_resp_not_empty(self):
        p_list = self.resp.get('data', [])
        test_result = len(p_list) > 0
        self.assertTrue(test_result)

    def test_portfolios_resp_validate_resp(self):
        all_fields_is_valid = True

        for p in self.resp['data']:
            if not isinstance(p['portfolio_id'], int):
                all_fields_is_valid = False

            if not isinstance(p['name'], str):
                all_fields_is_valid = False

            if not isinstance(p['recommended_cash'], int):
                all_fields_is_valid = False

            if not isinstance(p['income_avg'], float):
                all_fields_is_valid = False

            if not isinstance(p['currency'], str):
                all_fields_is_valid = False

            if not isinstance(p['subscribed'], int):
                all_fields_is_valid = False

            if not isinstance(p['accordance'], int):
                all_fields_is_valid = False

            if not isinstance(p['asset'], int):
                all_fields_is_valid = False

            if len(p['currency']) > 6:
                all_fields_is_valid = False

            if 'synchronized' not in p:
                all_fields_is_valid = False

        self.assertFalse(all_fields_is_valid)


class TestPorfolioListClient(unittest.TestCase):
    """
      Метод списка портфелей для главной странице торгового кабинета

      {
            "portfolio_id": 28,
            "name": "US+RU: Умный портфель All",
            "recommended_cash": 2000000,
            "income_avg": 41.06,
            "currency": "USDRUR",
            "subscribed": 0,
            "accordance": 0,
            "asset": 0,
            "synchronized": null
        },


    """
    resp = None

    def setUp(self):
        method = '/portfolios/client/{}'.format(PORTFOLIO_ID)
        url = SERVICE_URL + method
        self.resp = r.get(url).json()

    def test_portfolios_resp_is_valid(self):
        test_result = isinstance(self.resp, dict)
        self.assertTrue(test_result)

    def test_portfolios_resp_is_ok(self):
        self.assertTrue(self.resp['success'])

    def test_portfolio_resp_validate_resp(self):
        all_fields_is_valid = True
        p = self.resp['data']

        if not isinstance(p['portfolio_id'], int):
            all_fields_is_valid = False

        if not isinstance(p['name'], str):
            all_fields_is_valid = False

        if not isinstance(p['recommended_cash'], int):
            all_fields_is_valid = False

        if not isinstance(p['income_avg'], float):
            all_fields_is_valid = False

        if not isinstance(p['currency'], str):
            all_fields_is_valid = False

        if not isinstance(p['subscribed'], int):
            all_fields_is_valid = False

        if not isinstance(p['accordance'], int):
            all_fields_is_valid = False

        if not isinstance(p['asset'], int):
            all_fields_is_valid = False

        if len(p['currency']) > 6:
            all_fields_is_valid = False

        if 'synchronized' not in p:
            all_fields_is_valid = False

        self.assertFalse(all_fields_is_valid)


if __name__ == '__main__':
    unittest.main()
