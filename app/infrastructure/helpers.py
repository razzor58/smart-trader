import uuid
import typing
from requests import HTTPError

from app.settings import PROXY_CATALOG_URL, PROFILE_SERVICE_URL
from app.config import logger
from app.infrastructure.db.infra_dao import SessionContext
from app.infrastructure.tools import KeyCloak, Realms, http_request


def get_clients_rest_catalog(logins: typing.List[str]) -> typing.List[dict]:
    try:
        response = http_request(
            **{
                'base_url': PROXY_CATALOG_URL,
                'method': 'POST',
                'params': {
                    'reqId': str(uuid.uuid4()).lower()
                },
                'json': {
                    'klogins': logins,
                },
                'headers': KeyCloak(Realms.BROKER).get_auth_header()
            })
    except HTTPError as e:
        logger.error(e)
        return []

    return response.get('data', {'clients': []}).get('clients')


def store_clients_from_catalog(logins: typing.List[str], db_session: SessionContext) -> int:
    total_created = 0

    for client in get_clients_rest_catalog(logins):
        created = db_session.create_or_update_clients_data_if_not_exists(client)
        if created:
            total_created += 1
    return total_created


def store_single_client(login: str) -> int:
    with SessionContext() as db_session:
        return store_clients_from_catalog([login], db_session)


def get_profile(client_id: str, subject_id: str, auth_header: str) -> typing.List[dict]:
    try:
        response = http_request(
            **{
                'base_url': PROFILE_SERVICE_URL,
                'method': 'POST',
                'params': None,
                'client_id': client_id,
                'json': {
                    'language': 'ru',
                    'subject_id': subject_id,
                },
                'headers': auth_header
            })
    except HTTPError as e:
        logger.error(e)
        return []

    return response.get('profiles', [])


def update_client_risk_profile(client_id: str, profiles: typing.List[dict], db_session: SessionContext):
    for p in profiles:
        profiles_lst = [pr for pr in p['profile'] if pr['type'] == 'risk']
        if profiles_lst:
            profile_id = profiles_lst[0]['id']
            row = {
                'client_id': client_id,
                'questionary_id': p['questionary_id'],
                'template_id': p['template_id'],
                'filling_datetime': p['filling_datetime'],
                'confirmed': p['confirmed'],
                'isActual': p['isActual'],
                'profile_id': profile_id,
                'profile_name': profiles_lst[0]['name']
            }
            if p['confirmed'] and p['isActual']:
                db_session.delete_profiles({'questionary_id': p['questionary_id']})
                db_session.add_profile(row)
                db_session.update_client_risk_profile(client_id, profile_id)


def update_single_profile(client_id: str, master_id: str):
    with SessionContext() as db_session:
        update_client_risk_profile(
            client_id,
            get_profile(client_id, master_id, KeyCloak(Realms.PERSEUS).get_auth_header()),
            db_session
            )
    logger.info(f'Profile for client {client_id} updated')
