import pytest
from copy import deepcopy
from app.domain.usecases.api.portfolio_add_cash import PortfolioAddCashUseCase
from app.domain.constants import CoreError


class TestPortfolioAddCashUseCaseMain:

    @pytest.fixture(autouse=True)
    def _get_context(self, db_mock, portfolio_id, new_asset, client_risk_level):
        db_mock.get_client_risk_profile.return_value = client_risk_level
        self._use_case = PortfolioAddCashUseCase(login='owner',
                                                 portfolio_id=portfolio_id,
                                                 portfolio_dao=db_mock,
                                                 payload={
                                                     'new_asset_size': new_asset,
                                                     'deposit': 0
                                                 })
        self._portfolio_before = deepcopy(self._use_case.portfolio)
        self._response = self._use_case.execute()
        self._portfolio_after = self._use_case.portfolio

    @pytest.mark.parametrize('new_asset, risk_level', [(1000, 1)])
    def test_cash_is_change(self, new_asset, risk_level):
        assert self._portfolio_after.start_asset == new_asset

    @pytest.mark.parametrize('new_asset, risk_level', [(0, 1)])
    def test_accordance_is_zero_when_full_exit(self, new_asset, risk_level):
        assert self._portfolio_after.accordance == 0

    @pytest.mark.parametrize('new_asset, risk_level', [(1000, -1)])
    def test_risk_level_is_checked(self, new_asset, risk_level):
        assert self._response.error == CoreError.RISK_PROFILE_INCOMPATIBLE
