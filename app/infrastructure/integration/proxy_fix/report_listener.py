from __future__ import print_function
from datetime import datetime
import os
import sys

import grpc
from grpc._channel import _Rendezvous as GRPCException
from google.protobuf.json_format import MessageToDict
from google.protobuf import wrappers_pb2 as wrappers

# App modules
from app.infrastructure.integration.proxy_fix import (
    header_manipulator_client_interceptor as ci,
    GrpcService_pb2_grpc,
    GrpcExec_pb2,
)

import app.infrastructure.db.domain_dao as db
from app.config import logger as log
from app import settings

parent_folder = os.path.join(os.path.abspath(os.path.dirname(__file__)), '../')
sys.path.append(parent_folder)

REPORT_INTERNAL_NUM_FIELD = 'internalNum'
REPORT_ORDER_ID_FIELD = 'clOrdId'
REPORT_ORDER_STATUS_FIELD = 'ordStatus'
REPORT_ORDER_TEXT_FIELD = 'text'


def process_message(msg, db_session):
    report_details = msg.get('execRepForBasicOrder', {}).get('execRepDetailNormalOrder')
    int_num = msg.get(REPORT_INTERNAL_NUM_FIELD)

    if not report_details or not int_num:
        log.error('unexpected message')
        log.info(msg)
        return

    report_details[REPORT_INTERNAL_NUM_FIELD] = int_num

    # Сохраняем новые отчеты в БД (возвращаем True - если отчет с таким OrderId ранее не сохранялся)
    if db_session.save_online_report(report_details):
        # Меняем состояние заявки
        db_session.update_bids_by_reporter(
            report_details[REPORT_ORDER_ID_FIELD], **{
                'exec_status': report_details.get(REPORT_ORDER_STATUS_FIELD),
                'trade_num': report_details.get('tradeNum'),
                'order_num': report_details.get('orderNum'),
                'ax_price': report_details.get('avgPx'),
                'ex_value': report_details.get('exValue'),
                'cumQty': report_details.get('cumQty') or 0,
                'side': report_details.get('side'),
                'leavesQty': report_details.get('leavesQty') or 0,
                'exec_message': report_details.get(REPORT_ORDER_TEXT_FIELD, 'OK'),
            })
        db_session.session_commit()
        log.debug(f'report for bid_id={report_details.get(REPORT_ORDER_ID_FIELD)} '
                  f'status={report_details.get(REPORT_ORDER_STATUS_FIELD)} '
                  f'message={report_details.get(REPORT_ORDER_TEXT_FIELD)}')
    else:
        log.debug(f'report {report_details[REPORT_ORDER_ID_FIELD]} is already processed')


def run():
    log.info('== Start listener ==')
    log.info(f'SERVICE_URL: {settings.PROXY_FIX_SERVICE_URL}')

    #  Подключаемся к БД
    db_session = db.get_session()

    next_report_id = db_session.get_last_report_id() + 1
    log.info(f'subscribing for next report_id: {next_report_id}')
    try:
        # Вызов микросервисов
        with grpc.insecure_channel(settings.PROXY_FIX_SERVICE_URL) as channel:

            # Для передачи токена авторизации в заголовке каждого запроса
            header_adder_interceptor = ci.header_adder_interceptor('client_token', settings.PROXY_FIX_CLIENT_TOKEN)
            intercept_channel = grpc.intercept_channel(channel, header_adder_interceptor)

            stub = GrpcService_pb2_grpc.ProxyFixServiceStub(intercept_channel)

            report_subscription = stub.SubscrReports(
                GrpcExec_pb2.ReportSubscriptionRequest(internalNum=wrappers.Int64Value(value=next_report_id)))

            # Читаем сообщения из подписки
            for resp in report_subscription:
                msg = MessageToDict(resp)
                process_message(msg, db_session)

    except GRPCException as e:
        log.info(f'time: {datetime.now()}')
        log.error(e)
        log.error('Exit due to error.')
        sys.exit()
    finally:
        # Закрываем сессию
        db_session.session_close()
