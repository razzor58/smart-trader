from datetime import datetime

from app.domain.usecases import PortfolioUseCase
from app.domain.interfaces import IPortfolioDAO
from app.domain.tools import calc_limit_price, define_exchange_destination
from app.domain.constants import CoreError, BidOperation, Currencies, get_default_account
from app.domain.responses import ErrorResponse, SuccessResponse
from app import settings


class TradeConfirmUseCase(PortfolioUseCase):

    def __init__(self,
                 login: str,
                 portfolio_id: int,
                 portfolio_dao: IPortfolioDAO,
                 payload: dict):
        super().__init__(portfolio_id, login, portfolio_dao)
        self.payload = payload

    def get_bids_for_send(self, **kwargs):
        """ Возвращаем заявки для отправки на биржу"""
        return self.db.get_bids_for_send(**kwargs)

    def define_accounts_from_depo(self, **kwargs):
        """ по какому счету выставить заявки"""

        bids = kwargs.get('bids')
        depo_limits = self.db.define_accounts_from_depo(client_id=self.client_id)
        ticker_accounts = {}
        for ticker, bid in bids.items():
            if depo_limits.get(ticker) is not None:
                ticker_accounts[ticker] = depo_limits[ticker]['trd_acc']
            else:
                ticker_accounts[ticker] = get_default_account(bid['type'], bid['currency'])

        return ticker_accounts

    def is_trade_allowed(self):
        return self.db.check_trade_allowed(self.client_id)

    def set_sync_status(self, status):
        if self.portfolio_id and self.client_id:
            self.db.update_clients_portfolios(
                {
                    "client_id": self.client_id,
                    "portfolio_id": self.portfolio_id,
                },
                {
                    "synchronized": status
                })

    def execute(self, execute_time=None):
        """Отправить пакетку на биржу"""

        # Читаем входные параметры
        moment = execute_time or datetime.now()
        trade_end_time = moment.replace(hour=settings.TRADING_CLOSE_HOUR,
                                        minute=settings.TRADING_CLOSE_MINUTES,
                                        second=settings.TRADING_CLOSE_SECONDS)

        bids_from_api = self.payload['data']

        # Читаем данные из БД по клиенту
        bids = self.get_bids_for_send(bid_num=self.payload['num'])
        ticker_accounts = self.define_accounts_from_depo(bids=bids)

        # Выполняем проверки
        # Торговля активирована (чтобы случайно не отправили заявку по клиентскому счету при тестировании.
        # [adv].[clients].trading_allowed = 1
        if not self.is_trade_allowed():
            res = ErrorResponse(error=CoreError.TRADE_NOT_ALLOWED_FOR_CLIENT)

        # Проверка времени отправки
        elif moment.hour < settings.TRADING_OPEN_HOUR or moment.weekday() in (6, 7) or moment > trade_end_time:
            res = ErrorResponse(error=CoreError.TRADE_SESSION_CLOSE)

        # Если все проверки пройдены - отправляем заявку в FIX адаптер
        else:

            # для админки RAIS
            if 'buy' in bids_from_api and 'sell' in bids_from_api:
                orders_to_send = []
                orders_to_send.extend(bids_from_api['buy']['items'])
                orders_to_send.extend(bids_from_api['sell']['items'])
            # для API Express - другой формат API
            else:
                orders_to_send = bids_from_api

            orders_to_exchange = []
            for order_detail in orders_to_send:
                ticker = order_detail['ticker']
                bid = bids[ticker]

                side = BidOperation.side_is(bid['operation'], is_quik_format=True)
                bid_price = calc_limit_price(order_detail, bid, side)

                account = ticker_accounts[ticker]
                ticker_exchange = Currencies.USD000UTSTOM.value if ticker == Currencies.USDRUB.value else ticker

                # Формируем сообещения в ProxyFix
                cl_ord_id = str(bid['id'])
                order = {
                    'ordType': '2',
                    'price': bid_price,
                    'clOrdId': cl_ord_id,
                    'account': account,
                    'side': side,
                    'timeInForce': '0',
                    'clientId': bid.get('client_code'),
                    'orderQty': order_detail['sharesCount'],
                    'marketLot': order_detail['nominal'],
                    'symbol': ticker_exchange,
                    'securityId': ticker_exchange,
                    'securityIdSource': '8',
                    'exDestination': define_exchange_destination(bid['classcode'], account),
                    'text': 'RA_{}'.format(cl_ord_id)

                }
                orders_to_exchange.append(order)

            # Проставляем статус портфеля как не синхронизирован
            self.set_sync_status(status=0)

            res = SuccessResponse(data=orders_to_exchange)

        return res

    def handle_response(self, exchange_resp):
        """ Проставляем заявкам статус отправки """

        orders_data = exchange_resp['data']['details']
        now = datetime.now()
        for order_id, values in orders_data.items():
            self.db.update_bids_by_api(bid_id=order_id, client_accept_time=now, **values)
