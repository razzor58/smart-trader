import time
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from app.settings import DB_CONNECT_STRING
from app.config import logger

# Session templates
db_engine = create_engine(DB_CONNECT_STRING, pool_pre_ping=True)
Session = sessionmaker(bind=db_engine)

# Test connection
try:
    SEC_TO_SLEEP = 1
    logger.info('=' * 70)
    logger.info(f'App try connect to db in {SEC_TO_SLEEP} sec')
    time.sleep(SEC_TO_SLEEP)
    connection = db_engine.connect()
    logger.info('DB connect successful')
    connection.close()
except Exception as e:
    logger.info(e, exc_info=True)


class CommonSession:

    def __init__(self):
        self.session = Session()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_val is None:
            self.session.commit()

        self.session.close()
