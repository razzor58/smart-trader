from flask_restplus import Resource
from app.infrastructure.rest.tools import auth_app, SUCCESS_RESP
from app.infrastructure.rest.schemas import PortfolioDto
from app.infrastructure.db.domain_dao import SessionContext
from app.domain.usecases.pnl.handler import GetPnlUseCase


pnl = PortfolioDto.pnl_api
api = PortfolioDto.portfolio_api

_portfolio = PortfolioDto.portfolio


@pnl.route('/<login>')
class ClientPortfolioResultsAll(Resource):
    @auth_app
    def get(self, login, **kwargs):
        with SessionContext() as db:
            use_case = GetPnlUseCase(login=login, portfolio_dao=db)
            return use_case.get_pnl_all()


@pnl.route('/report')
class ServiceResultsAll(Resource):
    @auth_app
    def get(self, **kwargs):
        with SessionContext() as db:
            use_case = GetPnlUseCase(login=None,  portfolio_dao=db)
            return use_case.get_report()


@pnl.route('/search/<login>')
class ClientSearch(Resource):
    @auth_app
    def get(self, login, **kwargs):
        """  Поиск клиента в сервисе """
        with SessionContext() as db:
            use_case = GetPnlUseCase(login=None, portfolio_dao=db)
            return use_case.get_client(login)


@pnl.route('/<portfolio_id>', defaults={'login': None}, doc=False)
@pnl.route('/<portfolio_id>/<login>', doc=False)
class ClientPortfolioResults(Resource):
    def get(self, portfolio_id, login):
        """PNL - витрина (для АРМ Администратора)"""
        with SessionContext() as db:
            use_case = GetPnlUseCase(login=login, portfolio_dao=db, portfolio_id=portfolio_id)
            data = use_case.get_pnl()
        return data or api.abort(404, "PortfolioExt not found")

    @api.expect(PortfolioDto.pnl_days)
    def post(self, portfolio_id, login):
        """Обновить pnl по клиенту"""
        with SessionContext() as db:
            use_case = GetPnlUseCase(login=login, portfolio_dao=db)
            use_case.recalc_pnl(days_ago=api.payload.get('days_ago', 1), portfolio_id=portfolio_id)

        return SUCCESS_RESP
