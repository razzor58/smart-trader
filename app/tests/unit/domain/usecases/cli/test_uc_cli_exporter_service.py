import pytest
from datetime import datetime
from unittest.mock import patch
from app.domain.usecases.cli.exporter_service import ExporterService
from app.infrastructure.integration.quik_exporter import client


@pytest.fixture
def quik_trade(test_ticker, test_client_code):
    return {'event_time': '2020-11-21 14:00:10', 'document_id': '81fa969e-2412-4545-ad3a-9ab4d34dfffd',
            'trade_num': '3044519501', 'trade_date': '2019-11-21T00:00:00', 'operation': 'Продажа',
            'class_code': 'TQBR', 'trade_date_time': '2019-11-21T14:00:10', 'order_num': '18922687653',
            'sec_code': test_ticker, 'account': 'L01-00000F00', 'client_code': test_client_code, 'qty': '1',
            'qty_pcs': '1',
            'price': '6055', 'value': '6055', 'repo_value': '0', 'accruedint': '0', 'settle_code': 'Y2',
            'settle_date': '2019-11-25T00:00:00', 'settle_currency': 'RUR', 'exchange_code': None,
            'trades_time_ms': None, 'price2': '0', 'repo2value': '0', 'accruedint2': '0', 'repo_term': '0',
            'ts_commission': '0', 'linked_trade': '0', 'kind': 'Обычная', 'kindFlag': None, 'r_flags': '68'}


class KafkaMockMessage:

    def __init__(self):
        self.offset = 1000
        self.value = '''<?xml version="1.0" encoding="utf-8"?>
            <quik_trade xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                        xmlns="http://schemas.bcs.ru/microservices/">
                <system_block>
                    <originator>QEXPORT</originator>
                    <event_time>2019-04-10T12:58:24.9901142+03:00</event_time>
                    <document_id>749a4636-c8a7-46e5-9c37-ccf17ce1ba4f</document_id>
                    <mean_for>microservices</mean_for>
                    <event>Updated</event>
                </system_block>
                <body>
                    <trade_num>4077489837</trade_num>
                    <trade_date>2019-04-10T00:00:00</trade_date>
                    <operation>Купля</operation>
                    <class_code>SPBFUT</class_code>
                    <trade_date_time>2019-04-09T19:16:04</trade_date_time>
                    <order_num>35051050489</order_num>
                    <sec_code>RIM9</sec_code>
                    <account>SPBFUT00UUS</account>
                    <client_code>TEST003</client_code>
                    <qty>1</qty>
                    <qty_pcs>1</qty_pcs>
                    <price>122800</price>
                    <value>159457.03</value>
                    <repo_value>0</repo_value>
                    <accruedint>0</accruedint>
                    <settle_code xsi:nil="true"/>
                    <settle_date>2019-04-10T00:00:00</settle_date>
                    <settle_currency xsi:nil="true"/>
                    <exchange_code xsi:nil="true"/>
                    <trades_time_ms xsi:nil="true"/>
                    <price2>0</price2>
                    <repo2value>0</repo2value>
                    <accruedint2>0</accruedint2>
                    <repo_term>0</repo_term>
                    <ts_commission>3.5</ts_commission>
                    <linked_trade>0</linked_trade>
                    <kind>\xd0\x9e\xd0\xb1\xd1\x8b\xd1\x87\xd0\xbd\xd0\xb0\xd1\x8f</kind>
                    <kindFlag xsi:nil="true"/>
                    <r_flags>64</r_flags>
                </body>
            </quik_trade>'''.encode('utf-8')


class TestExporterServiceUseCase:

    @pytest.fixture(autouse=True)
    def _get_context(self, db_mock):
        self._use_case = ExporterService()
        self.db_session = db_mock

    def test_apply_quik_trade_from_kafka(self, quik_trade):
        self._use_case.apply_quik_trade_from_kafka(quik_trade, self.db_session)

    def test_apply_quik_limit_from_kafka_no_client(self, quik_trade):
        self.db_session.get_positions_by_client_id.return_value = None
        self._use_case.apply_quik_trade_from_kafka(quik_trade, self.db_session)

    def test_apply_quik_limit_from_kafka(self, test_client_code):
        money_limit_message = {
            'event_time': datetime.now(),
            'load_date': datetime.now().strftime('%Y-%m-%d'),
            'client_code': test_client_code,
            'curr_code': 'SUR',
            'firm_id': 0,
            'tag': 0,
            'limit_kind': 0,
            'leverage': 0,
            'locked_value_coef ': 0,
            'current_bal': 1000,
            'current_limit ': 0,
            'locked': 0,
            'open_bal': 0,
            'open_limit': 0,
            'firm_name': 0,
            'locked_margin_value': 0,
        }
        self._use_case.apply_quik_limit_from_kafka(money_limit_message, self.db_session)

    @patch('app.infrastructure.integration.quik_exporter.tools.db_session')
    @patch('app.infrastructure.integration.quik_exporter.client.db_session')
    @patch('app.infrastructure.integration.quik_exporter.client.get_consumer')
    def test_service_is_ok(self, test_consumer, test_session, test_session_tools):
        test_message = KafkaMockMessage()
        test_consumer.return_value = [test_message]
        client.main('quik_trade')
