"""add_signals_hist_table

Revision ID: ed51ae00968b
Revises: 2b7e5955972c
Create Date: 2020-07-08 16:59:26.563456

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ed51ae00968b'
down_revision = '2b7e5955972c'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('signals_history',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('create_date', sa.DateTime(), nullable=True),
    sa.Column('short_code', sa.String(length=50), nullable=True),
    sa.Column('signal', sa.String(length=11), nullable=True),
    sa.Column('operation', sa.String(length=20), nullable=True),
    sa.Column('vol', sa.Float(), nullable=True),
    sa.Column('acc_vol', sa.Float(), nullable=True),
    sa.Column('open', sa.Float(), nullable=True),
    sa.Column('close', sa.Float(), nullable=True),
    sa.Column('hi', sa.Float(), nullable=True),
    sa.Column('lo', sa.Float(), nullable=True),
    sa.Column('time', sa.DateTime(), nullable=True),
    sa.Column('volume', sa.String(length=200), nullable=True),
    sa.Column('analyzed_id', sa.Integer(), nullable=True),
    sa.Column('upload_flag', sa.Boolean(), nullable=True),
    sa.Column('deal_price', sa.Float(), nullable=True),
    sa.Column('position_type', sa.String(length=5), nullable=True),
    sa.Column('price', sa.Float(), nullable=True),
    sa.Column('epoch_time', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_signals_history_short_code'), 'signals_history', ['short_code'], unique=False)
    op.add_column('signals', sa.Column('epoch_time', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('signals', 'epoch_time')
    op.drop_index(op.f('ix_signals_history_short_code'), table_name='signals_history')
    op.drop_table('signals_history')
    # ### end Alembic commands ###
