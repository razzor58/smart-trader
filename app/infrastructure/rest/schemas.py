from datetime import datetime, date
import decimal
import uuid

from flask_restplus import Namespace, fields
from flask_restplus import reqparse

from app.domain.entities.plain import Portfolio


parser = reqparse.RequestParser()
parser.add_argument('portfolioId', type=int, location='args', help='id портфеля')

form_parser = reqparse.RequestParser()
form_parser.add_argument('portfolioId', type=int, location='form', help='id портфеля')


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.strftime('%Y-%m-%d %H:%M:%S')
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    if isinstance(obj, uuid.UUID):
        return str(obj)
    raise TypeError("Type %s not serializable" % type(obj))


def parse_get_params(p_name):
    params = parser.parse_args()
    res = params.get(p_name)
    return res


def parse_form_params(p_name):
    params = form_parser.parse_args()
    res = params.get(p_name)
    return res


FLOAT_DIGITS = 2

portfolio_fields = {
    'id': fields.Integer(required=True, description='id портфеля'),
    'name': fields.String(required=True, description='Имя портфеля')
}

user_portfolio_fields = {
    'portfolio_id': fields.Integer(description='id эталонного портфеля'),
    'cash': fields.String(description='Сумма ДС, выделенная на данный портфель')
}

structure_fields = {
    'ticker': fields.String,
    'part': fields.Float,
}


trade_created = {
    'id': fields.Integer(required=True, description='id сделки'),
    'status': fields.String(required=True, description='Статус'),
}


def format_dict(dict_obj):
    for name, _data in dict_obj.items():
        if isinstance(_data, (datetime, date)):
            dict_obj[name] = _data.strftime('%Y-%m-%d %H:%M:%S')
        if isinstance(_data, uuid.UUID):
            dict_obj[name] = str(_data)
        if isinstance(_data, float):
            dict_obj[name] = round(_data, FLOAT_DIGITS)

    return dict_obj


def model_from_entity(entity):
    model = {}
    d = entity.__annotations__
    for k, v in d.items():
        if v is int:
            _type = fields.Integer()
        elif v is str:
            _type = fields.String()
        elif v is float:
            _type = fields.Float()
        elif v is datetime:
            _type = fields.DateTime()
        elif v is bool:
            _type = fields.Boolean()
        else:
            _type = fields.Raw()

        model[k] = _type

    return model


class StatsObject(fields.Raw):
    def format(self, value):

        elems = value['items'] if 'items' in value else value

        if isinstance(elems, dict):
            for k, v in elems.items():

                elems[k] = format_dict(v)
        elif isinstance(elems, list):
            elems = [format_dict(dict_obj) for dict_obj in elems]

        return value


class BidObject(fields.Raw):
    # вообще тут ошибка в названии `create_time` но думаю, что фронт заинтегрил так
    def format(self, value):
        value['bids'] = []
        for bucket in value['items']:
            value['bids'].append({
                'bid_craete_time':  bucket['bid_create_time'].strftime('%Y-%m-%d %H:%M:%S'),
                'is_executed': bucket['is_executed'],
                'execute_message': bucket['execute_message'],
                'items':  [format_dict(v) for v in bucket['items']]
            })
        del value['items']
        return value


class RegularDict(fields.Raw):
    def format(self, value):
        return value


class DataWithAddParams(fields.Raw):
    def format(self, value):
        return value


class ClientDto:
    api = Namespace('client', description='Упрвлени профилем клиента')
    limit_fields = api.model('limit', {
        'currency': fields.String(required=True, description='Валюта'),
        'balance': fields.String(required=True, description='Остаток средств на счете'),
    })
    agreement = api.model('agreement', {
        'agreement_id': fields.Integer(required=True, description='ID Генерального Соглашения')
    })

    agreement_data = api.model('agreement_data', {
        'agreement_id': fields.String(required=True, description='ID Генерального Соглашения'),
        'agreement_num': fields.String(required=True, description='Номер Генерального Соглашения'),
        'limits': fields.List(fields.Nested(limit_fields), description='Свободные денежные средства'),
    })
    output = api.model('data', {
        "data": fields.List(fields.Nested(agreement), description='Соглашения'),
        "success": fields.Boolean(required=True, default=True),
        "error": fields.String(required=True, default=None)
    })


class AdminDto:
    api = Namespace('admin', description='управление стратегиями через GUI')


class PortfolioDto:
    portfolio_api = Namespace('portfolio', description='Портфели')
    pnl_api = Namespace('pnl', description='Статистика портфелей')
    portfolio = portfolio_api.model('master_portfolio_table', {
        'id': fields.Integer(required=True, description='id портфеля'),
        'name': fields.String(required=True, description='Имя портфеля'),
        'recommended_cash': fields.Integer(description='Минимальн сумма активов'),
        'description': fields.String(description='Описание'),
        'income_avg': fields.Float(description='Среднегодовая доходность'),
        'income_total': fields.Float(description='Доходность с начала работы'),
        'max_dd': fields.Integer(description='Максимальная просадка'),
        'signals_cnt_avg': fields.Float(description='Среднее количество сигналов'),
        'structure': RegularDict()
    })
    pnl_days = portfolio_api.model('days', {
        'days_ago': fields.String(required=False, description='Имя портфеля'),
    })

    new_asset_size = portfolio_api.model('cash', {
        'new_asset_size': fields.Integer(required=True, description='Размер ДС портфеля'),
        'tradedate': fields.String(required=False, description='Дата изменения средств'),
        'deposit': fields.Integer(required=False, description='Cумма внесенных ДС'),
    })

    master_portfolio = portfolio_api.model(name='master_portfolio', model=model_from_entity(Portfolio))


class StatDto:
    stat_api = Namespace('stat', description='Статистика портфелей')
    token = stat_api.model('client_token_model', {
        'client_token': fields.String(required=True, description='идентификатор запроса'),
    })

    positions_output = stat_api.model('data', {
        "data": StatsObject(),
        "success": fields.Boolean(required=True, default=True),
        "error": fields.String(required=True, default=None)
    })

    stats_bids_output = stat_api.model('data', {
        "data": BidObject(),
        "success": fields.Boolean(required=True, default=True),
        "error": fields.String(required=True, default=None)
    })

    bids_fields = {
        'bucket_num': fields.String(attribute='bucket_num'),
        'ticker': fields.String(attribute='ticker'),
        'send_status': fields.String(attribute='send_status'),
        'operation': fields.String(attribute='operation'),
        'trade_num': fields.String(attribute='trade_num')
    }

    trade_fields = {
        'ticker': fields.String(attribute='ticker'),
        'direction': fields.String(attribute='direction'),
        'quantity': fields.String(attribute='quantity'),
        'market_price': fields.String(attribute='market_price'),
        'trade_num': fields.String(attribute='trade_num'),
        'commission': fields.String(attribute='commission')
    }

    bid_output = stat_api.model('data', {
        "data": fields.List(fields.Nested(bids_fields)),
        "success": fields.Boolean(required=True, default=True),
        "error": fields.String(required=True, default=None)
    })

    trade_list_output = stat_api.model('data', {
        "data": fields.List(fields.Nested(trade_fields)),
        "success": fields.Boolean(required=True, default=True),
        "error": fields.String(required=True, default=None)
    })


class TradeDto:
    trade_api = Namespace('trades', description='Сделки по портфелю')
    trade = trade_api.model('trades', {
        'num': fields.String(required=True, description='guid сделки в ситеме RAIS'),
        'data': RegularDict(),
    })


class PinDto:
    pin_api = Namespace('pin', description='Управление торговым паролем')
    add_pin_model = pin_api.model('add_pin_model', {
        'pin': fields.Integer(required=True, description='торговый пароль'),
    })

    check_pin_model = pin_api.model('check_pin_model', {
        'pin': fields.Integer(required=True, description='торговый пароль')
    })
    confirm_sms_model = pin_api.model('confirm_sms_model', {
        'code': fields.Integer(required=True, description='проверочный код')
    })


class KeyCloakDto:
    auth_api = Namespace('keycloak', description='авторизация приложения')


class MasterDto:
    master_trade_api = Namespace('manage', description='Отправить сигнал по мастер портфелю')
    master_trade = master_trade_api.model('master_trades', {
        'ticker': fields.String(required=True, description='код инструмента'),
        'acc_vol': fields.Float(required=True, description='накопленный объем'),
        'vol': fields.Float(required=True, description='объем изменений'),
        'operation': fields.String(required=True, description='тип сделки'),
        'price': fields.Float(required=True, description='цена сделки')
    })
    name = master_trade_api.model('name', {
        'name': fields.String(required=True, description='имя')
    })


class ServiceDto:
    service_api = Namespace('service', description="Импорта клиента из ЕВА")
    service = service_api.model('service', {
        'portfolio_id': fields.Integer(description='id эталонного портфеля'),
        'cash': fields.Integer(description='Сумма ДС, выделенная на данный портфель'),
        'replace': fields.Boolean(description='очистить существующие данные', required=False),
        # 'tariff_id': fields.Integer(description='id тарифа', required=False, default=1)
    })


class PortfolioPageDto:
    portfolio_page_api = Namespace('portfolio_pages', description='Наборы портфелей')
    portfolio_page = portfolio_page_api.model('portfolio_pages', {
        'id': fields.Integer(required=True, description='id связи'),
        'portfolio_id': fields.String(required=True, description='id портфеля'),
        'landing_page': fields.String(required=True, description='Имя набора'),
    })


class RequestDto:
    api = Namespace('orders', description='Торговые заявки')

    order_fields = api.model('BidRequest', {
        'secur_code': fields.String(required=True, description='Код инструмента'),
        'qty': fields.Integer(required=True, description='Количество инструмента')
    })

    model = api.model('bid_request', {
        'data': fields.List(fields.Nested(order_fields))
    })
