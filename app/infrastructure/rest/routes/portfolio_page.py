from flask_restplus import Resource

from app.infrastructure.db.domain_dao import SessionContext
from app.infrastructure.rest.tools import get_login_from_token
from app.infrastructure.rest.tools import api_response
from app.infrastructure.rest.schemas import PortfolioPageDto

from app.domain.usecases.api.portfolio_page_manage import PortfolioPageManageUseCase

api = PortfolioPageDto.portfolio_page_api


@api.route('/', defaults={'login': None, 'page_id': None}, doc=False)
@api.route('/<int:page_id>', doc=False)
class PortfolioPage(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db = SessionContext()
        self.use_case = PortfolioPageManageUseCase(portfolio_dao=self.db)

    def __del__(self):
        self.db.commit_and_close()

    @get_login_from_token
    def get(self, **kwargs):
        portfolio_data = self.use_case.get_portfolio_pages(kwargs.get('page_id'))
        return api_response(data=portfolio_data, **kwargs)

    @api.expect(PortfolioPageDto.portfolio_page)
    @get_login_from_token
    def post(self, **kwargs):
        portfolio_page_id = self.use_case.create_portfolio_page(api.payload)
        return api_response(data={"portfolio_page_id": portfolio_page_id}, **kwargs)

    @get_login_from_token
    def delete(self, **kwargs):
        self.use_case.delete_portfolio_page(kwargs['page_id'])
        return api_response(data=None, **kwargs)
