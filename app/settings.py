from envparse import env
from vault_kv import init_from_env

vault = init_from_env()
env.read_envfile()

LOG_LEVEL = env.str("LOG_LEVEL", default="DEBUG")

MULTIPROCESS_METRICS_MODE = True if env.str("prometheus_multiproc_dir", default=False) else False

# TODO: приложение должно работать одинаково, и не знать о том, где запускается.
TEST_CONFIGURATION = env.str("STAGE_ENV", default=False)

# ----------- Basic ------------------------------------------------------------------

APP_NAME = "stage" if TEST_CONFIGURATION else ""
APP_URL_PREFIX = env.str("APP_URL_PREFIX", default="/api/test")
APP_DOC_ENABLED = env.bool("APP_DOC_ENABLED", default=True)
APP_VERSION = "1.8"
DOCUMENT_ROOT = env.str("DOCUMENT_ROOT", default="/tmp")
DOCUMENT_URL = env.str("DOCUMENT_URL", default="https://rais.broker.ru/docs/")
RECORD_TO_PAGE = 10

HTTPS_PROXY = {"https": "http://s-proxy-04-g.global.bcs:8080"} if env.bool("USE_PROXY", default=True) else {}

# ----------- DB connection ----------------------------------------------------------

DB_CONNECT_STRING = "postgresql://{}:{}@{}:{}/{}".format(
    env.str("DB_USER", default="postgres"),
    vault.get_key("DB_PASS", default=env.str("DB_PASS", default="qwe123")),
    env.str("DB_HOST", default="localhost"),
    env.int("DB_PORT", default=5432),
    env.str("DB_NAME", default="postgres"),
)

# ----------- Trading configuration --------------------------------------------------

TRADING_OPEN_HOUR = 10
TRADING_CLOSE_HOUR = 18
TRADING_CLOSE_MINUTES = 29
TRADING_CLOSE_SECONDS = 50

# ----------- Key Cloak configuration ------------------------------------------------
KEY_CLOAK_BASE_URL = env.str("KEY_CLOAK_BASE_URL", default="")
KEY_CLOAK_BASE_URL_INT = env.str("KEY_CLOAK_BASE_URL_INT", default="")

KEY_CLOAK_PERSEUS_CLIENT_ID = "ef-front"
KEY_CLOAK_PERSEUS_USER = vault.get_key("KEY_CLOAK_PERSEUS_USER",
                                       default=env.str("KEY_CLOAK_PERSEUS_USER", default=""))
KEY_CLOAK_PERSEUS_PASSWORD = vault.get_key("KEY_CLOAK_PERSEUS_PASSWORD",
                                           default=env.str("KEY_CLOAK_PERSEUS_PASSWORD", default=""))
KEY_CLOAK_PERSEUS_CLIENT_SECRET = vault.get_key("KEY_CLOAK_PERSEUS_CLIENT_SECRET",
                                                default=env.str("KEY_CLOAK_PERSEUS_CLIENT_SECRET", default=""))

KEY_CLOAK_BROKER_TEST_URL = "https://auth.tusvc.bcs.ru/auth/realms/Broker/protocol/openid-connect/token"
KEY_CLOAK_BROKER_TEST_USER = "k-213213"
KEY_CLOAK_BROKER_TEST_PASSWORD = vault.get_key("KEY_CLOAK_BROKER_TEST_PASSWORD",
                                               default=env.str("KEY_CLOAK_BROKER_TEST_PASSWORD", default=""))
KEY_CLOAK_BROKER_TEST_CLIENT_ID = "bcspassport"
KEY_CLOAK_BROKER_TEST_CLIENT_SECRET = vault.get_key("KEY_CLOAK_BROKER_TEST_CLIENT_SECRET",
                                                    default=env.str("KEY_CLOAK_BROKER_TEST_CLIENT_SECRET", default=""))

KEY_CLOAK_BROKER_USER = env.str("KEY_CLOAK_BROKER_USER", default="")
KEY_CLOAK_BROKER_CLIENT_ID = env.str("KEY_CLOAK_BROKER_CLIENT_ID", default="")

KEY_CLOAK_BROKER_PASSWORD = vault.get_key("KEY_CLOAK_BROKER_PASSWORD",
                                          default=env.str("KEY_CLOAK_BROKER_PASSWORD", default=""))

KEY_CLOAK_BROKER_CLIENT_SECRET = vault.get_key("KEY_CLOAK_BROKER_CLIENT_SECRET",
                                               default=env.str("KEY_CLOAK_BROKER_CLIENT_SECRET", default=""))

# ----------- Trade Pin configuration ------------------------------------------------

TRADE_PIN_URL = "https://ent.bcs.ru/bm-trade-pin/trade-pin/"
TRADE_PIN_URL_TEST = "https://bm.tusvc.bcs.ru/trade-pin/trade-pin/"

# ----------- Report service configuration -------------------------------------------

REPORT_SERVICE_URL = env.str("REPORT_SERVICE_URL", default="")
REPORT_SERVICE_TEMPLATE_URL = env.str("REPORT_SERVICE_TEMPLATE_URL", default="")

# ----------- Quotes service configuration -------------------------------------------

QUOTES_SERVICE_URL = env.str("QUOTES_SERVICE_URL",
                             default="https://api.bcs.ru/quotes-partner/v1/351AAE82-ABA8-4688-8C58-ECCAFEFB63D1")
QUOTES_SERVICE_URL_TEST = "https://api.bcs.ru/quotes/v1"
REQUEST_TIMEOUT = 10
# ----------- Bids configuration -----------------------------------------------------

SIGNAL_ACTIVE_DAYS = 1

# ----------- Proxy fix configuration ------------------------------------------------

PROXY_FIX_CLIENT_TOKEN = vault.get_key("PROXY_FIX_CLIENT_TOKEN", default=env.str("PROXY_FIX_CLIENT_TOKEN", default=""))
PROXY_FIX_SERVICE_URL = env.str("PROXY_FIX_SERVICE_URL", default="localhost:10000")
PROXY_FIX_TEST_USERS = [
    "373022",
    "505705",
    "186048",
    "SPBFUT00s4b",
    "139722",
    "B1",
    "B1/RKS101T",
    "362274",
    "512653",
]

# ----------- Quick exporter configuration -------------------------------------------

KAFKA_SERVERS = env.list("KAFKA_SERVERS", default=["localhost:9990"])
KAFKA_USERNAME = vault.get_key("KAFKA_USERNAME", default=env.str("KAFKA_USERNAME", default=""))
KAFKA_PASSWORD = vault.get_key("KAFKA_PASSWORD", default=env.str("KAFKA_PASSWORD", default=""))
KAFKA_GROUP_NAME = "RAIS_CONSUMER_GROUP"
KAFKA_AUTO_COMMIT_INTERVAL_MS = 1000
KAFKA_MAX_POLL_RECORDS = 10

KAFKA_TRADES_TOPIC = env.str("KAFKA_TRADES_TOPIC", default="exporter-trades")
KAFKA_MONEY_LIMITS_TOPIC = env.str("KAFKA_MONEY_LIMITS_TOPIC", default="exporter-moneylimits")
KAFKA_DEPO_LIMITS_TOPIC = env.str("KAFKA_DEPO_LIMITS_TOPIC", default="exporter-depolimits")
KAFKA_FUTURE_HOLDINGS_TOPIC = env.str("KAFKA_FUTURE_HOLDINGS_TOPIC", default="exporter-futuresholding")

KAFKA_XML_NAMESPACE = 'http://schemas.bcs.ru/microservices/'

KAFKA_LOGS_BATCH_SIZE = 100000
KAFKA_LOGS_TIME_TO_RESTART = 3

# ----------- Rabbit MQ configuration ------------------------------------------------

RABBIT_MQ_URL = env.str("RABBIT_MQ_URL", default="")
RABBIT_MQ_EXCHANGE = "rais.topic"
RABBIT_MQ_SERVER_IP = "192.168.8.33"
RABBIT_MQ_SERVER_PORT = 5672
RABBIT_MQ_ROUTING_KEY = "rais.clients.bids"
RABBIT_MQ_QUEUE_NAME = "rais"

# ----------- DataHub configuration ------------------------------------------------
RU_DATA_AUTH_URL = 'https://dh2.efir-net.ru/v2/Account/Login'
RU_DATA_LOGIN = vault.get_key("RU_DATA_LOGIN",
                              default=env.str("RU_DATA_LOGIN", default=""))
RU_DATA_PASSWORD = vault.get_key("RU_DATA_PASSWORD",
                                 default=env.str("RU_DATA_PASSWORD", default=""))

QUOTES_SERVICE_WITH_DELAY_URL = env.str('QUOTES_SERVICE_WITH_DELAY_URL', default='')

CALENDAR_API_TOKEN = vault.get_key('CALENDAR_API_TOKEN',
                                   default=env.str('CALENDAR_API_TOKEN', default=''))

PROXY_CATALOG_URL = env.str('PROXY_CATALOG_URL', default='')
PROFILE_SERVICE_URL = env.str('PROFILE_SERVICE_URL', default='')

BLACK_BOX_API_KEY = vault.get_key("BLACK_BOX_API_KEY",
                                  default=env.str("BLACK_BOX_API_KEY", default=""))

BLACK_SIGNAL_LIMIT = 1
# ----------- API configuration -----------------------------------------------------
API_ALLOWED_USERS = [KEY_CLOAK_BROKER_USER, ]
