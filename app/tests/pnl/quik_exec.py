# Common package
import sys
import os

from app.domain.usecases.cli.exporter_service import ExporterService as ps
from app.infrastructure.db.domain_dao import SessionContext

# App modules
parent_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(parent_path)

quik_trade = {'event_time': '2020-11-21 14:00:10', 'document_id': '81fa969e-2412-4545-ad3a-9ab4d34dfffd',
              'trade_num': '3044519501', 'trade_date': '2019-11-21T00:00:00', 'operation': 'Продажа',
              'class_code': 'TQBR', 'trade_date_time': '2019-11-21T14:00:10', 'order_num': '18922687653',
              'sec_code': 'BAC', 'account': 'L01-00000F00', 'client_code': 'B1/RKS101T', 'qty': '1',
              'qty_pcs': '1',
              'price': '6055', 'value': '6055', 'repo_value': '0', 'accruedint': '0', 'settle_code': 'Y2',
              'settle_date': '2019-11-25T00:00:00', 'settle_currency': 'RUR', 'exchange_code': None,
              'trades_time_ms': None, 'price2': '0', 'repo2value': '0', 'accruedint2': '0', 'repo_term': '0',
              'ts_commission': '0', 'linked_trade': '0', 'kind': 'Обычная', 'kindFlag': None, 'r_flags': '68'}


def main():
    """Подтвердиить отправку заявки по портфелю клиента"""
    db_session = SessionContext()
    ps.apply_quik_trade_from_kafka(quik_trade, db_session)
    db_session.session_close()


if __name__ == "__main__":
    main()
