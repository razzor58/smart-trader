from app.infrastructure.db.infra_dao import SessionContext
from app.domain.constants import TickerTypes
from app.config import logger
from app.infrastructure.tools import KeyCloak, Realms, http_request


RU_DATA_DATE_COUPONS_URLS = 'https://dh2.efir-net.ru/v2/Bond/DateOptions'


def process():
    with SessionContext() as db_session:

        ticker_list = db_session.get_ticker_list(ticker_type=TickerTypes.BONDS)
        auth_header = KeyCloak(Realms.RU_DATA).get_auth_header()
        req_date = db_session.get_work_day()
        date = req_date.dates.strftime('%Y-%m-%d')
        next_date = req_date.next_date.strftime('%Y-%m-%d')

        for ticker in ticker_list:
            request_date = next_date if ticker['trade_mode'] == 1 else date
            data = http_request(**{
                'base_url': RU_DATA_DATE_COUPONS_URLS,
                'method': 'POST',
                'params': None,
                'isin': ticker['isin'],
                'request_date': request_date,
                'json': {
                    'symbol': ticker['isin'],
                    'date': request_date,
                    'isCloseRegister': True
                },
                'headers': auth_header
            })
            data.update({'tradedate': request_date, 'isin': ticker['isin']})
            db_session.save_date_option(data)

    logger.info(f'Results: total bonds in portfolios={len(ticker_list)}, record saved={len(ticker_list)}')
